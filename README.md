# Course materials

Still in development.

## Deploy

This is the `user` folder of a Grav install, complete with the `learn2` theme with some small cutomizations. To deploy simply upload to the grav install at the right domain.

## Worklog

- 25082019 Added p5js widget, thanks to https://toolness.github.io/p5.js-widget/
- 24082019 added new sections on learnable programming and social computing, added the IBM video
