---
title: Parametric design strategies
---

If you have followed the tutorial on experimental typography you are probably already familiar with the `textToPoints` function in p5js. Using this same technique, I have built this *simple sketching tool* that allows you to explore the *design space* afforded by making our typographic experiment parametric.

To do that I have decided that I am going to turn certain element in my experiment's composition into dynamic parameters that I can change using a UI. The parameters in this case are:
- the actual string of text to be displayed
- the background color of my entire composition
- the size of the typeface I want to draw
- the fill and stroke parameters of the shape I will draw at every point
- thickness of the stroke
- size of my shape
- density of the sampling on the typeface's points
- and the speed of rotation of my shape

Making these parameters dynamic and creating a simple UI for them allows us to quickly change these parameters so that we can see our results immediately without having to change any code. The range of these parameters defines a *design space* where a multiplicity of looks can be explored.


<iframe src="https://codesandbox.io/embed/mdd-experimental-typography-parametric-xs9dg?fontsize=14&hidenavigation=1&theme=dark&view=preview" style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;" title="mdd-experimental-typography-parametric" allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking" sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"></iframe>