---
title: Browser-based graphics
---

This is a course meant to get you started on browser-based computer graphics from a designers perspective. The goal of this course is to familiarize the reader with computer graphics, using the humble web browser that everyone has installed in their OS as a gateway for experimentation. Web browsers are amazingly capable pieces of software and they can do a lot more than just displaying websites passively. 

If you are completely new to browser-based graphics, you are adviced to follow the order in this index to approach the subject in an order that hopefully makes more sense. Once you are more comfortable you can use this index as a quick access to the parts that you need a refresher on.

This course was written by Luis Rodil-Fernandez to support students of the Master of Digital Design at the Amsterdam University of Applied Sciences. A few sections link out to other resources, see the acknowledgements section for more details.

### Preparing for the sprint

Before the sprint, please:
- Create accounts in both [https://editor.p5js.org/](https://editor.p5js.org/) and [codesandbox.io](https://codesandbox.io)
- Think of the coolest website you have seen lately and bring the bookmark for the **Website Showdown** session
- Read all the chapters in the **Static Graphics** section below
- Make yourself familiar with the basics of p5js and play around a little with the sketches provided in the notes
- Collect notes as you read through the material using the *hypothes.is annotation layer*

#### About the annotation layer

As you read the contents of these lessons, questions will inevitably pop-up. You can use the [collaborative annotation tool hypothes.is](https://web.hypothes.is) to leave your comments and questions on these pages. Install the free hypothes.is browser plugin to interact with the annotation's layer on this website. Once you have installed and created an account on **hypothes.is**. Add your questions to the hypothes.is group called [MDD graphics sprint](https://hypothes.is/groups/X8wKPgpR/mdd-graphics-sprint) so that everybody's questions, comments and feedback can be accessed there before, during and after the sprint.

### Acknowledgements

These instruction materials owe a great deal to [Lauren McCarthy and the p5js community](http://www.p5js.org) as well as Processing at large and the Processing Foundation for supporting the development of all this wonderful work.

This course links out to other wonderful educational resources most notably Rune Madsen's [Programming Design Systems](https://programmingdesignsystems.com/introduction/index.html#introduction-AlRYsdq), which is fantastic and you should read in its entirety.

Thanks also to Atul Varma aka [@tollness](https://github.com/toolness) for the [p5js widget](https://github.com/toolness/p5.js-widget), which made it possible for this course to have interactive inline code examples.

Most of the examples in these tutorials are my own, as I couldn't find something with the focus and the fast pace that I needed in my course. All this material is released in the hope that it can be helpful to my students first and foremost and perhaps other souls out there in the internet that want an in-depth yet directly applicable knowledge of animation and graphics in the browser.

In this materials I refer to the work of artists, designers and coders. I try to give credit to everybody for their work. If you see your work here and the credit is missing, please accept my apologies and feel free to contact me to notify me of the omission.

I have used many images without license. Although unlicensed, these works are believed to qualify as fair use: The purpose of use is non-profit educational material, the works are used for informational purposes only, and the works are widely published in books and on websites.

## Static graphics
- [Understanding your canvas](/p5js/understanding-your-canvas/)
	- Vector screens
	- Raster screens
	- Form factors
	- Mass production of rectangular screens
	- Pixels, resolution and aspect ratio
	- The web browser as a screen
- [Color](/p5js/color/)
	- [Digital representation of color](/p5js/color/digital-color)
	- [Color spaces](/p5js/color/color-spaces)
	- [Josef Albers color cycling experiment](/p5js/color/josef-albers)
	- [A short history of color theory](https://programmingdesignsystems.com/color/a-short-history-of-color-theory/index.html#a-short-history-of-color-theory-xZzRFOZ) (link out to Rune Madsen's material)
	- [Color Schemes](https://programmingdesignsystems.com/color/color-schemes/index.html#color-schemes-dV9Rf6L) (link out to Rune Madsen's material)
- [Painting with code](/p5js/painting-with-code)
	- [Introduction to p5js](/p5js/painting-with-code/p5js-intro)
	- [Basic primitives](/p5js/painting-with-code/p5js-basic-primitives) line, ellipse, triangle, rect & arc
	- [Bitmaps](/p5js/painting-with-code/p5js-bitmaps) 
	- [Repetitions using loops](/p5js/painting-with-code/p5js-loops)
- [Typography](http://printingcode.runemadsen.com/lecture-typography/) (link out to Rune Madsen's material)
	- [Experimental typography](https://www.youtube.com/watch?v=wbDF6xcgvV8) (link out to youtube tutorial by Xin Xin)
	- [OpenType glyph inspector](https://opentype.js.org/glyph-inspector.html) observe the glyphs in your favorite font
	- [Type Therapy](https://emanmakki.com/Type-Therapy) interactive typography experiments with source code
	- [Generative strategies](/p5js/typography/generative) example of a parametric typography experiment made at MDD in 2021
- [Coordinate system](/p5js/coordinate-systems)
  - Rectangular screens
	- Relative coordinates
		- Translate, rotate
		- Push & pop
  - Circular screens
  	- Polar coordinates
- [Input interfaces](/p5js/painting-with-code/input-methods)
	- Mouse
	- Keyboard
	- Touchscreen
- [Modularity exercise](/p5js/painting-with-code/modular-typeface)
- [Bonus: exporting to SVG](/p5js/painting-with-code/p5js-export)
- [Static poster examples](/p5js/painting-with-code/p5js-static-posters)

## Motion graphics
- [Fundamentals](/p5js/animation)
	- [Disney's principles of animation](/p5js/animation/disneys-principles)
- [Animation in device design](/p5js/animation/animation-devices)
- [Tweening](/p5js/animation/tweening)
- Time
	- Using time in your code
- Principles of computational animation
  - [Kenichi Yoneda's (kynd) Interpolation and Animation notes](https://kyndinfo.notion.site/Interpolation-and-Animation-44d00edd89bc41d686260d6bfd6a01d9)
	- LERP
	- Easing (JS & CSS)
	- Random motion
	- Perlin noise
	- Physics-based animation
		- Magnetism
		- Particle systems
	- Evolutionary models
- Generative grahics
- Rotoscoping
- Aesthetics of animation history and trends
- Animation and interaction

## 3D graphics
- work in progress