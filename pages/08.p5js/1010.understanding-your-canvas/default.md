---
title: Understanding your canvas
---

![Replaced by Maze de Boer](../media/maze-de-boer-replaced.jpg?classes=caption "'Replaced' by Maze de Boer")

#### Vector screens

Vector screens do not really use pixels to define an image, instead they work more like an oscilloscope. By deflecting a beam that swipes very fast through the screen to form images. Here you can see the _Vectrex_, a video came console that used vector graphics on a vector screen. You can still see some of this in vintage videogame parlours. All the graphics in the classic game _Asteroids_ were made this way.

![vectrex](../media/vectrex.jpg?classes=caption "Vectrex gaming console")

These types of screens are monochromatic, they can't really display color. The _Vectrex_ addressed this by adding transparent plastic overlays that added color to the play area. Some games shipped with their own overlays that you could place on your screen to add color to the game.

This approach to graphics is still is use today, although it is admittedly a bit geeky. This is more recent work made in Processing using an oscilloscope.

XYScope is a library for Processing to render graphics on a vector display (oscilloscope, laser) by converting them to audio. [Ted Davis XYscope](http://teddavis.org/xyscope) [instagram](http://instagram.com/teddavisdotorg/)

The method used by vector screens (a deflected beam) is also how lasers (in clubs for example) continue to work. A laser as a device used to present graphics is basically a continuous beam deflected (at very high speeds) by two mirrors that control the X and Y position of the beam in two dimensions.

<iframe src="https://player.vimeo.com/video/226597331" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

This is the result of a workshop on generative typography in the Basel School of Design by glitch-artist [Ted Davis](https://teddavis.org/). It is a good example of the combination of both, vector and raster graphics. Lasers work very similarly to vector display systems, with mirror deflecting the beam in the X and Y at great speeds axis to form an image. The refresh rate of laser is quite poor though as the deflection is mechanical.

![lettera](../media/laser-typo-a.gif)
![letterj](../media/laser-typo-j.gif)
![letterz](../media/laser-typo-z.gif)

<iframe src="https://player.vimeo.com/video/274485191" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/274485191">LASER LETTERS II</a> from <a href="https://vimeo.com/ffd8">ffd8</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

#### Raster screens

The advent of CRT (Cathode Ray Tube) brought a more efficient way of displaying flat images at faster speeds (higher refresh rates).

![ctr](../media/ctrmonitor.gif)

![colorcrt](../media/color-crt-closeup.jpg?classes=caption "Close-up of a color CRT screen (notice the interlacing)")

![interlacing](../media/interlacing.jpg?classes=caption "Detail of interlacing effect on digital video, on the left interlaced video and on the right progressive scan video")

#### Vector vs. Raster

![vectorraster](../media/vector-raster.jpg)
![vectorraster](../media/vector-raster2.jpg)
![vectorraster](../media/vector-raster3.jpg)
![vectorraster](../media/vector-raster-circles.png)

#### Screen form factors

Screens haven't always been rectangular. In fact the form of a screen used to be much more closely related to the form factor of the product. With the mass production of rectangular screens, this practice is not so common anymore. These days is mostly products marketed as "luxury" have non-rectangular form factors today.

![HMSBelfast](../media/radar_0_hms_belfast.jpg?classes=caption "Early radar screen from HMS Belfast, 1936")

![weather](../media/weather_radar.jpg?classes=caption "Early weather radar - Hurricane Abby approaching the coast of British Honduras in 1960")

![sagesystem](../media/ibm_sage3.jpg?classes=caption "SAGE: Semi-Automatic Ground Environment 1958 (US Air Force, in conjunction with MIT and IBM)")
![sagescreen](../media/ibm_sage.jpg?classes=caption "Detail of SAGE vector screen")

![sageinput](../media/ibm_sage_console_with_lightgun2.jpg?classes=caption "Detail of SAGE's input system in use by operator")

![sutherland1](../media/sutherland-sketchpad.jpg?classes=caption "The TX-2 graphics computer, running Ivan Sutherland's Sketchpad software (1963)")

![sutherland1](../media/Sketchpad-Apple.jpg?classes=caption "The input system in Sketchpad")

![handheld](../media/handheld-ultrasound-prototype.jpg?classes=caption "Handheld ultrasound prototype")

![ultrasound](../media/ultrasound.png?classes=caption "Ultrasound image, observe its shape and low resolution")

![ultrasound2](../media/clarius-handheld-ultrasound.jpg?classes=caption "These days of course there's an app for that")


#### The Black Mirror

Screens have become such a pervasive presence in our lives that **we almost expect them to be the support for all the images we encounter today**. 

The title of the _Black Mirror_ series by Charlie Brooker plays exactly on this notion of the screen as a signifier for all technology as the series explores uncanny developments in how people relate to tech.

![rozendaal1](../media/rozendaal-2011-nordin-stockholm-01.jpg)
![rozendaal2](../media/rozendaal-2011-nordin-stockholm-02.jpg)

**Popular Screen Sizes (60″, 55″, 46″, 40″, 32″, 27″, 24″, 21″, 17″, 15″, 13″, 10″, 7″, 3.5″)** installation by Rafaël Rozendaal


#### Pixels

A *pixel* is tiny picture element in a screen, it is a vehicle used by the screen to deliver to us the experience of a color. They have become so small that we no longer perceive them, but they are still there, as the _bricks_ that form all images in  our screens. Screens cannot operate in terms of whole images, they do not _understand_ the general composition of an image. Screens for example, cannot represent how colours are layered in a painting when they display the picture of a painting. In old monitors and TVs, or early videogame consoles the **look** was determined not just by the pixels of the digital system, but also by the analog conversion used by the display. An old TV is an example of an analog display. Analog display's (similarly to vector displays) represent the world using a beam of electrons that scans the phosphor coating inside of the glass vessel of the screen, this beam travels horizontally along the screen and traces the image is what are known as *scanlines*.

![megaman](../media/scanline-megaman.jpg?classes=caption "Megaman in an old CRT analog monitor")

Compare these two images, on the left the image is shown in digital pixels in a digital display system, on the right it is shown in digital pixels using an analog display screen.

![scanline-vs-pixels](../media/pixel-vs-scanline.jpeg)

Scanlines give you that instant retro look!

#### Aspect ratios

Pixels are in fact almost never square. Most pixels are taller than they are wider, but that's not how we perceive them. The dimensions of a pixel are dependant on the screen's resolution.

#### Resolution

Next to color, resolution is perhaps on of the most complex topics in image perception. Resolution doesn't only refer to the technical fact of how many pixels the screen has in each dimension it has more to do with how the image is constructed in these picture elements. The field of photography had notions of resolution that were very specific to film types and material substrates (density of grains of silver per frame for example). In the digital image the notion of resolution is even more complex. For the purpose of this module we will focus on two aspects of the notion of resolution: pixel densities and screen dimensions.

High-end screens and cameras today arguably have resolutions higher than our visual perception systems can process. Our eyes can literally not process all the information that modern imaging techniques are capable of registering. We can no longer sea all the pixels, our brains merges the visual information into a single smooth image.

If you want to dive deeper into the topic of image resolution and the the implications of shifts in resolutions, have a look at the work of _Rosa Menkman_, specifically her website [Beyond Resolution](https://beyondresolution.info/).

#### Relationship between browser, canvas and screen

When we are working in the browser, our coordinate system is relative to the canvas where the render operations will occur. This means that we cannot assume that the pixels at coordinate **(0, 0)** will always be at the top-left of our screen. It will be only at the top-left of our canvas. The canvas position will depend on the browser's window position and the characteristics of the HTML document that contains our canvas, such as margins, paddings, etc. So if you want your graphics to occupy the entire browser window make sure your canvas is correctly set up. We will see how to do that later.

## Acknowledgements

The artworks used to illustrate are low-res images from works by Maze de Boer and Rafael Rozendaal, obtained from the artists own portfolio websites. Thanks!

Images from IBM's SAGE system were obtained mostly from [this article in ExtremeTech](https://www.extremetech.com/computing/151980-inside-ibms-67-billion-sage-the-largest-computer-ever-built). Thanks!

Other SAGE images from [this Atlantic article](https://www.theatlantic.com/technology/archive/2013/01/the-never-before-told-story-of-the-worlds-first-computer-art-its-a-sexy-dame/267439/).

Laser typography animated gifs from [CreativeApplications.net page on Ted Davis workshop](https://www.creativeapplications.net/processing/laser-letters-typography-meets-media-interaction-at-the-basel-school-of-design/). Page of glitch-artist [Ted Davis](https://teddavis.org/)

Images of radar screens and XT-2 are all from the excellent [Encyclopedia of Human-Computer Interaction, 2nd Ed.](https://www.interaction-design.org/literature/book/the-encyclopedia-of-human-computer-interaction-2nd-ed/visual-representation). Thanks a lot!