---
title: LCD screen teardown
---

![mclellan01](../media/clock-mclellan-600x799.jpg)
![mclellan03](../media/iphone-teardown.jpeg)

Please form 6 groups and take a screen from the pile. This exercise is about deep observation and understanding of our screen-based canvas. Using screw drivers, pliers and the various tools available in the lab, please take this screen apart. The purpose of this exercise is not to demolish, but to deconstruct. Look at the work of Todd McLellan to get an idea of what I mean. Try to take things apart carefully without breaking them if you can.

As you take the screen apart and come across it's insides ask yourself:
- how is this put together?
- how are these pieces fastened to each other?
- how do the wires stay in place?
- how do the mechanical parts move? how does the swivel work?
- what connectors are exposed?
- what each the function of each part?

Document every step and every part using photography or video. Create a document on Gdrive, or Github wiki or Notion, wherever is most convenient for you to collect your material. 

Use a search engine to understand each part, try to learn more about it, maybe how it does what it does or how much it costs or what else you can do with it or where it comes from. Important as well is: how to dispose of it properly! you will be needing that later.

Look for projects or alternative uses for old screens and learn more about alternative uses for these parts. Link to documentation of these projects. Try to arrange the information you come across in a coherent way.

![mclellan04](../media/motorbike-teardown.jpg)
![mclellan02](../media/phone-mclellan-600x799.jpg)

