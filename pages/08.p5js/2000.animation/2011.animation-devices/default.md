---
title: Animation in devices
---

Animation isn't constrained to screen-based media alone. If you pay close attention you will see animation playing a large role in your daily experiences with physical products as well. A classics example of Apple's obssession with attention to detail is the sleeping light of the macbooks.

#### The macbook sleeping light

![macbook](../media/breathing-led.gif)

>I recall listening to a rare lecture by Jony Ive in London back in the late 1990s where he explained that, when you were trying to sleep, the old sleep LEDs of laptops would blink on and off harshly, lighting up your entire bedroom each time which made it harder for some people to get to sleep and irritated people.

>They therefore set out to create a more relaxing light which was not so aggressive and seemed more anthropomorphic. 

>As simple as this may sound, it meant going to the expense of creating a new controller chip which could drive the LED and change its brightness when the main CPU was shut down, all without harming battery life.

Details matter!

#### Animation as feedback to interaction

Other devices use light an animation to give people feedback about their internal state, they might display a rolling animation when they are loading, or listening. They might display a solid light when they are in playback state, etc. Home assistants are a typical example of using physical light as an interaction feedback mechanism. 

![alexa](../media/alexa.gif)

![assistants](../media/speech-assistants-leds.gif)

#### The follow me 🐰 rabbit
- [A Closer Look at Apple's Breathing Light](https://avital.ca/notes/a-closer-look-at-apples-breathing-light) 