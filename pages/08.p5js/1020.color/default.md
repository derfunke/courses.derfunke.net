---
title: Color
---

Color is one of the most elusive qualities of all of human experience. As well as a crucial aspect in all visual work. The quality of a conscious subjective experience is called is called "qualia", and we do not really know if there are such things as common qualia for colours. This means that what you understand as the experience of *yellow* might or might not be the same thing that another person understands as *yellow*. It sounds a little crazy! How then could we agree that for example a football referee is showing a player a "yellow" card? How can we all agree on how to cross a road given that colour in traffic lights is utterly significant? Or how does people know when and where to take the right metro line if most maps are color coded?

There's more to colour than meets the eye! How we perceive colors has a lot to do with the the surface that we are looking at, in what context we are seeing it, what other colors are around it, what our expectations are and some cultural conditioning, is a complex cocktail and as a designer you are expected to master this context for good effect in your visual communication.

An important aspect of color that you need to understand from the get go is that colours interact. How we perceive a color has much to do with the colours around it.

#### Color mixing systems

In the **additive color** system, the appearance of a color is determine by different gradations of the primary color components, which in the case of light, and theremore most screens, is based on **RED**, **GREEN** and **BLUE**.

![basiccolors](../media/red_green_blue.png)
![additive](../media/2000px-AdditiveColor.svg.png?resize=200)

For print processes where each layering of the inks results in less light being reflected back, the substractive color system is more appropriate. In this system the primary colors are RYB (RED, YELLOW and BLUE). This is the color system that you likely learnt in primary school as it is the one most commonly used in painting too. RYB is most appropriate when the pigments actually mix. With modern printing technologies there wasn't as much color mixing as there was layering and for this purpose *red* and *blue* where replaced by *magenta* and *cyan* sometimes known as "process red" and "process blue", which are more effective at *filtering* when colors are applied in layers or printed using half-toning techniques. This color system is commonly known as CMY, with K sometimes added for pure blacks in inkjet or other photomechanical printing processes.

![substractive](../media/200px-SubtractiveColor.svg.png)

For the purpose of this course it is not vital that you master conversions between these two color systems, but it is important to understand that what you make on screen might not look as good in print and that color, and how colors are used in these processes will play a role in that difference in quality. So if for example, you use Processing for a generative poster design and you compose your image on screen, you have to make sure that the correct color system is used before you submit it to print if you want your colors to *look right*.

#### Tools

The following tools will help you in choosing colors for your works:

- [Colordot](https://color.hailpixel.com) HSL linked to mouse position let's you quickly build palettes
- [Paletton](http://paletton.com/)
- [Adobe Color CC](https://color.adobe.com/) implements auto palettes
- [Pantone Color Finder](https://www.pantone.com/color-finder/) doesn't always make sense to use Pantone in digital palettes (Pantone is meant for pigments, not for additive colors, e.g. you can't have *gold* color on screen but you can have a *gold* ink pigment)
- [Colourco.de](https://colourco.de/) similar but has tools for pairing colors
- [Coolors.co](https://coolors.co) decent at finding shares, too bloated for my taste

#### Further reading
- ["Interaction of Color", Josef Albers](https://www.worldcat.org/title/interaction-of-color/oclc/813924836?referer=brief_results)
- [A primer to colors in digital design](https://uxdesign.cc/a-primer-to-colors-in-digital-design-7d16bb33399e)

