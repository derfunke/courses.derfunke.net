---
title: Colors interact 
---

This quote is from a book by Josef Albers titled *Interaction of Color*. It's an important reference book that deals with color perception and how colors interact with one another in visual work:

>In visual perception a color is almost never seen as it really is — as it physically is. This fact makes color the most relative medium in art.
>
>In order to use color effectively it is necessary to recognize that color deceives continually. To this end, the beginning is not a study of color systems.
>
>First, it should be learned that one and the same color evokes innumerable readings. Instead of mechanically applying or merely implying laws and rules of color harmony, distinct color effects are produced-through recognition of the interaction of color-by making, for instance, two very different colors look alike, or nearly alike."
> — **Josef Albers**

![albers](../media/interactionofcolor2.gif?classess=caption "The small squares in this figure are identical but they interact differently with the colors around them and no normal human eye can see these as identical when looking at the whole picture")

Albers' way of naming the visual phenomena arising from looking at colors together, is **interaction of colors**. He doesn't speak of *visual illusions*, as if somehow this was a failure of our perceptual systems or a cheap trick conjured by some someone to create confusion, the truth is plainly available to the eye. And the truth is that this *interaction* is very much the nature of colour, similar colors can be perceived differently depending on their companions in a scene. Color theory, while a useful learning, can be problematic if we apply it methodically without letting the actual visual experience of color guide our composition.

### Dynamic demo of color perception shifting

What you see in this experiment is a dynamic version of the Josef Albers illustration, where we simply adjust the saturation of the colours in the background without affecting the square shapes. Observe how your eye interprets the relationship between the colors as the colors fade.

<script type="text/p5" data-autoplay data-preview-width="250">
let sat = 0;
let lastUpdate = 0;
let direction = 1

function setup() {
  createCanvas(200, 200);
}

function draw() {
  let w = width / 6;
  noStroke();
  colorMode(HSL);

  fill(hue(color('#FF8600')), sat, lightness(color('#FF8600')));
  rect(0, 0, w*1.8, height);

  fill(color('#AC5B0E'));
  rect((w*1.8)-40, (height/2)-20, 40, 40);

  let xpos = w*1.8;
  fill(hue(color('#FFF725')), sat, lightness(color('#FFF725')));
  rect(xpos, 0, w*1.2, height);

  fill(hue(color('#2B2C67')), sat, lightness(color('#2B2C67')));
  xpos += w*1.2;
  rect(xpos, 0, w*1.2, height);

  fill(hue(color('#428BB5')), sat, saturation(color('#428BB5')));
  xpos += w*1.2;
  rect(xpos, 0, w*1.8, height);

  fill(color('#AC5B0E'));
  rect(xpos, (height/2)-20, 40, 40);

  // every 100ms update the saturation of our colors
  if( 100 < (millis() - lastUpdate) ) { sat+=direction; lastUpdate = millis(); }

  // go from no-saturation at all to full saturation and then back
  if( (sat > 128) || (sat < 0) ) { direction = -1 * direction; };
}
</script>
