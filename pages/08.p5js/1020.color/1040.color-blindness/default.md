---
title: The problem with Red & Green
---

About 8% of men are colourblind, some women too are colourblind but the rate of incidence is much lower in women, only 1 in 200 women are colourblind. There are an estimated 350 million people in the world who are colourblind. Even more mindblogging, there are many people who are colourblind that don't know it, after all if you have never had the experience of red, why would you miss it?

Given that I have discussed earlier the importance of colour, perhaps it might seem contradictory advie to say that when you design you should also keep in mind the question: is colour the only way to discern information in my work? are there any other cues that could help someone with a different vision ability interpret the work? Can I also perhaps use shapes, icons, textures, directionality, or overlays as additional clues? Creating designs that include clourblind people is no just about tweaking palettes, although that would already go a long way.


### More about colourblindness

- [Read this article in Verge by Andy Baio](https://www.theverge.com/23650428/colorblindness-design-ui-accessibility-wordle) it's a great description of the experience of a colourblind person.
- [Colorblindness simulator browser plugin](https://chrome.google.com/webstore/detail/colorblindly/floniaahmccleoclneebhhmnjgdfijgg)

