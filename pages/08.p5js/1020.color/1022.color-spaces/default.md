---
title: Color spaces
---

If you think of each color component as a dimension in space, three color components will give you a three dimensional space, as you go along one of the axis the color value for that component will change and so will the color. This illustration shows you the cube formed by all the values in the RGB color space.

![additive](../../media/RGB_cube_additive.png)

The RGB color space is great for computers, it is a simple way to represent color computationally, but it is not very intuitive to a human. It is hard to memorize exactly that the color components of a *lemon yellow* are. There are things that are simply a bit harder for people to do in the RGB color space. For example imagine that you pick a bright pink, how to do you easily find a slightly less saturated version of that pink in the RGB color space?

The HSV (hue, saturation, value) color space is a little bit more intuitive for humans to think of digital color. The **HUE** component determines the actual color component, the **SATURATION** determines determines how much of the tint for that color you want and the **VALUE** is how much white or black should be present on that mix. 

![HSV](../../media/hsvcone.gif)

It is possible to convert from one color space to another using some math. The Javascript implementation of this conversion can be found in the appendix.
