---
title: Palette rotation
---

This is an ancient technique that was used in computer graphics on 8 bit games to cause the illusion of animation and background movement. If you pay close attention you will see that it simply consists on a clever color shifting technique. Colors shift their position in the palette.

**Color cycling** or **Palette shifting** was an animation technique used in 8bit videogames. Have a good look at this site and observe how color can be used to animate and to change the mood of a scene.

[effectgames.com tool to explore 8bit color cycling techniques](http://effectgames.com/demos/worlds/)

