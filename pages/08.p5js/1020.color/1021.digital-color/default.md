---
title: Digital representation of color
---

#### How colors are represented in a digital system

Computers represent color by storing bits in memory, a position of a specific bit determines what component of the color and the value of that bit determines how much of that component. This diagram shows how a **32 bit color value** is stored in the graphics cards of a computer.

![32bcolor2](../../media/32bit_color_boxes.png)
![32bcolor](../../media/32bit_color_bits.png)

As you can see, computers store color values in binary numbers (ones and zeroes), the hexadecimal number system is a convenient way to represent these color values and this is why in HTML and CSS you have progrably become used to color your compositions by using the **HEX** color code.

![hexswatches](../../media/color-guide-swatch-13-colors-cmyk-color-chart-630x380.png)
