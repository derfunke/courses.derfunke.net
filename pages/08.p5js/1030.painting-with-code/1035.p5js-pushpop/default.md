---
title: Relative coordinates
---

The `push()` and `pop()` functions of p5js can help us in handling our shapes and position them on screen without having to calculate the individual position of every coordinate for every shape. These functions are used to create local drawing states, for now you can thing of them as *local coordinate systems* that apply only to the shapes in between these two functions. This notion is quite important and it will come back later when we move into 3D graphics and animation.

When you see a `push()` and `pop()` operation you can normally read it as "ok, this person is changing the frame of reference so that the (0,0) positions is somewhere else in the screen now".

Let's look at some practical examples of how this looks like:

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
}

function draw() {
    // paint it pink
    background(255, 0, 200);
    // do not draw a stroke around our shape
    noStroke();
    // change painting color to black
    fill(0);
 
    ellipse(0, 0, 20, 20);
  
    push();
      translate(width/2, height/2);
      ellipse(0, 0, 20, 20);
    pop();
}
</script>

Observe how the instruction that draws the ellipse hasn't changed at all, same parameters, yet these two circles are drawn at different positions. The trick is that the `translate` statement changes the origin of the coordinate system, what we put in `translate(x, y)` becomes our new `(0,0)`.

The `rotate()` statement when given only one parameter will rotate our coordinate system by whatever angle we give it.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  createCanvas(200, 200);
  background(255);
  noStroke();
}

function draw() {
  if (frameCount % 10 == 0) {
    // change to another color every frame
    fill(frameCount * 3 % 255, frameCount * 5 % 255, 
      frameCount * 7 % 255);
    push();
      translate(100, 100);
      rotate(radians(frameCount * 2  % 360));
      rect(0, 0, 80, 20);
    pop();
  }
}
</script>

Let's draw the clock again using `push()`, `pop()`, `translate()`, and `rotate()`.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  createCanvas(200, 200);
  background(255);
  noStroke();
}

function draw() {
  var angleinc = 360 / 12;
  fill(0);
  translate(width/2, height/2);
  for (let tick = 0; tick < 12; tick++) {
    push();
      rotate( radians(tick * angleinc) );
      push();
        ellipse(50, 0, 12, 12);
      pop();
    pop();
  }
}
</script>

At first it might seem boringly simple but `push` and `pop` have a hidden superpower in that they can be compounded, to create recursive visuals that can be visually quite complex.

<script type="text/p5" data-autoplay data-preview-width="250">
let theta = 0;
let t = 40;

function setup() {
  createCanvas(200, 200);
  smooth();
}

function draw() {
  background(255);
  translate(width/2, height/2);
  for (i=0; i<TWO_PI; i+=0.92) {
    push();
    rotate(theta+i);
    line(0, 0, t, 0);
    for (j=0; j<TWO_PI; j+=0.88) {
      push();
      translate(t,0);
      rotate(-theta-j);
      line(0, 0, t, 0);
      for (g=0; g<TWO_PI; g+=0.6){
        push();
        translate(t,0);
        rotate(theta+g);
        line(0,t,0,0);
        pop();
      }
      pop();
    }
    pop();
  }
  theta+=0.005;
}
</script>

### Follow the rabbit into the hole 🐰

- [Read more about transformations in the Processing manual](https://processing.org/tutorials/transform2d/)
- To better understand matrix transformations have a look at this one on [matrix transformations](http://www.proxyarch.com/search/lab2/lab2.html)
