---
title: Using external assets in p5js
---

You can create graphical elements in whatever other tool you like, like Photoshop or Affinity Designer and you can load them in your p5js sketches. How you do this will depend a little bit of where you are deploying your sketches, but the basic mechanics are more or less the same.
- Create the asset in your tool of choice
- Upload the assets to a web-accessible address so that your sketch can load it
- `preload` the asset in your 05js sketch.

See this short video by Dan Shiffman on how this is done using the p5js editor.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rO6M5hj0V-o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

If you prefer reading a step by step tutorial to a video, [this is an excellent tutorial that explains the same thing](https://nycdoe-cs4all.github.io/units/3/lessons/lesson_3.1).

### Example

The following sketch loads a graphical assets, a `.png` file and draws it at the current mouse position turning it into a kind of brush.

[source for this sketch](https://editor.p5js.org/zilog/sketches/Fb0f2anw9)
<iframe width="450" height="500" src="https://editor.p5js.org/zilog/embed/Fb0f2anw9"></iframe>

