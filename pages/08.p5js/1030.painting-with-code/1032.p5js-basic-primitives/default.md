---
title: Basic primitives in p5js
---

Let's draw something else, something that is a little less challenging to actually visualize. Let's draw a circle.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
    // paint it pink
    background(255, 0, 200);
    // do not draw a stroke around our shape
    noStroke();
    // change painting color to black
    fill(0);
    // draw a point at position (0,0)
    ellipse(width/2, height/2, 20, 20);
}
</script>

The `ellipse` function takes 4 parameters, the first two determine the position of its center and the other two determine the `width` and `height` of the ellipse respectively, when these two values are the same the resulting shape is a perfect circle. So the circle is basically a special case of the `ellipse` primitive. Try and change the values and see how this code behaves.

Other primitives in p5js include the [triangle](https://p5js.org/reference/#/p5/triangle), [rect](https://p5js.org/reference/#/p5/rect), [quad](https://p5js.org/reference/#/p5/quad), [ellipse](https://p5js.org/reference/#/p5/ellipse), and [arc](https://p5js.org/reference/#/p5/arc). Each of these needs a certain number of parameters to determine the reference points of the shape in the coordinate system, for example a triangle has 3 points, and each point has 2 components, the X component and the Y component, right? Therefore the `triangle()` function will require 6 parameters.

The circle and the square are special cases of `ellipse()` and `rect()` respectively. Let's play around with some of these.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
    // paint it pink
    background(255, 0, 200);
    // do not draw a stroke around our shapes
    noStroke();
    // change painting color to black
    fill(0);

    let xpos = 60;

    // draw a point at position (0,0)
    ellipse(xpos, height/2, 20, 20);

    // move 25 pixels to the right
    xpos += 15;

    // draw a rectangle
    rect(xpos, (height/2)-10, 20, 20);

    xpos += 35;

    // draw a triangle
    triangle(xpos, (height/2)-10, xpos-10, (height/2)+10, xpos+10, (height/2)+10);

    xpos += 25;

    // draw an arc center x, center y, width, height, start angle, end angle
    arc(xpos, (height/2)+10, 25, 25, PI, TWO_PI);

    xpos += 16;

    // draw an arc center x, center y, width, height, start angle, end angle
    quad(xpos, (height/2)+10, xpos+5, (height/2)-10, xpos+20, (height/2)-10, xpos+8, (height/2)+10);
}
</script>

As you can see when we try to draw things relative to each other, the calculations to place things on the canvas can become quite complex and difficult to track, in this case we are using a variable named `xpos` to keep track of the `x` position of our shapes, but then we have to calculate the `x` coordinate coordinate of each shape. This will quickly become quite a hassle to maintain and our code will become littered with *magical numbers* that we will not be able to read a few weeks from now. There must be a better way!

#### p5js: Lines

A point is a one dimensional primitive, so we only need to pass the `X` and `Y` position of its location to draw it. But a line can be two dimensional, to define a line we need to have two points. Therefore we need two coordinates.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
    // paint it pink
    background(255, 0, 200);
    // change painting color to black
    stroke(0);
    // draw a line across the canvas
    line(0, 0, width, height);
}
</script>

**Do it yourself:** try the same thing with the line that you tried with the dots, in the `X` axis and then try again for the `Y` axis.

Using simple lines and a coloring trick we can draw a button, or let's better call it *a shape that our eye will read as a button*.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
    // paint it pink
    background(255, 0, 200);

    // store the dimensions of our button in some variables to be used later
    let bh = 40;
    let bw = 120;

    // calculate coordinates for our button
    let tlx = (width/2) - (bw/2);
    let tly = (height/2) - (bh/2);
    let brx = (width/2) + (bw/2);
    let bry = (height/2) + (bh/2);

    // draw light part of our button
    stroke(255);
    line(tlx, tly, tlx+bw, tly);
    line(tlx, tly, tlx, tly+bh);
    // draw shaded part of our button
    stroke(0);
    line(brx, bry, brx-bw, bry);
    line(brx, bry, brx, bry-bh);
}
</script>

This simple coloring technique is fundamental to how we perceive UI widgets, most early browser buttons were draw using variations of this technique. It is so fundamental that artist [Jan Robert Leegte made an artwork about it](http://www.leegte.org/work/pastel-drawings/) in the first example that I know of UI-inspired minimalism.

Or if you prefer an old [material design](https://material.io/components/buttons/) approach to drawing a button.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
}

function draw() {
    // paint it white
    background(255);

    // store the dimensions of our button in some variables to be used later
    let bh = 40;
    let bw = 120;
    let raise = 1.5; 

    // calculate coordinates for our button
    let tlx = (width/2) - (bw/2);
    let tly = (height/2) - (bh/2);
    let brx = (width/2) + (bw/2);
    let bry = (height/2) + (bh/2);

  	noStroke();
  	fill(color(200, 200, 200));
  	rect(tlx+raise, tly+raise, bw, bh);
  	fill(color(255, 100, 100));
  	rect(tlx-raise, tly-raise, bw, bh);
}
</script>

