---
title: Introducing p5js
---

#### Why p5js?
- Zero barriers to start with the p5js editor
- Processing heritage
  - Great for graphics fundamentals
  - Invented for designers
  - Easier to learn
- Fairly thin canvas wrapper
- Easier coding means fast prototyping
- Integrates well with other web development techniques
- Once you get the hang of it, it is a smoother transition to change to other libraries
- Great and growing community
- The *p5js widget* means I can write tutorials that have live illustrations which is great for teaching
 
#### Projects made in p5js
- [Pixel Weaver](https://www.sosolimited.com/work/pixelweaver/)
- [Creatability tools](https://experiments.withgoogle.com/collection/creatability) were prototyped in p5js
- [Melody mixer](https://melodymixer.withgoogle.com/ai/melody-mixer/view/)
- [Quick draw!](https://quickdraw.withgoogle.com/data) original R&D work was done in p5js before Google took it over
- [Synth app](https://ericrosenbaum.github.io/MK-1/)  for MakeyMakey
- Yuri Suzuki's [Sonic Pendulum](http://yurisuzuki.com/design-studio/sonic-pendulum)
- [Kinetic typography](https://timrodenbroeker.de/processing-tutorial-kinetic-typography-1/) by Tim Rodenbroeker

#### Alternatives to p5js

There are other **canvas plus** libraries out there that you might prefer over p5js. The fundamentals you will see are more or less the same, but different libraries have different features. p5js focuses on ease of use and follows on the rich tradition of the Processing community with lots of educational resources for designers.

- From the same people that gave the world [Scriptographer](https://scriptographer.org/) comes [paper.js](http://paperjs.org/) by Jürg Lehni and Jonathan Puckey.
- [konva](https://konvajs.org/) seems ok, but never used it
- [two.js](https://two.js.org/)
- [anime.js](https://animejs.com/) is specially designed for staggered animations

Unlike Processing, which can do 2D and 3D both fairly well, p5js is still very much a 2D library. There is work underway to improve WebGL support and make it 3D as well but there's no public release of this work yet at the time of this writing.

- Round, flat, designer-friendly pseudo-3D engine for canvas & SVG [Zdog](https://zzz.dog/)
- [Threejs](https://threejs.org/) is perhaps the best graphics library for the browser, hands down. It is also one of the most complex ones.
- [pex-gl](https://github.com/pex-gl/pex-gl)

## Cheatsheets

Before you proceed make sure you have these cheatsheets within reach, they will help you reference the things you need more quickly, so that you can follow the following sections more smoothly.

- [Cheatsheet](../../media/p5cheatsheet.pdf) for total beginners
- [Cheatsheet](../../media/p5js-cheatsheet_by_RyoSakai.pdf) by Ryo Sakai (a bit more complete)

## Your first half-hour with Dan Shiffman

Daniel Shiffman has been active in the Processing community as an educator for a very long time. He wrote some of the earliest books about making generative art in Processing. He is also the main star of The Coding Train youtube channel. This specific lesson will help you through the basics of p5js. We suggest you watch this video to get started.

<iframe width="560" height="315" src="https://www.youtube.com/embed/c3TeLi6Ns1E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Always open in a browser tab

As Dan Shiffman says, learning how to code is also learning how to read documentation. Most people that write code spend 90% of their time actually reading. Typing in the code is only a small part of the work of coding.

Have the [p5js reference documentation](https://p5js.org/reference/) open at all times, here's where you can find what each function is for, what it does, examples on how to use them, etc.

## A step by step course

If you think that the pace of this course is a little bit too much and need a more step by step introduction to the basics, with do-it-yourself coding exercises. Take the [CodeAcademy "Learn p5js Course"](https://www.codecademy.com/learn/learn-p5js) before you proceed with this.

## Help when you get stuck

There's a ton of documentation out there on p5js, a good starting point is the examples page in the official website [https://p5js.org/examples/](https://p5js.org/examples/).

You can also ask [ChatGPT](https://chat.openai.com). Really. ChatGPT knows p5js pretty well and can definitely help you when you are stuck and you need answers to your questions.

![chatgpt-p5js](../media/../../media/chatgpt-knows-p5js.png?resize=600)

## Workflow

We will be using various tools to write p5js sketches. Most of the example out there will be using the official p5js editor at [https://editor.p5js.org/](https://editor.p5js.org/)

During classes we will be using [codesandbox.io](https://codesandbox.io), since it is a much more complete coding environment and will keep us company not just for p5js work but for any proefessional web development work we might do in the future.

#### p5js editor

You can access the [p5js editor here](https://editor.p5js.org/), create an account for yourself so that you can save your experiments and come back to them later. *(in class we have experienced some issues with the p5js editor in Firefox, copy and paste not working for example, perhaps switch back to Chrome if you encounter this problem?)*

![editor](../../media/editor.p5js.org.png)

#### p5js: anatomy of a p5js sketch

p5js provides you with two major hooks to initialize and to paint in your sketch. When your sketch is loaded p5js will call the function called `setup()` if it exists in your sketch and it will execute it only once. This is a good place to create your canvas, initialize any objects or arrays that your sketch might need, basically any bootstrapping required by your sketch. If you draw anything here it will also be drawn, but it will only be drawn one, so this is not a good place to do animation.

p5js will then try to execute a function called `draw()` in your code, if it finds it, p5js will go into an infinite drawing loop executing this function at every frame. This is the right place to put your drawing and animation code.

![bracketed](../../media/bracketed.png)

For example this little program hooks onto both of p5js' default functions but doesn't do anything in either, nothing is shown as a result.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  // what you put in here will only be executed once at the beginning of your sketch
}

function draw() {
  // what you put in here will be executed once per frame
}
</script>

#### p5js: dealing with errors

Writing code is fairly tricky when you are not used to it. Many things can go wrong and throw off your browser. Do not fear, things get better with practice and generally you can't break anything when you code in Javascript, so you have the license to experiment. 

Every program must abide by strict syntax and grammar rules, a simple misplaced comma can cause your sketch to display an error message. Error messages are there to tell you what is wrong with your code. Read them carefully and try to decipher their meanings.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  cruateCanvas();
}
</script>

The important bit in this error message is the bit that says `cruateCanvas is not defined`, that means that we are trying to use something in our program called `cruateCanvas` that apparently doesn't exist.

Panic doesn't solve programming errors. Deal with errors with serenity. Time an practice will make interpreting these seemingly arcane messages second nature.

#### p5js: creating a canvas and drawing your first pixel

Before we draw anything in p5js, we need to create a canvas with a specific dimension. In this example we will create a canvas that is 300 pixels wide by 100 pixels high, and then give it a background color of pink so that we can see it clearly.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(300, 100);
    // paint it pink
    background(255, 0, 200);
}
</script>

Let's now draw a single point in space, at coordinates (0,0).

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
    // paint it pink
    background(255, 0, 200);
    // change painting color to black
    stroke(0);
    // draw a point at position (0,0)
    point(0, 0);
}
</script>

**Do it yourself:** try placing the dot in the center of the screen, then one third to the left and lastly one forth from the right edge. To calculate the positions you can use the `width` and `height` variables in p5js, which contain the dimensions of the canvas.

That dot is kind of hard to see isn't it? Move onto the lesson on basic primitives to get more visible results. 