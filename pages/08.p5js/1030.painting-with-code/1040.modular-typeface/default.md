---
title: Starting on the modular typeface exercise
---

In the same way that we use modularity in our typeface design to compose a larger set of letters from a simple set of shapes, we can do the same thing with our code.

If you only just staretd coding, it's very likely that your programs are only a few lines long. Programs have the tendency to grow large, sometimes very large, into the millions of lines of code. For the purposes of day to day design prototyping you will be dealing with sketches that contain from a few dozens to a low thousands lines of code. But writing your code in a modular fashion will help you make ir reusable across projects and you will progressively get better at this.

Let's look at how to approach modularity in Javascript using the modular alphabet as our guide. Let's start by drawing the A.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(255);
  fill(0);
  
  // place cursor at the center of our canvas
  translate(width/2, height/2);
  
  // x, y, width, height, start, stop
  arc(0, 0, 100, 100, HALF_PI-0.35, HALF_PI+0.35);
}
</script>

Ok, that was easy it only required us to draw one shape. Let's try to draw the letter B, seems a little more challenging. For our `b` we will use an `arc` and an `ellipse` drawn a little bit further down. We will tweak the numbers and positions untill it looks right.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(255);
  fill(0);
  
  // place cursor at the center of our canvas
  translate(width/2, height/2);

  // we will make our B letter using an arc and a circle
  arc(0, 0, 80, 80, 3*PI/2, PI/2, CHORD);
  ellipse(40, 40, 80, 80);
}
</script>

Observe that to draw one letter we needed two shapes, furthermore the relationships in the positions of the shapes are proportional, so if the `arc` has 80 as it's size, then the position of the circle that completes the `b` will be displaced to the right and down about half of the size of the `arc`, so the position of the circle will be `40, 40`, and the dimensions of both arc and circle will be the same `80`. We could abstract this into a single function and make or program more modular.

## The function

The most basic way of putting together things that belong together is by creating a function. Functions help us in two ways, they agglutinate functionalities that belong together into a single unit. Using the example above, we draw the letter `b` using two graphical elements, we can create a function called `draw_b` so that these two drawing operations can be grouped into one call, and then every time we need to draw a `b` instead of drawing the two elements in isolation, we can summon the power of our newly created function and that will do it for us. This ability is crucial in programming and it's where its true power really is. Let's look at how the `draw_b` functiona would look like.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  createCanvas(300, 300);
}

function draw_b() {
  // we will make our B letter using an arc and a circle
  arc(0, 0, 80, 80, 3*PI/2, PI/2, CHORD);
  ellipse(40, 40, 80, 80);
}

function draw() {
  background(255);
  fill(0);
  
  // place cursor at the center of our canvas
  translate(width/2, height/2);
  draw_b(); // then draw a b

  // place cursor 100 pixels to the left
  translate( -100, 0 );
  draw_b(); // then draw another b
}
</script>

Now we have encapsulated the two shapes needed to draw our letter `b` into a single function called `draw_b`, and we can now draw as many `b` as we like without having to repeat the `arc` and the `ellipse`, and because we are using relative positioning by calling `translate` and our `b` is always drawn starting at position `0, 0`, we can place it wherever we like.

Another powerful aspect of creating functions is that it allows us to parametrize it's functionalities. Our `draw_b` function saves us some typing work so far, but it's not very powerful yet. It always draws the same shape, in the same size and it relies on us calling translate first to place it in the right position before we draw it. We could parametrize these aspects of our `draw_b` function to make it more generic. Parametrizing in this case means turning some of the fixed *magic numbers* inside of our function into parameters that we feed to the function, so that every time we call it we can create a different version of our letter. In this case I am going to create a parameter called `size` that will determine the dimensions of our letter `b`.


<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  createCanvas(300, 300);
}

function draw_b(size) {
  // we will make our B letter using an arc and a circle
  arc(0, 0, size, size, 3*PI/2, PI/2, CHORD);
  ellipse(size/2, size/2, size, size);
}

function draw() {
  background(255);
  fill(0);
  
  // place cursor at the center of our canvas
  translate(width/2, height/2);
  draw_b(80); // then draw a b of size 80

  // place cursor 100 pixels to the left
  translate( -100, 0 );
  draw_b(35); // then draw another b of size 35
}
</script>

Amazing! Now we have a more generic `draw_b` function that can draw many different kinds of `b`, small `b`, medium `b` and large `b`.

Observe that to do this, I had to declare the parameter of the function between the parenthesis, and then I had to replace every instance of the number `80` in the previous version for the `size` variable name, and I also changed every instance of the number `40` for the expression `size/2`, or half the `size`. This preserves the relationship we had in the previous function and generalizes it to any size.

We could do this to every letter of our alphabet.

