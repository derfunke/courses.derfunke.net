---
title: Exporting your sketch
---

If you want to work in another medium, like for example print, but you still want your design to be generative  you can export your compositions in p5js to work them further in other softwares. You can export your p5js sketches to SVG files that you can then polish further in Affinity Designer or Inkscape. You can import that SVG into a lot of software used in fablab machines, such as vynil cutters, sticker printers, laser cutters, etc. So that you can give your designs a material twist.

Execute this script, see how it automatically downloads an SVG file containing the sketch.

<script type="text/p5" data-preview-width="250">
function setup() {
  createCanvas(200, 200, SVG); // Create SVG Canvas
  strokeWeight(1); // do 0.1 for laser
  stroke(255, 0, 0); // red is good for laser
  noFill(); // better not to have a fill for laser
}

function draw() {
  for (x = 0; x < 10; x++) {
    for (y = 0; y < 10; y++) {
      rect(x * 10, y * 10, x, y);
    }
  }
  save("grid-export.svg"); // give file name
  print("saved svg to disk");

  noLoop(); // we just want to export once
}
</script>
