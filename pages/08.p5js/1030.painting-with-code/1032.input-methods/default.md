---
title: Input methods
---

## Mouse

#### Mouse coordinates

Every sketch has two special variables that keep the current position of the mouse. The *X* position of our mouse is stored in the `mouseX` variable, and `mouseY` constains the *Y*. 

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  createCanvas(200, 200);
}

function draw() {
  background(255);
  line(mouseX, 0, mouseX, height);
  line(0, mouseY, width, mouseY);
}
</script>

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  createCanvas(200, 200);
}

function draw() {
  background(255);

  // draw lines from top and bottom of screen to mouse positions, spaced 10 pixels apart
  for(let xpos = 0; xpos < width; xpos += 10) {
    line(xpos, 0, mouseX, mouseY);
    line(xpos, height, mouseX, mouseY);
  }

}
</script>

Let's draw two circles at a fixed position and other two, smaller circles, that always move in the direction of the mouse. The effect should be that of two googly eyes following the mouse.

<script type="text/p5" data-autoplay data-preview-width="250">
let eyeSize = 30;
let angle;
let half = 200/2;

function setup() {
  createCanvas(200, 200);
}

function draw() {
  background(255);
  
  // left eye
  fill(255);
  ellipse(half - eyeSize, half, eyeSize * 1.8, eyeSize * 1.8);

  fill(0);
  angle = atan2(mouseY - half, mouseX - (half - eyeSize));
  leftEyeX = (half - eyeSize) + (eyeSize / 2) * cos(angle);
  leftEyeY = half + (eyeSize / 2) * sin(angle);
  ellipse(leftEyeX, leftEyeY, eyeSize / 2, eyeSize / 2);
  
  // right eye
  fill(255);
  ellipse(half + eyeSize, half, eyeSize * 1.8, eyeSize * 1.8);

  fill(0);
  angle = atan2(mouseY - half, mouseX - (half + eyeSize));
  rightEyeX = (half + eyeSize) + (eyeSize / 2) * cos(angle);
  rightEyeY = half + (eyeSize / 2) * sin(angle);
  ellipse(rightEyeX, rightEyeY, eyeSize / 2, eyeSize / 2);
}
</script>

Knowing when our mouse coordinates are inside of the boundaries of a UI element allows us to implement a primitive **hoover effect** functionality in our button.

Observe how the visual effect in this interaction is entirely determined by a change of color. In this second example I decided not to use black and white to draw the edges of the button and instead used different shades in the pink tint, from a bright pink to a dark, almost brownish tone. What effect does this have in how you perceive the button? How would these two buttons feel to the touch if they were objects and you could run your finger through them?

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
    // paint it pink
    background(255, 0, 200);
}

function draw() {
    // store the dimensions of our button in some variables to be used later
    let bh = 40;
    let bw = 120;

    // calculate coordinates for our button
    let tlx = (width/2) - (bw/2);
    let tly = (height/2) - (bh/2);
    let brx = (width/2) + (bw/2);
    let bry = (height/2) + (bh/2);

    let mouseIsInside = false;

    // find out if our mouse is inside of the button region defined by it's coordinates
    if( (mouseX > tlx) && (mouseX < brx) && (mouseY > tly) && (mouseY < bry) ) {
      mouseIsInside = true;
    }

  	let l = color(255, 200, 220);
  	let s = color(155, 60, 80);
  
    // draw light part of our button
    if(mouseIsInside) {
      stroke(s);
    } else {
      stroke(l);
    }
    line(tlx, tly, tlx+bw, tly);
    line(tlx, tly, tlx, tly+bh);
    // draw shaded part of our button
    if(mouseIsInside) {
      stroke(l);
    } else {
      stroke(s);
    }
    line(brx, bry, brx-bw, bry);
    line(brx, bry, brx, bry-bh);
}
</script>

#### Mouse wheel

Use your mouse wheel on the canvas in this sketch to observe how it affects the movement of our square.

<script type="text/p5" data-autoplay data-preview-width="250">
let pos = 25;

function setup() {
  // create a working canvas
  createCanvas(200, 200);
}

function draw() {
  // create a working canvas
  createCanvas(200, 200);
  background(237, 34, 93);
  fill(0);
  rect(25, pos, 50, 50);
}

function mouseWheel(event) {
  print(event.delta);
  //move the square according to the vertical scroll amount
  pos += event.delta;
}
</script>

The mousewheel serves different purposes when we are in different interfaces, and this can play with people's expectations. For example when we are browsing the web we expect the mousewheel to scroll up and down as we move it. But when we are in a zooming interface, like the one in miro.com, we expect the mousewheel to zoom in and out. Sometimes the mousewheel is used in combination with a modifier key such as <key>SHIFT</key> or <keyb>CTRL</keyb> to activate the zoom in/out functionality.

<script type="text/p5" data-autoplay data-preview-width="250">
let dim = 25;

function setup() {
  // create a working canvas
  createCanvas(200, 200);
}

function draw() {
  // create a working canvas
  createCanvas(200, 200);
  background(237, 34, 93);
  fill(0);
  rect(25, 25, dim, dim);
}

function mouseWheel(event) {
  print(event.delta);
  //move the square according to the vertical scroll amount
  dim += event.delta;
}
</script>

## Keyboard

All p5js sketches keep a special `key`  variable with the value of the last keyboard letter pressed. Click on the canvas and press a letter in your keyboard to see the result of this sketch.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  createCanvas(200, 200);
}

function draw(){
  background(32);

  fill(255);
  textSize(144);
  // the next line uses the variable 'key' where the last keypress is stored
  text(key, 80, 160);
}
</script>

In the following sketch we move the triangle around with the standard gaming keys <kbd>w</kbd>, <kbd>a</kbd>, <kbd>s</kbd> and <kbd>d</kbd>. In this example we use the `keyIsDown()` function call from p5js to identify if a key has been pressed. To determine which key we need to give that function a *keycode*, which is a number that identifies your key in a keyboard. To find out which keycode a key in your keyboard has you can use [this online tool](https://www.toptal.com/developers/keycode).

<script type="text/p5" data-autoplay data-preview-width="250">
let xpos, ypos;

function setup() {
  createCanvas(200, 200);
  xpos = width/2;
  ypos = height/2;
}

function draw() {
  background(40);
  noFill();
  stroke(255);

  translate(xpos, ypos);
  triangle(-10, 10, 0, -10, 10, 10);

  // handle key-down events
  // get keycode from https://www.toptal.com/developers/keycode
  if(keyIsDown(65)) {
    xpos -= 2;
  }

  if(keyIsDown(68)) {
    xpos += 2;
  }
  
  if(keyIsDown(87)) {
    ypos -= 2;
  }
  
  if(keyIsDown(83)) {
    ypos += 2;
  }
}
</script>


## Multitouch screens

Most touchscreens support multiple touch points so instead of a using a single variable like p5js uses for the mouse position, touch points are saved in a javascript list called `touches`. A list is *zero indexed* meaning the the first element in a list is at position `0`, so if we want to get the x position of the first finger detected in the touch screen, we need to access it like this `touches[0].x`, and if we want the `y` component of that position, we get it like this `touches[0].y`. To iterate through all the fingers currently touching the screen, we can use a for loop like in this example. 

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  createCanvas(200, 200);
}

function draw() {
  background(255);

  fill('magenta');

  // for each touch, draw an ellipse at its location.
  // touches are stored in a javascript list (also known as array)
  for (var i = 0; i < touches.length; i++) {
    ellipse(touches[i].x, touches[i].y, 50, 50);
  }
}
</script>
