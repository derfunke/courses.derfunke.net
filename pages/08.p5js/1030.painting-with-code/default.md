---
title: Painting with code
---

If you are reading this you probably have used some kind of image creation software before, tools like Adobe Illustrator, Inkscape or Affinity Designer. These are convenient interfaces streamlined for the creation of professional graphics, they provide us with tools such as brushes, vector creation tools, curves, etc, to create our compositions. Convenient as these tools are beneath each of these tools lay a series a common principles that underlay the whole of computer graphics. 

When we get to know these principles more intimately through code we open a whole world of possibility to make our work more dynamic, responsive and interactive. The aim of this module is to familiarize you with the fundamentals and learn them using a javascript library called p5js.


