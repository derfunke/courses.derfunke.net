---
title: Introduction and first circuit
---

If you missed the theoretical introduction in the classroom, you can catch up a little by [checking out the slides](https://docs.google.com/presentation/d/1NSnOREevM34UPDcJRtDyFvpk17cdMe43lZd7fGIYLvY/edit?usp=sharing).

## First session

In the first session we wired one sensor to our ESP32 board and wrote the code to read that sensor. Things discussed:
- serial protocol
- serial monitor / plotter
- data rates (refresh rate)
- communication speeds

<script src="https://gist.github.com/dropmeaword/b47591f8bce266f3120a6659e21536cc.js"></script>

