---
title: Basic actuation
---

Receiving an OSC message and triggering an actuator. Things discussed:
- relays
- visualizing data in touchdesigner

<script src="https://gist.github.com/dropmeaword/7c12aeda3af74fb0d51f99758fc1bb49.js"></script>

