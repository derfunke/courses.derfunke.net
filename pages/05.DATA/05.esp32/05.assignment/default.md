---
title: Assignment
---

Build a controller for a digital experience using the LDR circuit you built in the previous steps.

### References

- [Hypo-Chrysos](https://marcodonnarumma.com/works/hypo-chrysos/), Marco Donnarumma
- [Corpus Nil](https://marcodonnarumma.com/works/corpus-nil/), Marco Donnarumma
- [YOUbiläums Browser](https://www.jeffreyshawcompendium.com/portfolio/zkm-youbilaums-browser/), Jeffrey Shaw
- [IN_SIDE VIEW](https://www.jeffreyshawcompendium.com/portfolio/in_side-view/), Jeffrey Shaw
- [Legible City](https://www.jeffreyshawcompendium.com/portfolio/legible-city/), Jeffrey Shaw
- [Placeworld](https://www.jeffreyshawcompendium.com/portfolio/placeworld/), Jeffrey Shaw
