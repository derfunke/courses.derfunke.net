---
title: Building a controller with the ESP32
---

Over the coming sessions we will be building a device that will allow us to communicate with game engines, visual engines and DAWs to create new possibilities for physical interactions.

- [Part 1: introduction and first circuit](day1/)
- [Part 2: connecting your device to wifi](day2/)
- [Part 3: streaming the sensor data over the network](day3/)
- [Part 4: basic actuation](day4/)
