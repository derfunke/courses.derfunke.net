---
title: OSC
---

OSC is the secret sauce that will allow you to connect anything to anything, to go from the physical to the digital and back.

### Anatomy of an OSC message

Every OSC message is composed of an address, which is a string denoting a data stream, like for example the value of a sensor over time. And at least (but not limited to) one value, which would be the value of the sensor. This is easier shown by practice than it is to explain. For example, this is one OSC message:

```
/channel/light/one 144
```

In this message the address is `/channel/light/one` and the value is `144`. We can send multiple values over the same channel address, for example:

```
/channel/accel 0.234 0.4545 0.123
```

In this message the address is `/channel/accel` and the values are three floating point numbers `0.234`, `0.4545` and `0.123`.


### Unity: Send & Receive OSC

- Check out the brief ["getting started tutorial"](01.OSC_unity/default.md)

### TouchDesigner: OSCIn

- Get the [exercise demo files from this .zip](assets/osc_td_strobe.zip)
