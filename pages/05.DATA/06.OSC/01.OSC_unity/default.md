---
title: Using OSC in Unity
---

1. Download the [OscCore package](https://github.com/stella3d/OscCore/releases). This will allow you to send and receive OSC messages in your Unity project.
2. Create a new project in Unity, and from your downloads drag the `OscCore.unitypackage` file to your project's *Assets* folder.
3.  Notice how there's a new object in your hierarchy called *OSC Input*.
4.  If you click on that object and look at the Inspector you will see something like this. In the *OSC Receiver* section you can configure the port that Unity should listen to for incoming OSC messages. The sections bellow that show how can configure your message map to receive values for parameters, and they also allow you to map it to events in your scene.
![osccore1](assets/osccore_inspector.png)


### Converting OSC params to our light's position

We can use [this method](https://www.xarg.org/2017/07/how-to-map-a-square-to-a-circle/). Here's the javascript implementation of that method.

```
function map(x, y) {
	return [
		x * Math.sqrt(1 - y * y / 2),
		y * Math.sqrt(1 - x * x / 2),
	]
}
```

![polarcoord](assets/cartesian-polar-coordinates-conversion-formulas.png)
