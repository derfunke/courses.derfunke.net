---
title: Chindōgu - the art of the “unuseless”
taxonomy:
    category: 
        - artez
        - Y3
        - Y2
        - S2
        - 2016
---

Make five Chindōgu.

- Research what a Chindōgu is and its most salient characteristics? 
- Can an ‘app’ be a Chindōgu?
- Write a Chindōgu manifesto together.

### Wikipedia definition of Chindōgu
Chindōgu is the Japanese art of inventing ingenious everyday gadgets that, on the face of it, seem like an ideal solution to a particular problem. However, chindōgu has a distinctive feature: anyone actually attempting to use one of these inventions would find that it causes so many new problems, or such significant social embarrassment, that effectively it has no utility whatsoever. Thus, chindōgu are sometimes described as "unuseless" – that is, they cannot be regarded as "useless" in an absolute sense, since they do actually solve a problem; however, in practical terms, they cannot positively be called "useful".

![fig1](kenji.jpg?classes=caption "Kenji Kawakami with his Eye Drop Funnel Glasses™")

![fig2](noodle-cooler.jpg?classes=caption "The noodle cooler")




