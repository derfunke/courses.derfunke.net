---
title: Other consciousness
taxonomy:
    category: 
        - artez
        - Y2
        - S2
---
Each of us experiences the world in a slightly different way, differences in our bodies and our systems of perception, our senses, mean that we each perceive, act and react to things around us in slightly different ways.

For example, a person that had [Ménière's disease](https://en.wikipedia.org/wiki/M%C3%A9ni%C3%A8re%27s_disease) as a child will have trouble walking immediately after standing up for the rest of their lives and they will have to deal with vertigo every day. 

Blindness and deafness are considered “disabling” conditions while myopia or ezcema or paresthesia are not considered "disabling".

The conception of “another” that I am trying to convey here is not limited to physical condition or disease alone. “The Other” can be a human of a gender identity different to yours, is it possible to understand menstrual pain if you have never had it? Or convey the experience of a person born as male that doesn’t identify as male but rather as female? "The Other" can also be a non-human agency that you find interesting and from which we know something about how they perceive the world.

### Remarks and tips
This assignment has an important research aspect to it. You must present your research alongside your work at every step of the way.

I suggest that for the first part of your research you stay away from “hi tech”. Avoid computers, microcontrollers or anything that will require you to do “technical work” before you have explored “the otherness” that you want to convey. 

To get started, use your own body and your own experience to sketch. Use inhibition/stimulation, sub-threshold/saturation to understand how your senses work. What limitations do you encounter in trying to put yourself in the skin of the other? 

Eventually when you reach a deeper understanding of what you are trying to convey you can start concepting and designing with your usual tools.

### Mandatory reading
- “What is it like to be a bat?”, 1974, Thomas Nagel [download](https://www.sas.upenn.edu/~cavitch/pdf-library/Nagel_Bat.pdf)

### Further reading
- “The Man Who Mistook his Wife for a Hat”, 1985, Oliver Sacks [download](http://sajtichek.narod.ru/books/without_translation/wife_hat.pdf)
- “Spoon Theory”, Christine Miserandino's [download](https://cdn.totalcomputersusa.com/butyoudontlooksick.com/uploads/2010/02/BYDLS-TheSpoonTheory.pdf) [wikipedia link](https://en.wikipedia.org/wiki/Spoon_theory)

## Moodboard
![Upside Down Glasses, Carsten Hoeller](https://res.cloudinary.com/dqzqcuqf9/image/fetch/w_700,f_auto,q_auto:good,dpr_2.0/https://d2u3kfwd92fzu7.cloudfront.net/catalog/artwork/gallery/1325/CH09.11_upside5.jpg)
![The Machine to be Another](https://images.fastcompany.net/image/upload/w_1153,ar_16:9,c_fill,g_auto,f_auto,q_auto,fl_lossy/wp-cms/uploads/2014/01/3025388-poster-pstanded01.jpg)

### Examples
- [The Machine to be Another](http://www.themachinetobeanother.org/) [video](https://vimeo.com/84150219)
- [Living in a reversed world](https://www.youtube.com/watch?v=X5mjU3_vuvM) (example of how your vision works and how to “sketch” with “perceptual hacks”)
- “Upside Down Glasses”, Carsten Höller
- [A Breathtaking Journey](https://www.uu.nl/en/news/a-breathtaking-journey-increases-empathy-towards-refugees)
- [Sputniko! “Menstruation Machine”](https://www.youtube.com/watch?v=gnb-rdGbm6s)
- Karel van Laere “Paralysis”, “Largo” and “SLow” (all three works are part of the same research on quadriplegia)
- Francine Claassen: Products for a new identity
- Erwin Wurm: One minute sculptures
