---
title: Antenna Disguise
taxonomy:
    category: 
        - artez
        - Y2
        - S1
        - 2015
---

![fig1](antenna-cactus.jpg)

Download this document and have a good look at it [Border Bumping](http://borderbumping.net/archive/BorderBumpingUK.pdf)

This is research by Julian Oliver on disguises for GSM antennas. All these antennas are hiding as landscape or urban features. If you have a smartphone with you at this moment, you are carrying with you more than one antenna. What if these antennas had to be disguised in your body? What could they be like?

Sketch out a concept for a body-embedded antenna, think of the whole body, not just what is visible above the skin.

