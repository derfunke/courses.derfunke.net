---
title: Learnable bits
---

![goat](media/goat-interface.jpg?classes=caption "Goat contraceptive")

This will be a two-part assignment:

**Part 1** In the first half we will use a platform that is designed to teach programming to kids and learn how to use it ourselves. Be observant of how you use it, how is this different from what you have experienced with code before? What do I do when I use micro:bit vs. what do I do when I work in Processing for example.

Your assignment: In this assignment you are asked to develop a multiplayer game based on the micro:bit. The game must be playable by two or more players. You are free to make choices about the dynamics of the game.

![goat](media/analogsynth.jpg)

In the next three weeks, as personal research collect 3 or more examples of interfaces that you come across in this time. They can be digital, as in ocurring within your screen-based devices (computer, phone) or they can be physical like tape recorders, dishwasher. Our notion of Interface here is broad.

#### To deliver for this assignment
- The multiplayer game in a playable state
- Documentation of your research/observations of **at least 3 interfaces**

**Part 2** In part two we will look at ways of working with the observations and research you collect in part 1.

#### Getting the micro:bit

Get the bundle not just the board, the bundle contains a data USB cable and a battery pack with a connector that is ready to make your project "wearable".

- [KIWI electronics](https://www.kiwi-electronics.nl/bbc-micro-bit-go-bundel?search=microbit&description=true)
- [OpenCircuit](https://opencircuit.nl/Product/15215/BBC-microbit-Go-Bundel)


#### Suggested planning

| week | date | suggested planning | class feedback |
|:----:|:----:|:----|
| week 1 | sept 18 | get micro:bit, observe + document interfaces |  micro:bit intro |
| week 2 | sept 25 | ideate + prototype your game | working session |
| week 3 | oct 2 | **game presentation** |  micro:bit game can be played |
| week 4 | oct 9 | interfaces part 2 |  |
| week 5 | oct 16 | interfaces part 2 | |
| week 6 | oct 23 | interfaces part 2 | midterm  (group presentation) |

## Mandatory reading

- Bret Victor's [Learnable Programming](http://worrydream.com/LearnableProgramming/)

## The rabbit hole 🐰 ...fall into it
These are some optional things that you may follow through if you like:
- [p5js web editor](https://editor.p5js.org/)
- [XOD a visual programming language for Arduino](https://xod.io/)
- [Cables.gl](https://cables.gl)
- (yet to be released) [nodes.io](http://nodes.io/)
- (newly released and still not really functional) [Riggling](https://medium.com/@jtnimoy/drawing-in-rigglin-f7b26e916ed8) block-based programming interface for p5js

Work by Marcelli Antunez-Roca, as a performative understanding of the interface.

<iframe src="https://player.vimeo.com/video/26057904" width="640" height="512" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/26057904">REQUIEM 1999. Interactive Robot. One minute demo.</a> from <a href="https://vimeo.com/user3057767">Marcel&middot;l&iacute; Antunez Roca</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
