---
title: Olympiad of the Senses - Proprioception
---

## Things we need
- teacher: needs iPad to gather playlist
- teacher: needs bluetooth speaker
- students: bring comfy clothes that you can be on the floor with
- students: bring a good blindfold that you can be comfortable with for a long time

## Proprioception
Is the sense that allows our bodies to feel themselves.
- essential in allowing our bodies to understand how to navigate the space that they are in
- our muscles, bones, joints, hearing and touch come into play
- it helps complement our visual references, so one way to heighten our proprioceptive sense is by blocking our vision (sensory deprivation)
- for example there is a *cognitive dissonance* that involves proprioception when we are sitting in a train and the train next to us starts moving, our visual system interprets this as if we were moving, but our bodies (our proprioception) tells us that we are stitting still. Or when we read in a moving car, our eyes are telling us that we are standing still with respect to the letters on the page, but our bodies tell us that we are moving.
- motion sickness
- dancers
- learning (during childhood movement is a key aspect of how our brains assimilate information and learn to understand the world)
- proprioception helps us process non-visual information
- hearing (as in echolocation) plays an important role in proprioception, it tells us where our body is with respect to the space that we are in and makes us aware of dimension when we can't see
- when we use computers or other media that fix our bodies in one place our proprioceptive system is hardly used, so we process information differently, our understanding is not embodied
- vestibular system: haunted houses and virtual reality

#### How can we use it in design?

Understanding how to use proprioception as a tool allows us to better understand space and how our bodies navigate it.

It can help us in mapping an spatial experience and understand how the "experience feels", it can be used for example as a research tool to layout installation spaces, scenographies and exhibitions in ways that rely on other layers of experience that are not visual.

Sensory deprivation, deep listening, deep breathing and meditation can all help in temporarily heightening our proprioceptive sense.

It is a useful sense to map, understand, assimilate, create and enjoy experiences in architectural spaces.

## Session Plan
- **9.15** - the bodymap
- **9.20** - breathing together
- **9.30** - second body map
- **9.45** - Introduction to exercise: make pairs in which one is an *Angel* and another is a *Blindfold*
  - what will we do?
  - what is the role of the Angel?
    - guide the Blindfold: how do they do that?
    - Angel should speak as little as possible, you can only talk about colors, materials
  - what does the Blindfold do?
    - try to register the place you are in through touch, sounds, textures, temperatures, try to figure out if the place where you are is big or small, if it's crowded or not
- **10.00** - first exploration
- **10.20** - swap roles and go into second exploration
- **10.45** - assignment: make a Youtube Screening Tour that includes about 2 minutes of youtube video per location explored. Each film you select has to be related in some way to the space you experienced before.

(example: show the Tokyo metro video in a previously designated location)

  - keep it short, max. 2 minutes of video per location!
  - the place you experienced becomes a stage for that screening

<iframe width="560" height="315" src="https://www.youtube.com/embed/nmp-9x4gOeo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

----

- Rosa: [https://www.youtube.com/watch?v=f1XSUQD2c8w](https://www.youtube.com/watch?v=f1XSUQD2c8w)
- Daan: [https://youtu.be/G-Xsgj3-S1Q](https://youtu.be/G-Xsgj3-S1Q)
- Erik: [https://www.instagram.com/p/BoV7FxFgn6K/?igshid=d6x81hhb8ku3](https://www.instagram.com/p/BoV7FxFgn6K/?igshid=d6x81hhb8ku3)
- Wessel: [https://www.youtube.com/watch?v=wBqM2ytqHY4](https://www.youtube.com/watch?v=wBqM2ytqHY4)
- Kun: [https://youtu.be/j1gtj00CeVs](https://youtu.be/j1gtj00CeVs)
- Berend: [https://www.youtube.com/watch?v=E-eCfaGpyX8&feature=youtu.be](https://www.youtube.com/watch?v=E-eCfaGpyX8&feature=youtu.be)
- Jippe: [https://youtu.be/Lr80Mdk03vo?t=122](https://youtu.be/Lr80Mdk03vo?t=122)
- Katrijn: [https://vimeo.com/170584129#t=0m8s](https://vimeo.com/170584129#t=0m8s)
- Seline [https://youtu.be/UknC0fXYzXU?t=12](https://youtu.be/UknC0fXYzXU?t=12)
- Francisco: [https://youtu.be/Tw-kFH1OLuU?t=0m10s](https://youtu.be/Tw-kFH1OLuU?t=0m10s)
- Axe_afrika: [https://youtu.be/tzqbxo3PfTA](https://youtu.be/tzqbxo3PfTA)
- Ksenia: [https://www.youtube.com/watch?v=-gi2P-FGgKg](https://www.youtube.com/watch?v=-gi2P-FGgKg)
- Nicky: [https://www.youtube.com/watch?v=vD4cXWrfE7o](https://www.youtube.com/watch?v=vD4cXWrfE7o)
- Chaeny: [https://www.youtube.com/watch?v=hcHBZ3Ram7k](https://www.youtube.com/watch?v=hcHBZ3Ram7k)
- Aafje: [https://youtu.be/I_A_wW6th6c](https://youtu.be/I_A_wW6th6c)
- Hugo: [https://youtu.be/fTmywp1jItU](https://youtu.be/fTmywp1jItU) hugo
- puck [https://youtu.be/wfg0gtcqBz8?t=114](https://youtu.be/Ti3UL_mVHHI?t=220)
- Boudewijn [https://youtu.be/Ti3UL_mVHHI?t=220](https://youtu.be/Ti3UL_mVHHI?t=220)
- Miguel: [https://youtu.be/QzjqGJbycKA](https://youtu.be/QzjqGJbycKA)
- Enrico: Enrico [https://youtu.be/P5izhbb8zqI](https://youtu.be/P5izhbb8zqI)
- Bart Hasselbink: [https://youtu.be/n-wEvzqdDZg?t=22](https://youtu.be/n-wEvzqdDZg?t=22) - Michiel: [https://www.youtube.com/watch?v=nBuJUPWRLwE](https://www.youtube.com/watch?v=nBuJUPWRLwE) 
- Manali: [https://youtu.be/wWWR_YYWoOU?t=441](https://youtu.be/wWWR_YYWoOU?t=441)      
- ??? [https://youtu.be/2ZVm1wJ7UFE](https://youtu.be/2ZVm1wJ7UFE)


Chico de precious plastics:
- a.coira.iglesias!gmail.com