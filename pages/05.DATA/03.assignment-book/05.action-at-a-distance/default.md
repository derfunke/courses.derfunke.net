---
title: Spooky action at a distance
taxonomy:
    category: 
        - artez
        - Y2
        - S1
        - 2019
---

From the ​bathyscaphe​ used to explore the depths of the sea, to the ​telechiric machines envisioned for exploring other planets, to today’s drones that locate, spy upon and kill people across continents. Or robots that are sent into contaminated areas such as the Fukushima disaster zone or to defuse dangerous bombs. Or even lab equipment to observe and manipulate things that cannot be touched, like radioactive materials, or even seen with the naked eye, like microbes. Humans have always had the need to employ machines to explore the unknown without really “being there”.

In this assignment you will be asked to design a machine, experience or protocol that can result in a manipulation in one room, let’s call that the “control” room, taking effect in another. The effect of this control will be felt in another place, at a distance, let’s call that the “effect” room. You can use any method you like to connect the two.

As you work on this I’d like you to keep the following things in mind:

- If I am the “operator” in the control room, my body is not present in the place
where the action is happening. What kinds of feedback do I get from the "effect" room?
- Think about the word “distance” openly, you can interpret it in many ways: what
is “distance” in your design? Is it physical distance measured in meters or centimeters or perhaps kilometers? Is it “distance” in size or scale? (for example, an action that I perform with my hand at human scale, but that has to have an effect in a microscopic environment, like for example sucking the DNA out of a bacteria). Or is “distance” perhaps a distance in “time”, so something that I do now but has an effect 20 minutes later or perhaps a day or a year later.
- The word "room" here is a metaphor, I use it to indicate not just a place in space, but also a space in time or scale.

### Inspiration board

![fig1](bathyscaphe.jpg?classes=caption "Don Walsh and Jacques Picard inside the deep-diving research bathyscaphe Trieste during their exploration of the Mariana Trench on January 23, 1960")

![fig2](RIVET-roman-manipulator-harwell-x640.jpg?classes=caption "1967 – RIVET (Remote Inspection VEhicle Telechiric) – Hugh A. Ballinger")

![fig3](bell-remora-manipulator-x640.jpg?classes=caption "1960 – “REMORA” Manned Space Manipulator – Bell Aerosystems")

![fig4](Mobot_getty50553581-x640.jpg?classes=caption "1964 – MOBOT Mark II – Hughes Aircraft")

### References
- watch: [Extra Credits on Spooky Action at a Distance](https://www.youtube.com/watch?v=sITpTx5GL-w)
- web: [About the RIVET telechiric machines](http://cyberneticzoo.com/robots/1967-rivet-remote-inspection-vehicle-telechiric-hugh-a-ballinger-british/)
- book: "Drone Theory", Gregory Chamayou
