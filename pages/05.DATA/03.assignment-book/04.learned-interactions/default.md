---
title: Learned Interactions
taxonomy:
    category: 
        - artez
        - Y3
        - Y2
        - S2
        - 2019
---

Design a product that learns from usage, then use Wekinator to prototype it. Build all the inputs and outputs you need to demonstrate the interaction with your product.

## Planning
The assignment is 7 weeks long.
- week 1: intro + basic knowledge 
- week 2: build controller
- week 3, 4: project coaching
- week 5: intermediate presentation 
- week 6: project coaching
- week 7: presentation

To get the most out of this assignment I suggest you use the first week of this planning to follow Rebecca Fiebrink's cource in Kadenze, you must complete the first module before we meet again.

This is the course [Machine Learning for Musicians and Artists](https://www.kadenze.com/courses/machine-learning-for-musicians-and-artists-v), Kadenze wants you to create an account to follow the course.

## Reference software
- [Wekinator](http://www.wekinator.org)
- [Wekinator ready-made inputs and outputs](​http://www.wekinator.org/examples/)

### About Neural Networks
- video: [Neural Network visualization](https://www.youtube.com/watch?v=3JQ3hYko51Y)
- video: [But what is a Neural Network​](https://www.youtube.com/watch?v=aircAruvnKk)

### Interaction projects built with Wekinator
- [Objectifier, Bjørn Karmann](https://bjoernkarmann.dk/objectifier)
- [Pour Reception](​http://www.toreknudsen.dk/work/pour-reception/)
- [Classifyer](http://ciid.dk/education/portfolio/idp17/courses/machine-learning/projects/classyfi er/)