---
title: Assignment book
page-toc:
  active: true
---

Over the years I have developed a number of assignments with my students covering a range of their needs. Some of these assignments are thought out to address a specific technical skill, others are more about developing a healthy process, and others are about stimulating the creative muscle through fast prototyping.

Typically the result of these assignments is presented in class in a mini-exhibition where we all participate in a crit session together.

Some sound-bitey quotes to accompany you in the execution of these assingments:

- "Practice is the best of all instructors" good ideas come from practicing, not just from thinking
- "An agreeable companion on a journey is as good as a carriage" look to your classmates for help too
- "While we stop to think, we often miss our opportunity" sometimes you need to take a leap of faith
- "When two do the same thing, it is not the same thing after all" even if your idea has been done before you will still do it differently
- "The bow too tensely strung is easily broken" don't get too stressed out, give yourself time to try things out and experiment without pressure

All of these are from Plubius Syrus. (42 B.C.)

- “Perfection is achieved not when there is nothing more to add, but when there is nothing more to take away”  — Antoine de Saint-Exupery
