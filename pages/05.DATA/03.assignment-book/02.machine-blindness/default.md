---
title: Digital Invisibility
taxonomy:
    category: 
        - artez
        - Y2
        - S2
---

Computer Vision (or CV for short) is a set of technologies that allow computers to discern points of interest in a digital image. These points of interest can be landscape features such a hedge separating two arable fields, the numbers in a car license plate or a face. Computer Vision has many applications Google for example uses face recognition in their Google Maps product so that when their algorithms detect a face it is blurred as to not invade the privacy of passers by, it is also used to read the numbers in street doors to increase the accuracy of their map search engine. Facebook uses face detection to encourage you to tag people that appear in your pictures. Most traffic authorities in Europe have some kind of computer vision based system that allows them to automate fines for cars speeding on highways. More recently in the Netherlands, Cition, the company in charge of parking ticket control in a lot of Dutch cities, have started using car-mounted camera arrays and computer vision to design a drive-by parking infraction detector. CV is used by drones to detect lock into targets and identify points of interest in a video feed. CV is used extensively in the agricultural industry from self-driving tractors to detecting when a chicken egg is fertilised or not, or an apple is fit for sale in the supermarket or not.

Most of these CV algorithms are used in decision-making systems, do I let this apple through or not? do I give a fine to this driver or not? is this car well parked or not? is that bunch of pixels a person or not?

However one thing is known, Computer Vision isn’t perfect. It glitches-out in certain conditions and depending on the system it has false-positive rates as high as 15%. That means, for example, that 15 cars out of 100 are getting fines that shouldn’t be getting them. Most computer-based decision systems use some form of statistical modelling, this means that the input image is divided in numeric parameters denoting probabilities of something being “in it” or not (e.g. is that number a “five” or not?). The output of these algorithms is normally a percentage of probability that “the thing” that the algorithm is looking for in an image has been detected or not. Given this output all computer-supported decisions can fall within one quadrant of what is called a Confusion Matrix:

 Another aspect of CV is that even though it is embedded in many aspects of our lives, it is practically invisible to us. We hardly ever see it at work, most people doesn’t know how it works, we can’t really know which systems use it and which systems don’t. This technology is always looking at us, at our pictures, at our cars, at our streets, our apples and our eggs, but we hardly ever see it.

In this assignment we are going to explore ways to try and invert this logic. How can we become invisible to the eye of Computer Vision? How can we make our apples and our eggs move from one quadrant of the *Confusion Matrix* to another?

![confusion matrix](confusion-matrix2.jpeg)

Your design brief
Invent your own strategy to beat a face-detection algorithm into producing unexpected outcomes within the Confusion Matrix. This would be for example, detecting a face where there is none, or not detecting a face where there clearly is one.

Your design must function within everyday life and you can use any approach you like.

Your goal is to become invisible to the CV algorithms or at least confuse them to the extent that they take you for something else.
