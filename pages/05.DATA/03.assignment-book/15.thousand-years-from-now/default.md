---
title: One Thousand Years from Now
---

Choose a functionality or app of your mobile phone and design an equivalent that could be used 1.000 years from now.

#### Considerations
- You cannot rely on written instructions (3K years from now people might not be able to read English or Dutch)
- Don’t make assumptions about what people might know, it is common for a person in a western country today to know what an iPhone is but one thousand years from now we cannot assume that that knowledge will be as widespread.
- The operation of your design must be as self-evident as possible

