---
title: Dumb Scrap / Smart Scrap
taxonomy:
    category: 
        - artez
        - Y2
        - S1
        - 2016
---

### Part 1: Dumb Scrap

There’s enough scrap materials to make the world all over again. One of the first institutes in Europe entirely devoted to research in Interaction Design, the Domus Academy in Ivrea, was housed in a building of the italian computer manufacturer Olivetti. Olivetti’s scrapyard was a place where researchers sourced many of the materials for their prototypes.

In this assignment we will explore the hidden potentials of scrap material. We will be looking at a specific kind of scrap material that is easy to find. Car parts.

Investigate the electrical system of a car and identify some of its parts and what they do. Parts like for example the windshield wipers, horn, lock mechanism, head lamps, fans, soap pump, radio aerial, etc. These are some of the electrical parts in an old car, of course most modern cars have more complex electrical systems.

![car electrical system](car_electrical_system.png)

Call the nearest *autosloperij* and make a deal with them to get some of these parts. 
Test them, make sure they work to some extent and that you can make them go on and off.

They will of course try to sell them to you, try to get a sharp discount by saying that it is ok  they are from a very old car and that it is for a school assignment. You are less likely to be able to get parts for free that belong to a car that is still in circulation. 

### Part 2: Smart scrap

In the last twenty years there’s been a transition in technology that has affected pretty much all the devices around us from the fridge to the car, the change is mostly related to the introduction of programmable digital electronics. This change has brought about the age of the smart-fridge, the smart-phone, the smart-car and very nearly smart-everything. What distinguishes the smart from the dumb? One aspect of smart-things for example is the capability to be programmed and to adapt to new circumstances through software.

In this second half of the assignment we will turn into smart car parts what were dumb car parts before.

Working in pairs choose one of the scrap devices of the previous assignment. Using an arduino, learn how to operate that device and program it. Make your project react to something, it can be proximity (with a distance sensor), movement (with a camera), light or absence of light (with a light sensor), etc.

Once your part is somewhat programmable, connect that new smart device that you just created to a sensor so that it can react to the world around it. Try to choose a relationship between what the sensor senses and what your smart device does so that there is some kind of poetic closure in that relationship. Think of form as well, if you use the car lights for example, do not leave them hanging-off, design an enclosure for them or place them in a way that is consistent with the look of your project.

