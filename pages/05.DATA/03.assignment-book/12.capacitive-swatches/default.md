---
title: Capacitive Sensing Swatches
taxonomy:
    category: 
        - artez
        - Y3
        - S1
        - 2017
---

![fig1](swatch.png?classes=caption "A fabric swatch")

If you have followed the session on “Physical Computing essentials”, the materials of which you can get here: https://gitpitch.com/IDArnhem/physcomp-essentials you will also have done your first capacitive sensing circuit.

Capacitive sensing is a tricky sensing modality. On the surface the technique seems very very simple, but the magic depends very much on the material you use for the sensing.

For next week, use your simple circuit and investigate materials that you can use and sketch out possible interactions that make sense with those materials. Please each bring five swatches resulting from your material research so that we can try them out live in class.

Make sure that nobody else in your class is working on the same swatch, only bring the ideas that have turned out to be unique. Let’s not all bring one banana each!

### Format
Standardize on the swatch dimensions, make them into a piece of board that is 300x200mm. Let that board have a single connection out to the outside world (capacitive sensors only need one wire), in the center at the top, so that we can just plug our testing circuit with an alligator clip directly onto the swatch. You can use a little bit of copper tape for that connection. Keep notes on how to make that swatch so that somebody else can replicate it.

Hints for material experimentation: 
- salt makes water/liquids/jelly conductive
- graphite is sold as a powder in art shops and adding it to other things will make them conductive too, a single pot of graphite is enough for all of you to run multiple experiments.  
- Share costs.
- If your material can rot or corrode take note of that and think of ways of preserving the swatch.
