---
title: Situational Awareness
---

Every action we carry out on a digital device triggers a chain reaction of events often involving many different parties. For example, from Trackography we learn that visiting the website of a regional newspaper will facilitate a bunch of companies (mostly American) to obtain some information about us. Who is affected by this? What consequences does it have?

This happens transparently without most of us ever really noticing. As users we are blind to the actual flows or information and where responsibilities change hands, where the liminal zones are. The whole online experience has been made seamless through thoughtful design. Yet it is this seamlessness that makes it possible for a middle-person to go unnoticed.

In this assignment we will work on designing means to improve situational awareness while browsing the web.

Situational Awareness is the ability to identify, process, and comprehend the critical elements of information about what is happening around you during an activity. It is the things that happen around you while you are focused on something. More simply, it's knowing what is going on around you.

#### Suggestions
- Look at frontiers, borders, systems and other liminal zones... when change happens how can we bring that change into the activity that we are focusing on? (e.g. competition tennis courts are equipped with a system that beeps when the ball bounces outside the lines)
- Look for real-life analogies that could help communicate change. (e.g. reflectors help make the road visible when day changes to night)
- Think about how you browse the web, what devices you use to browse, is your situational awareness any different depending on which device you use?
