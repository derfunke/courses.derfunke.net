---
title: Free Haven
---

During this project we will explore ideas around community, freedom, openness, control and consent in relation to contemporary technology. Specifically software and the tools we use as creators.

To explore these ideas we will look at open source communities, digital graffiti, the american counterculture of the 60s, European Punk, DIY (do-it-yourself) culture as well as current trends like crypto and solarpunks and will look at how these phenomena help shape digital culture.

We will finish with an exhibition of the work produced during the semester on the 10th of June.

## Lesson plan

We have about 16 weeks long with 2 breaks in between.

### Feb 5: Freedom to change
- What do we mean when we say free?
- Exercise: what is freedom?
- Free Software (the 4 freedoms)
- What is Open Source?
- software piracy

Materials:
- [Stallman on freedom of software](https://hackcur.io/reinventing-freedom/)
- [The Difference Between Free and Open-Source Software](https://www.digitalocean.com/community/tutorials/Free-vs-Open-Source-Software)
- [The Open Source Guide](https://opensource.guide/)

##### Reading
- Mandatory reading for next week: BROAD BAND, Claire L. Evans, [Chapter 9: Communities](https://zilog.stackstorage.com/s/NLSPwKDAU6lThQJ)
- Optional: [What are the esential elements of a open source project/community?](https://opensource.guide)

### [Feb 12: Community (pt. 1)](free-haven/online-communities)
- Discussion on reading
- Stacy Horn and ECHO (video)
- MUDs and BBSen with practical exercise
- Demoscene (talk)

### [Feb 19: Community (pt. 2)](free-haven/demoscene)
- ~e-textile~
- cryptorave
- algorave
- camps, hackathons and parties
 
Mandatory reading for this session: [Interview Omsk Social Club + Bitnik](https://networkcultures.org/moneylab/2019/11/11/cryptorave-an-interview-with-omsk-social-club-mediengruppe-bitnik-by-maisa-imamovic/)

Optional:
- [Cryptorave reader](https://0b673cce.xyz/cryptorave_reader_2019.pdf)
- [Omsk Social Club at The Influencers](https://vimeo.com/303859212)

## _(( winter break ))_

### [March 4: Counterculture](free-haven/zines)
- DIY Punks
- Fanzines
- Short **assignment**

Tools:
- [Worms, Butterflies and Dandelions. Open source tools for the arts](https://medium.com/@tchoi8/worms-butterflies-and-dandelions-open-source-tools-for-the-arts-9b4dcd76a1f2)

### March 11: WEC workshop
With guest: Garret Lockhart which will talk about the Mudflats community in British Columbia.
- Whole Earth Catalogue
- Workshop: WEC scan&copy

### March 18: IDA goes remote (day 1)
- check-in with everybody
- Discord install and brief tutorial
- Minetest install and tutorial
- Take a group photo in Minetest
- Reflection
- Share movie night on Discord
 
### The following sessions are on-hold

```
### March 18: Authority & Exposure
- Steal this book
- .COM, film screening "We live in public"
- Discussion: living exposed in digital communities
- Shared and Safe spaces
- Conflict
- Codes of conduct
- A CoC for IDA

### March 25 (midterm grading)
- Deliver assignment with launch event

### April 1: Version control
- **assignment** for last block
- tools for collaboration
- version control
- git
```

### April 8: Autonomy
- Fuck Off Google
- Whose cloud?
- Degooglify
- Cloud Emancipation
- Consentful Tech
- Self-hosting
- homebrew server

### April 15: Self-hosting
- How and what?
- Alternatives to everything
- Homebrew server

### April 22: Radical decentralization part 1
- Homebrew server
- work on assignment

## _(( spring break ))_

### May 6: Radical decentralization part 2
- Bitcoin
- Distributed web
- IPFS
- Dat protocol
- work on assignment

### May 13: Radical decentralization part 3
- Secure Scuttlebutt
- Solar punk
- work on assignment

### May 20: Radical decentralization part 4
- work on assignment

### May 27
- work on assignment

### June 3
- work on assignment

### June 10
- grading week
- exhibition
- Schouw will be June 19

## Book reproductions

There are two *cult* books that are part of this block that are not so easy to find in original form or they are too expensive because over the years they have become collectors items.

Let's try and reproduce them as [facsimiles](https://en.wikipedia.org/wiki/Facsimile) to the best of our ability this semester.

- [Computer Lib, Ted Nelson, original 1974 edition](https://zilog.stackstorage.com/s/FQWvBQOuUbVMY6M)

"Physically, the book is classic ‘70s underground cult: the whole thing had been typed and pasted up with hand-written headings and drawings. Page number cross-references were blanks filled in by hand. The book itself is oversized (almost 11×14 inches), with the two halves back-to-back, one upside-down from the other. The text of the book shows the author’s hypertext focus: it isn’t one (or two) books so much as it is a series of poster-sized essays on various topics that make up the electron cloud around the book’s subject. (The 1987 revised edition is classic ‘80 laser-printer chaos, and is not nearly as nice to read: the dimensions have been scaled down resulting in text narrowly wrapped, and awkwardly flowed from page to page)." ( [source](https://nedbatchelder.com/blog/200301/computer_libdream_machines.html) )

- [Steal this Book, Abbie Hoffman (25th anniversary edition)](https://zilog.stackstorage.com/s/1ntGGP7jYdNrjqK)

## Reference materials

- [BBS: The Documentary](https://www.youtube.com/watch?v=nO5vjmDFZaI), Jason Scott
- [Adobe is cutting off users in Venezuela due to US sanctions](https://www.theverge.com/2019/10/7/20904030/adobe-venezuela-photoshop-behance-us-sanctions)
- [Inessential Weirdness of Open Source Software](https://www.harihareswara.net/sumana/2016/05/21/0)
- [There Is No Software](http://www.ctheory.net/articles.aspx?id=74), Friedrich Kittler
- [Consentful Tech Zine](https://www.consentfultech.io/)
- [Progressive Hack Night Code of Conduct](https://progressivehacknight.org/culture/2017/07/01/code-of-conduct.html)
- [P5.js Code of Conduct](https://github.com/processing/p5.js/blob/master/CODE_OF_CONDUCT.md)
- [Code Newbie CoC](https://www.codenewbie.org/blogs/our-code-of-conduct)
- BROAD BAND, Claire L. Evans, [Chapter 9: Communities](https://zilog.stackstorage.com/s/NLSPwKDAU6lThQJ)
- "Tyranny of structurelessness", Jo Freeman
- [OSSTA-Zine](https://github.com/galaxykate/OSSTA-Zine)
- [Bring Kindness Back to Open Source](https://www.hanselman.com/blog/BringKindnessBackToOpenSource.aspx)
- [After Years of Abusive e-mails, the creator of Linux steps aside](https://www.newyorker.com/science/elements/after-years-of-abusive-e-mails-the-creator-of-linux-steps-aside)
