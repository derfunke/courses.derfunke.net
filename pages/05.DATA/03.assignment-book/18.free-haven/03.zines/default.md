---
title: Zines and DIY cultures
---

A fanzine is a non-professional and non-official publication produced by enthusiasts of a particular cultural phenomenon for the pleasure of others who share their interest. Even though fanzines go way back and have existed for a long time, they have a strong tradition among counter-cultural movements a lot of science fiction and poetry work was first published in fanzines. Zines also have a strong presence in cultures with strong DIY tendencies such as punks, goths, hippies and hackers where self-publishing, self-reliance and community building where important values.

These days Zines are becoming big again, specially around contemporary topics of exclusion in online communities. But also as a means to find alternative, less normative forms of expression outside of social media, net artists that are experimenting with print, etc. A way of presenting work face to face to an audience of like-minded people without the frame imposed by sites like Instagram.

The [Tech Zine Fair](http://techzinefair.org) takes place in New York and gathers zine makers including artists, designers, digital rights advocates and various other organizations.

The [CtrlZ.AI](http://www.ctrlz.ai/) zine fest takes place in Barcelona focused on technology and artificial intelligence.

## Assignment

![](media/assignment.png)

## Reference works

These are some zines (some interactive, some not) that I want you to look have a look at as reference works for this assignment.

Other zines that you can check out:
- [Encoding by Aarati Akkapeddi](http://aarati.me/project.html?project=project-encoding)
- [Grifting Amazon by Unknown Unknowns](https://www.unknownunknowns.org/product/grifting-the-amazon)
- [How Does the Internet Zine by Bubble Sort Zines](https://shop.bubblesort.io/products/how-does-the-internet-zine)
- [Ways of Being Online](https://ways-of-being-online.hashbase.io/)
- [Julia Evans great resources on tech zine making](https://jvns.ca/)
- [Printjob Publishing](https://printjob.press/)
- [Run Your Own Social Network - Darius Kazemi](https://runyourown.social/)
- [Hack the Planet zines](https://github.com/nachash/htp-zines)
- [Archive of Hacking zines](https://github.com/fdiskyou/Zines)
- [Political zines](https://github.com/rechelon/zine_library)
- [Cat e-zine](https://github.com/tenderlove/e-zine/blob/master/1.pdf)

The work of Nathalie Lawhead aka [@alienmelon](https://alienmelon.itch.io/). She's a net and game artist with a unique style, she makes fun stuff.
- ["Everything is going to be Ok"](http://unicornycopia.com/unicornycopia.html) by Nathalie Lawhead is an interactive game-like fanzine.
- She's got a game on anatomically incorrect dinosaurs.
![upsidedown](https://img.itch.zone/aW1hZ2UvMzM2NjkvMTQ0ODY5LmdpZg==/original/DDAMdu.gif)

## Resources for zine makers

This is a list of free (libre) and open source resources for zine makers.

#### Typography
There are quite some libre typefaces out there, here are some to help you get started.
- [Open Source Publishing Font Foundry](http://osp.kitchen/foundry/)
- [Inter Font Family](https://rsms.me/inter/)
- [Collletttivo Foundry](http://collletttivo.it/)
- This is a [compilation made by Frederick Brodbeck on open source typefaces on the web.](https://www.are.na/frederic-brodbeck/open-source-typefaces)

#### Tools
Here are some open source tools that you can use for your zine:

[Inkscape](https://inkscape.org/) - is a vector drawing tool, somewhat like Illustrator. You can create PDFs and SVGs quite easily with it. If you are running on mac osx, you will need to download  [1.0 beta version](https://inkscape.org/release/inkscape-1.0/?latest=1), the 0.92 will not work on the latest version of OSX.

[Krita](https://krita.org/en/) - is a drawing tool. Good for illustration work, game art, and pixel manipulation stuff.

[GIMP](https://www.gimp.org/) - is a photo manipulation software, if you are familiar with Photoshop. GIMP has similar capabilities. It feels like an old version of Photoshop 6.

[Blender](https://www.blender.org/) - open source graphics kitchen sink, it is popularly known as a fairly advanced 3D modelling tool, but it can do a lot more than that, you can also do illustration work, animation and video editing with Blender.

[Godot Engine](https://godotengine.org/) - is a fairly great free game engine with a great editor, if you like Unity you will find a good sparring partner in Godot. Godot is actually easier to use than Unity and it's great for putting up together a simple game. 

[Electric Zine Maker](http://unicornycopia.com/ezm/) While this one is technically speaking not Open Source or Libre Software, I still include it because it is an alternative custom tool and because it's cool.

### Swap Party & Zine Fests

Swap parties or zine fests are events where zine makers gather in person and setup their own little display table to share the fruit of their work.

![swapparty](media/brooklyn_swap_party.gif)

# Postpunk

Punk had a strong influence in the arts, music and fashion and evolved in parallel to the nascent hacker cultures that were taking root in USA and Europe at the time. Perhaps the strongest imprint was felt in a radical culture of autonomy, independace and DIY (do it yourself) culture. This manifested in many ways, *Goth* bands and fans would make their own clothes as their own way of departing from what was commercially available. Punk fans would do their own publications in the form of fanzines.

Bauhaus.

![bauhaus](media/bauhaus-02.jpg)

![jdivision](media/joy_division.jpg)

Siousxie and the Banshees.

![siouxie](media/siouxie.jpg)

Karl Lagerfeld at Le Main Bleu.

![lagerfeld](media/1977_lagerfeld.jpg)

### DIY 

![sideburn](media/sideburn1__form_a_band.jpg?width=500)

“There’s an illustration from a fanzine called Sideburn #1, which was a drawing made by Tony Moon just to fill the space. It’s a drawing of three guitar chords and it says, ‘now form a band’. That fanzine is extremely rare, but the drawing is often quoted by lots of musicians as the impetus to do something, and it’s seen as a key message of punk,” says Toby. “You didn’t need to have been to music school or be particularly proficient or skilled. It was much more about the energy and drive to do something. It’s a rallying call to the troops.”

( [source](https://tumblr.austinkleon.com/post/152691534221) )


## Fall into the rabbithole 🐰
- [History of Le Main Blue Club](https://daily.redbullmusicacademy.com/2017/09/la-main-bleue-feature)
- Nathalie Lawhead once gave this amazing talk on [Fish Simulators](https://www.youtube.com/watch?v=55qQbAGF2zE) recommended watch.
