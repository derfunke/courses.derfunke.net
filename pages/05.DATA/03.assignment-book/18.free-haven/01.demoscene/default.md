---
title: Demoscene
---


## Running demoscene stuff on your mac

Most of all the demoscene content doesn't run on today's computers. 

1. [Download the DOSBox](https://sourceforge.net/projects/dosbox/), MS-DOS emulator.
2. Create a folder in your home directory called `MSDOS`


## Music trackers

This the [MilkyTracker manual](https://milkytracker.titandemo.org/docs/MilkyTracker.html)


What a track is made of.

Each note in a track is made out of 10 characters, the first three at for the pitch, the next two are for the instrument number, the other two are volume and the last three are the effect to be applied to that instrument. 

```
C-4 ·1 ·· 037
··· ·· ·· 037
··· ·· ·· 037
··· ·· ·· 037
```


