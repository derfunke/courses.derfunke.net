---
title: Museum of Happy Phones
taxonomy:
    category: 
        - artez
        - Y3
        - S1
---

After going through the chapter on [Microinteractions](/topics/microinteractions), think of where you encounter these every day. Microinteractions are normally simple and we barely notice them, we do them without thinking much. 

> This video posted to social media by Kody Antle, the son of the animal trainer Bhagavan 'Doc' Antle, shows Sugriva the chimpanzee browsing through Instagram. Chimps are known to use at least 22 types of tools, and have been shown to be good at computer games.

<iframe width="560" height="315" src="https://www.youtube.com/embed/XTiZqCQsfa8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

For this assignment you are asked to observe yourself using your phone: are there any specific apps or interface features, or perhaps gestures that you find really satisfying? Things about your phone that make you happy? Try to find these features and document them, when do they happen? what about them is so satisfying to you? what other things compare to that experience?

Once you have your findings, try to isolate that feature into a design artifact all of its own. The result must be in the form-factor of a screen device. It will be presented on a phone or alongside other phones. So think of the form-factor of the phone as a kind of "exhibition design".

This work by Rebecca Rui explores some of these notions in its own way.

<iframe src="https://player.vimeo.com/video/283030397" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

### Planning

| week | date | suggested planning | class feedback |
|:----:|:----:|:----|
| week 1 | sept 18 | observe + document + ideate | group A + B (quick feedback) |
| week 2 | sept 25 | low fidelity prototype | group A + B (quick feedback) |
| week 3 | oct 2 | improved prototype | group A (one-to-one) |
| week 4 | oct 9 | improved prototype | group B (one-to-one) |
| week 5 | oct 16 | getting ready to present | group A + B (quick feedback) |
| week 6 | oct 23 | present final | midterm  (group) |

### Recommended reading 
- [Loren Brichter on Pull to Refresh](https://tinyurl.com/y24kggya). If this link doesn't work google for "Loren Brichter CMU lecture Pull to Refresh" and the googles will take you there.
- [US senator wants to make autoplay videos and infinite scroll illegal](https://gizmodo.com/senator-moves-to-make-autoplay-videos-and-infinite-scro-1836822459) through the Social Media Addiction Reduction Technology Act, or SMART Act.
- [Why the Pull to Refresh Feature Must Die](https://www.fastcompany.com/3023421/why-the-pull-to-refresh-gesture-must-die)
- [Why Pull to Refresh Isn't Such a Bad Guy](http://www.neglectedpotential.com/2014/01/ptr/)
- ['Our minds can be hijacked': the tech insiders who fear a smartphone dystopia](https://www.theguardian.com/technology/2017/oct/05/smartphone-addiction-silicon-valley-dystopia)
