---
title: Wayfinding in the Data Jungle
---

![hero](trailblazing-signs-in-usa.jpg?classes=caption "Trail blazers in the USA")

The amount of data we deal with on a regular basis forms a landscape so dense that it is hard to make out what is important. Like in a jungle it is hard to see where one plant begins and another one ends or where the sky and the ground are. Masses of data are like large patches of vegetation, everything looks the same, green, dense and difficult to navigate.

In this assignment you are asked to develop strategies for finding your way around a data jungle.

Which are the important hubs where information accumulates? Which are the outliers? What information is significant? What information is noise? 

Where are the paths that connect all these things and how can we make them visible to the explorer of the Data Jungle?

### Suggestions
- You can use formats that you are familiar with such as visualizations and sonifications, but try to contextualize them in some kind of “user experience”, think of maquettes, architectural spaces, public spaces, browser plugins, audio tours, etc.
- Use Trackography data as *the territory* you are trying to map out.
