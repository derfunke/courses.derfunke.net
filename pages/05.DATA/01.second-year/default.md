---
title: Critical Engineering 1
taxonomy:
    category: 
        - artez
        - second year
---

In the second year of the Interaction Design program you will get Critical Engineering 1. This course focuses on some of the more physical aspects of Interaction Design, focusing specifically in technical knowledge and basic skill-building.


### First semester

- First assignment: [Learnable bits](/data/assignment-book/microbit-multiplayer)
