---
title: Design, Art and Technology Arnhem
---

DATA is an award winning program in Interaction Design at the Arnhem University of the Arts. The program distinguishes itself by focusing on applied artistic inquiry in the fields of design and technology.

Here you can find the course materials used in the **Critical Engineering** class taught by me. The class spans two whole academic years, full-time and is taught every week on Wednesday.

## Syllabi

- [2019-2020](#)
