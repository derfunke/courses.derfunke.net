# ORC/\

Useful things to download: [MIDI Monitor](https://www.snoize.com/midimonitor/) will allow you to snoop into the MIDI stream.

### General concepts
- ORCA was designed to make sound machines (synths)
- I like to think of it as a weird-ass chessboard for livecoding generative music that looks a bit like a gigantic living sudoku
- Moving around
- Selecting, copying and pasting
- Every letter does something
- Inputs and outputs for each letter
- Bangs
- Making sounds

### Parameters

Let's use the T (tracker) operator for this example. When we type T on a cell, immediately we see it takes two parameters on the left and at least one on the right.

```
..T.
....
```

One of the parameters on the left can be used to indicate how many positions to the right can be used as parameters. This is weird to explain but actually easy to understand when you try it out.

```
08T........
...........
```
The 8 indicates that the T will track, the 8 notes on the right hand of the T. And the 0 indicates.

Let's make a little tracked synth with this construction.

```
..C8.........
.D38TADBFHD6A
..:AFF51.....
.J..J........
..:8FA54.....
.J..J........
..:1FF54.....
```

Or perhaps a bass line:

```
.........C.....
...68TCFG7CFE..
..C..E.........
.D7..J.........
.*:32Ef5.......
```

### MIDI in ORCA

```
:A2F51
```

where:

A is the channel
2 is octave
F a note value
5 velocity
1 duration

## Time

D - delay
C - clock

Count to 8:
```
.C8
```

Count to 8 more slowly (every 2 beats):
```
2C8
```


```
..C.....
.D..aRf.
..:03cf5
........
........
.D2.aRf.
..:02cf5
```

### Comparisons

```
#.IF.#..............
....................
#.COMPARE.2.VALUES.#
....................
aFb.aFa.1F0.1F1.....
.....*.......*......
....................
#.INVERT.BANGS.#....
....................
....D4..............
.....F..............
.....*..............
```

### Let's make some beats

Put on your shit Kickers (tm) and kick some shit:

```
#..kick..#
..........
..C.......
..6F6.....
...*:30cff
```
Let's get some snare drums in there:

```
#..snare...#
............
..C.........
..1F2.......
....:10eff..
```

Some hi-hats:

```
#..hat..#
.........
..0R4....
...3B2...
..fC1....
.2B0.....
2D2......
..:10hff.
```

### Moving stuff around

Because programs in ORCA rely on the organization of letters in space, things can sometimes get jammed together in space in ways that makes it hard to organize your sound-making machine.

The Y operator moves the value in it's left to the right.

The J operator moves the value above to the spot below.

The X operator 'writes' a value and the Q operator fetches the written value. Think of X and Q as ways to store and retrieve variables.

### Playing a chord with jumpers

```
#.Play.a.chord.#...............................
...............................................
#.J.allows.you.to.move.data.around............#
...............................................
...............................................
........8C4....................................
.......D814TCAFE...............................
........:03A...................................
.......J...J...................................
........:04A...................................
```

### Channeling MIDI out to other apps

One great thing about ORCA is that in itself it doesn't produce any audio. All that ORCA does is create a machine that makes MIDI output. You can think of it as a robot that plays a keyboard, but doesn't care which keyboard it plays. So if you change the keyboard from under its hands it can make different sounds. 

One way of "changing the keyboard" is to channel the MIDI commands to another software that can make sounds using MIDI input.

### Funky stuff

This one is cool, makes the bang move around using the X operator and a clock.

```
.Cg.......
.dB8......
..5X*.....
..........
....:03B31
....:03C31
....:02B31
....:03B31
....:03C31
....:02B31
....:03B31
....:03A31
```

### Experimenting with SunVox

SunVox is an amazing software tracker and synthesizer, writen by Alexander Zolotov (aka NightRadio). It's free and it runs on many platforms including tablets.

It's a wonderfully strange piece of software, so it fits well in the Esoteric Codes ecosystem.

You can [download it for free here](https://warmplace.ru/soft/sunvox/).

Now let's take the default sound "modules" (or instruments) from SunVox and map them to some MIDI channels.

![sunvox midi](media/sunvox-001.png)

Each of these boxes is a sound generator, you can thing of them as instruments in an orchestra. 

![sunvox midi](media/sunvox-002.png)

Right clicking on an instrument, pulls up this menu, choose *Module Options*. 

![sunvox midi](media/sunvox-003.png)

Set the MIDI trigger to be "any" and the input channel (in the middle) to a number that you can then use from ORCA. Remember that ORCA starts counting from 0, so instrument #1 in SunVox is 0 in ORCA. It's a bit confusing but you get used to it. 

### Using a hardware synth: Behringer TD-3

Set the TD-3 in USB sync mode ->
- press the `FUNCTION` button and hold
- then press `BACK` and `WRITE` while holding `FUNCTION`
- now you should see one of the `INT/MIDI/USB` light blink, set it to `USB`

Set the MIDI channel in the TD-3 ->
- press the `FUNCTION` key and hold, then press the `F#` or `CH` key
- the `ACCENT` and `SLIDE` keys determine whether you are about to set the MIDI In or MIDI Out channels: `ACCENT = MIDI Out` and `SLIDE = MIDI In`
- Set MIDI In to be channel 1, now you can play notes in your TD-3 from Orca by using the Orca channel number 0.



