---
title: Esoteric Codes
---

The notion of Esoteric Codes is not really broadly known, as is itself a little bit esoteric. I got the name from [a website by Daniel Temkin](https://esoteric.codes/about/) an artist that works with code.

Generally speaking Esoteric Codes are approaches to languages, platforms and systems that break from the norms of computing. The *esolang* community has a long history and is a quirky online community that gathers around weird programming languages, however I prefer the broader definition of Esoteric Codes, as it is more inclusive of other forms of computing not just programming.

In this block we will explore the culture around coding, putting emphasis in its cultural manifestations at the more experimental fringes, outside of common techniques and conventional thinking and illustrate it through some quirky softwares.

While esolangs are interesting to the computer science geeks and compiler-curious people. I will try to put emphasis in languages that are interesting from the perspective of an audiovisual artist that can be used in practices like *Live Coding*.

Most of the software we will be using for this module is open source.

## What to expect

This project is pretty experimental for me and I am not an expert on all the systems that will be covered, so in each class you will get a very rough introduction to a new quirky software. You will then be given some time to explore it on your own in block 1 and hopefully get a grasp of it, after that in block 2 you will be asked to use your favorite to produce a work of your own.

In Block 1 we will use the sessions to get acquainted with a new esoteric system and then you get a week to experiment with it and do something with it. At the end of block 1 you will be assessed on the basis of the expeirments you show.

At the end of Block 1 you will get an assignment that will last the whole of Block 2.

## Lesson plan

We will have 14 sessions in total for this project, divided in two blocks. We will meet on Wednesdays, except for three weeks in which we will meet on Friday.

## Block 1

#### BLK1: 8 Sept

Introduction: what is code? how do we code? why do we code? what is hard about code? what is an esoteric language?

learning to code vs. coding to learn

- [How to read software](https://github.com/dropmeaword/how-to-read-software/blob/master/PITCHME.md)

ESOLANG #1: [Introduction to the Microbit](https://github.com/dropmeaword/intro-to-microbit/blob/master/NOTES.md)

Reading: ["Learnable Programming", Bret Victor](http://worrydream.com/LearnableProgramming/) (about 1h30 reading time)

Design principles behind the micro:bit environment that improve the learning experience of coding:
- avoid syntax-related problems
- never allow for a “broken state” 
(make sure that people can explore no matter what)
- never show errors
(- give immediate feedback) <— important for any creative process


#### BLK1: 15 Sept 

Paradigms: spatial programming

ESOLANG #2: ORC/\
  - [Getting started with ORCA](/data/esoteric-codes/orca)

#### Session of the 24th is cancelled
```
#### BLK1: 24 Sept (attention: Friday)

Paradigms: visual programming

Present: results of experiments with Microbit

ESOLANG #1: [MAX/PD](https://cycling74.com/products/max) TouchDesigner
```

#### BLK1: 29 Sept

Paradigms: functional synthesis
Present: results of experiments with ORCA

ESOLANG #3: Hydra/Fluxus

#### BLK1: 6 Oct

Paradigms: synth-based
Presentation: results of experiments with MAX or TD

ESOLANG #4: Supercollider/SonicPi


#### BLK1: 15 Oct  (attention: Friday)

Paradigms: massively parallel
Presentation: results of experimetns with Hydra

ESOLANG #5: GPU shaders

#### BLK1: 20 Oct (midterm grading) (no class)

On this date you will have no class from me.

You will get the assignment for Block 2 on this date.

## Block 2

#### BLK2: 5 Nov  (attention: Friday)

Presentation: wrap-up of results from previous block

#### BLK2: 10 Nov

#### BLK2: 17 Nov

Intro to the ESP32.

- Theory
- Examples
- Reading a sensor
- Creating a physical interaction (or as Gridtegel put it "een Arduino in een doos, het doet een ding met een lampje as je op een knopje drukt")

#### BLK2: 24 Nov

- Switching a relay
- Safety instructions when working with 220 VAC
- ESP32 wifi
- OTA programming
- OSC
- Light & sound

#### BLK2: 1 Dec

#### BLK2: 8 Dec

#### BLK2: 15 Dec

Final project presentation.


# References
- [About Esoteric Codes](https://esoteric.codes/about/) by [Daniel Temkin](http://danieltemkin.com/)
- [Merveilles Town Community Mastodon](https://merveilles.town/about)
- Repository of visual programming languages [The Whole Code Catalog](https://futureofcoding.org/catalog/)
- Generative Contemplation, [Alex Brankjovic](https://alexbrajkovic.com/) https://foundation.app/@alexbrajkovic/generative-contemplation-20219
- [Esolang wiki](https://esolangs.org/wiki/Esoteric_programming_language)
- Shader Showdown competition software [Bonzomatic](https://github.com/Gargaj/Bonzomatic)
- [Learn ORCA](https://metasyn.github.io/learn-orca/)
- [Live Code Lab](https://livecodelab.net/)
- [Fragment](https://www.fsynth.com/)
- [KodeLife GPU shader in realtime](https://hexler.net/kodelife)
- [Meemoo](https://meemoo.org/)
- [in-browser Orca WTF](https://orca.wtf/)

# Some interesting  videos

<iframe width="560" height="315" src="https://www.youtube.com/embed/pLpHBUmwGCk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/smQOiFt8e4Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/2BIOINFSbMg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
