---
title: Life Support
---

Using a combination of sensors, actuators and living things, create a machine to keep *something* alive.

That something could be a flower, a seed, a fish, a cell, a person, a tree, a mould, a mushroom, a chicken, a flea, some plankton, a fly... 

Consider that a *living thing* could also be a metaphor for a living thing, like the flame of a candle or as in the case of Verena Friedrich's work, a soap bubble.

Deliver on the 23rd of March, the end result will be presented as an exhibition. 

### Suggested schedule 
- **9 Feb** - introduction & looking at living things around me, what do they need to live?
- **16 Feb** - determining the needs for the thing you want to keep alive
- **23 Feb** - make
- **2 March** - **vakantie**
- **9 March** - make
- **16 March** - preview (try out and feedback)
- **23 March** - final presentation

## Some references

- [Hackteria](https://www.hackteria.org/), go get lost in their wiki. Really, do it.
- [The Immortal, Revital Cohen](https://vimeo.com/41160704)
- [ROBERTINA ŠEBJANIČ](https://robertina.net/time-displacement-chemobrionic-garden/)
- [ŠPELA PETRIČ](https://www.spelapetric.org/#/institute-for-inconspicuous-languages/)
- [Gilberto Esparza](https://gilbertoesparza.net/portfolio/plantas-nomadas/)
- [AUTOPHOTOSYNTHETIC PLANTS, Gilberto Esparza](https://gilbertoesparza.net/portfolio/plantas-autofotosinteticasautophotosynthetic-plants/)
- [The Long Now, Verena Friedrich](http://heavythinking.org/the-long-now/)
