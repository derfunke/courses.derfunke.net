---
title: Alien Agency
---

Taking the [Life Support](data/life-support) system that you created in the previous assignment, create an audiovisual work (video, stream, sound, music, live performance, etc.) using your chosen living creature as your collaborator.

What is important about this work is that certain elements of its output are not under your control, but rather they are influenced by the living creature that you are collaborating with.

### Suggested schedule 
- **30 March** - guest speaker Nina Kopacz
- **6 April** - introduction to next assignment
- **13 April** -
- **20 April** -
- **27 April** - **day off (koningsdag)**
- **4 May** - **spring break**
- **11 May** -
- **18 May** - preview + feedback
- **25 May** -
- **1 June** - guest performance
- **8 June** - final presentations

## Some references
 
#### Interspecifics

Interspecifics is a collective from Mexico that has been working on interspecies collaboration, their practice spans installation, music and live performance. They often perform with bacterial cultures, organic fuel cells and other living organisms that interact with their instruments in unpredictable ways.

<iframe width="560" height="315" src="https://www.youtube.com/embed/AZPDet-0ER0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/QJglw7JyK8k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Martin Howse

His works often involve the participation of living organisms such as [mycelia, plants and soil](http://www.1010.co.uk/org/earthcode.html). His work [Radio Mycelium](http://www.1010.co.uk/org/radiomycelium.html) is a device as well as an installation and a performance. Martin Howse builds his own instruments and tools, often combining custom software with esoteric electronics.

![earthboot](media/howse-ebc.jpg)

![rmycelia](media/howse-radiomycelium.jpg)