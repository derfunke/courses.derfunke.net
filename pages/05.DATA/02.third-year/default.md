---
title: Critical Engineering 2
taxonomy:
    category: 
        - artez
        - third year
---

In the third year of the Interaction Design program you will get Critical Engineering 2. This course focuses on more philosophical and aesthetic aspects of human-computer interaction, interface building and ethics.

### First semester

- First assignment: [Museum of Happy Phones](/data/assignment-book/museum-of-happy-phones)
