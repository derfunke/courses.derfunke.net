---
title: Building tools at the speed of light (NO CODE)
---

If you follow digital design blogs you have likely come across so called NO CODE tools. These are designer-friendly tools that allow you to hack up prototypes really quickly using really common backends like Google spreadsheets or Airtable.

You should add some of this to your professional toolkit as they can be quite handy for one-offs, odd jobs, quick prototypes and personal _microtools_.

If your practice consists on making many many apps that all more or less have similar features, have to follow common conventions and have simple table-like data structures, these tools will help you deliver fast. NO CODE tools are often not cheap in their pro editions (which you will need sooner or later) and most of them offer free tiers elpful to learn and do odd jobs.

Here are some popular tools that you can try:
- [Airtable](https://airtable.com/)
- [Glide](https://www.glideapps.com/)
- [Coda](https://coda.io/welcome)
- [Stacker](https://stacker.app/)

There's a much more [exensive list of NO CODE tools compiled by Louis Veyret available in this google spreadsheet](https://docs.google.com/spreadsheets/d/1QRhocTMg1cZAZyWz5vznqg6KdZrzU-b557Il8p0Q7HM/edit?usp=sharing).