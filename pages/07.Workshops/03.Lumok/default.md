---
title: Building a lighthouse
---

Use this site to download and upload stuff for this workshop: [http://gg.gg/13eu5p](http://gg.gg/13eu5p)

[Introduction in Notion](https://www.notion.so/Project-principals-adc0c4837227442ea76b5193a3a661e6?pvs=4)

- We will build 5x lighthouses today
- Let's make 5 teams
- Each person in a team takes responsibility over one of these activities: soldering LEDs, soldering PCBs, construction, making good wires & connectors, making some content in TD

## Activity 1: LED fixture

tools needed: scissors, cutting pliers, soldering iron, JST crimping tool

materials: colored wires, round black fat wire (3 leads), JST connectors, soldering tin, pre-cut aluminium square tube

#### Build the pixel strip

- cut two strips of 45 pixels each
- observe the direction of the arrows
- solder a bridge of about 3cm of wire joining both strips
- make sure the arrows are all pointing in the same direction

- get pre-cut aluminium profile
- slide one of the strips through the top two openings
- tape the strips down on opposing sides of the alu profile
- find the first pixel of your strip, if you don't know, ask, it's important  :)

#### Wire to connect the strip to the board

- get a length of about 40cm of the black round wire
- strip it at both ends
- on one end you will need to install a 3-pin JST connector
- the other end will be soldered to the end of the strip where the first pixel is

## Activity 2: PCB

tools needed: cutting pliers, soldering iron, tweezers

materials: raw PCB, soldering tin, USB wall adapter, sacrificial USB cable, black dupont headers, JST connectors, bag of components

#### Populate the board

- Solder the JST connectors
- Solder the sockets for the microcontroller and the motor drivers (black pin things)
- Solder all other components (should be in the bag)

#### Create a USB power cable
- make a frankencable by using a sacrificial USB cable stripping one end and attaching it to a round barrel connector (connector should be in the bag)

#### Flash the microcontroller
- borrow the PCB that you can use to program the ESP32
- visit this website: [https://warez.desearch.cc](https://warez.desearch.cc)
- select the firmware "rodil+murgia R1 (with ethernet)"
- click on "Flash"
- select the serial port where your ESP is connected to
- you will be given the option to configure the wifi after you flash, conncet to the installation's WIFI (will not work on the school's network)
  - SSID: SPECTRUMNET
  - PSSWD: SPECTRUM333333
- your microcontroller is now ready
- stick the microcontroller in the red PCB
- use the blue push buttons to configure the board to work with the motor, [push the buttons like this](https://www.notion.so/Push-button-configuration-e03443b48fd14e83b04b9f42eb2f7306?pvs=4)

#### Install phone app to test
- Go to your phone's app store and search for the WLED app
- install it on your phone

## Activity 3: testing the light

- connect the LED strip to the PCB board using the 3pin JST socket
- power the PCB using the frankencable and a USB walll adapter
- after a few seconds a new WIFI access point should appear on your phone called "WLED-AP"
- connect your phone to it (password: wled1234)
- start the WLED app
- go to "Setup"
  1. configure the LED strip
    - Data GPIO: 12
    - Start: 0
    - Length: 90
  2. configure Sync Interfaces
    - Go to DMX network section
      - choose type Art-Net
      - start universe:  (chose your group number for this one)
      - DMX mode: "Dimmer + Multi RGB"
  3. check the config [in the notion](https://www.notion.so/Configuring-WLED-549b5ba5ec1240689cec3df8786efe9d?pvs=4) (we used a different LED strip for the notion documentation)
- go back to the WLED app's main screen
- try changing the colors, animations, etc.
- with many people building different fixtures it is possible that there will be many "WLED-AP" wifi access points in the space
- coordinate with others so that only one is powered at a time, go to settings and change the name of yours
- Let's called them: LH1, LH2, LH3, LH4 and  LH5 (pick one, that's yours)


## Activity 4: Mechanics

tools needed: allen key M5, screwdriver, hammer (wooden hammer is ideal)

materials: 3D prints, tripod stand, pre-cut 12cm steel tube, pre-cut 5cm screw

#### Prepare the stand
- get a stand (tripod) and attach the coupling to it using a M5 screw

#### Building the rotary
- attach the gear with the small center hole to the motor, you will have to press quite hard to do that, tap it VERY GENTLY with a hammer untill it's totally in and the motorhead and is flush with the gear 
- build the rotary assembly as in the diagram, take your time
- attach the motor to the rotary
- adjust the height of the ring so that both gears align as perfectly as you can

#### Bringing everything together
- attach the LED fixture to the top coupling in the rotary, it's a tight fit you might need to tap VERY GENTLY with a hammer
- drive the cable coming out of the LEDs through the tube of the rotary untill it pops from underneath, get as much cable out as you can
- attach the rotary element to the tripod using the coupling and keeping the cable in the whole of the coupling so that it's fully out


## Activity 5: content

pre-requisites: mouse, TouchDesigner installed

- You can find the templates and all the workshop stuff here: [http://gg.gg/13eu5p](http://gg.gg/13eu5p)
