---
title: The Otherness Toolkit - slicing pieces for your Anatomical Library
---

## What we cover in this session

Give a working introduction to working with Blender, so that those who have never done any work in Blender can manage:
- Layout
- Mouse controls &  modifier keys
- Saving and importing
- Concepts like mesh, face, edge, etc.

## Splice

We will be using Blender to splice free and publicly available 3D models and build a library of useful _parts_ that we can then use to assemble our creations.

#### Getting free models

You can try these places to download free 3D models. I recommend the FBX or Blender formats when available, but you can also use OBJ if there's nothing else. These websites contain huge repositories of 3D models and they have free offerings that although they might be lower quality will be enough for our purpose today.

- [Turbosquid](https://www.turbosquid.com/3d-model/free/)
- [CGTrader](https://www.cgtrader.com/free-3d-models/)
- [Sketchfab](https://sketchfab.com/3d-models/)
- [Free3D](https://free3d.com/3d-models/)

#### Splicing digital species

Import the model you downloaded using the `File > Import...` menu, chose the file format that matches what you downloaded.

![step 1](media/15072020__splicing-00001.png)

The object you just imported will probably be selected by default. Switch to _vertex edit mode_.

![step 2](media/15072020__splicing-00002.png)

Press `A` to select all vertices

![step 3](media/15072020__splicing-00003.png)

In the `Mesh` menu, select the `Bisect` option, this allows you to use a plane, to cut any mesh. When the `bisect` tool is active, there is a _tool options_ popup that appears at the bottom left of your edit area.

![step 4](media/15072020__splicing-00004.png)

When _bisecting_ you have to tell blender which part of the mesh you want to preserve and which one you want to discard. You can use the `clear inner` or `clear outer` to tell which is which. Experiment with these option untill you are comfortable.

![step 5](media/15072020__splicing-00005.png)

For this model I will chose the `clear inner` option to cut the body off and keep the snout. Given the shape of my object I will do this twice at different angles to cut off the parts I do not need.

![step 6](media/15072020__splicing-00006.png)

Our result will be a mesh containing only the part of the body that we need.

![step 7](media/15072020__splicing-00007.png)

Let's save this body part to our library as a blender file to use later.
