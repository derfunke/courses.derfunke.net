---
title: The Otherness Toolkit
---

During this workshop we will explore ways of sketching, constructing and digitizing speculative bodies. Attendants will be introduced to three different techniques dealing with perception, speculative organs and digital representations of the body. We will practice these techniques in the first half of the workshop through exercises and physical experiments. In the second half of the workshop we will work together to develop prototypes for speculative bodies.

## Context

In 2019 an international team of astronomers announced that they had completed the first image that accurately depicts a black hole. That black hole has never been directly witnessed by any human eye, nor has it been captured using any kind of photographic process. It was composed from billions of data points from the Event Horizon Telescope, that were interpreted by a custom algorithm and rendered on a computer screen. Just like the first microscopes allowed us to look into the strange and unknown world of microbial life, this algorithm has allowed us to look at a black hole. We humans have always relied on technology to augment our capabilities. For example, to see what the naked eye cannot see or as is the case with machine learning, to process vast amounts of information and derive insights from patterns, thereby augmenting human cognitive abilities. Each of these technologies augments a capability that can be understood to originate in the human body. This strong and rich relationship between technology and human perception will be our playground. 

Through speculative design we will develop concepts for other possible bodies. Using techniques centered around the body and human perception such as movement, photographs and some make-up techniques we will develop the physical execution of our work. From the analogue, we will then move into the digital, creating ‘face filters’ (i.e. Instagram filters) as a means to express our results.

## Activities

**Day 1** we will explore the role that our eyes, ears, skin, and even our body shape and movement affect our perception of the world around us. We will make: a body atlas and a simple wearable device to augment hearing

**Day 2** we will explore augmentation and modelling to create speculative organs that augment future bodies

**Day 3**:** we will work on digital representations of the previous work, make 3D models using photogrammetry and extend these speculative bodies with digital features

**Day 4, 5 and 6** attendants will work on their own prototypes with daily coaching.

