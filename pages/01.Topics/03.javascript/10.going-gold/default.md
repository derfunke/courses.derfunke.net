---
title: Going gold
---

## What we will cover here
Packaging your assests professionally before publishing on the web.
- browser compatibility
- transpilers: ES6, Babeljs, Typescript
- the importance of a task runner
- dependency management

![toast](media/how-do-you-like-your-toast.jpg?classes=caption "how do you like your toast?")

As your web and javascript projects grow you will probably want to keep them under control. Tracking your assets, making sure that your code is browser cross-compatible, packing things to minimize load times might not be a concern when you start prototyping, but as you get closer to release you will need to streamline your workflow. Add CSS, videos and image assets into the mix, and soon your workflow will probably include several dozen tools that do all kinds of things from image resizing, format conversions, CSS pre-processing, javascript minifiers, etc. Making a release will get overwhelming pretty quickly. There are a few tools that will help you keep sane in this process.

Like everything in Javascript, for everything you might need there are at least 300 possible choices of tools, frameworks and techniques. It's a big community out there and each chef likes it in their own way. So there's not one best way of doing it. I think however that you will need a few of these in your toolbelt so here are some tips.

### Get yourself a good task-runner

- [Webpack](https://webpack.js.org/) fully fledged built tool for JS projects. It will handle all your script files and give you import functionality, it will bundle your graphic assets together, minify your js and run CSS pre-processors.
- [Grunt](https://gruntjs.com/) or [Gulp](https://gulpjs.com/) both pretty good. I favour Gulp slightly because I find it simpler, both are more or less equivalent. Will be able to do almost everything that Webpack can do (while being simpler) and will do more than Webpack with the right plugins.

### Browser targetting & Transpilers

When the browser is your target platform you will encounter multiple difficulties when making your designs work in the wild variety of browsers and browser versions that are in use out there in the world. The immense diversity of target platfors is what makes the release process so complex, if you only needed to target the browser that you ade your work with you would be done with a simple upload. Most headaches come from the use of advanced features in CSS and/or Javascript.

To overcome some of these targetting nightmares in Javascriptthere's a series of intermediate code processors called *transpilers* (or translating compilers). What a transpiler does is to take in code written for a specific target version of Javascript, for example ES6 and *translate* that code to be cross-compatible with a variety of browser platforms and Javascript specs. 

If you have to target multiple browser versions with different javascript version support and do not want to crump your coding style you will need to use [Babel](https://babeljs.io/). Babel allows you to write Javascript in the ES6 standard, which is pretty sweet and it will compile to any version.

#### Typescript

Large applications in Javascript tend to be hard to maintain and require developers to be very disciplined to prevent things such as namespace pollution, variable type violations, etc. [Microsoft has released a superset of Javascript named Typescript](http://www.typescriptlang.org/) that tries to address some of these issues by providing new keywords for language features that would otherwise require tricks in Javascript (such as namespaces) and type support. Many modern Javascript APIs are in fact written in Typescript and the *transpiled* into Javascript before publishing.

### Get yourself a good dependency manager

My advice is to use **as few libraries as possible**, the leaner your project the more agile you will be the faster you can move without random break-downs and it will keep your build simple. If you can write something and it is easy for you, avoid importing a library.

- Most people use [npm](https://www.npmjs.com/get-npm) the fact that it is the default package manager for nodejs makes is the default for a lot of projects also. It is hard to stay *lean* with npm though as your project will soon become an unwieldy mess of hundred of libraries. But I am afraid that's modern javascript, the moment you bite the apple of using one library you expose yourself to a deep web of dependencies.
- [Yarn](https://yarnpkg.com/lang/en/) is a favorite
- [Bower](https://bower.io/)

## Why?

You might be asking yourself what would be the need for any of this. Well, it has to do with Javascript itself being a kind of patched up together language and the immense complexity and diversity of browsers. Here's an excellent article that might help understand:

- [Why is modern web development so complicated? A long yet hasty explanation: Part 1!](https://www.vrk.dev/2019/07/11/why-is-modern-web-development-so-complicated-a-long-yet-hasty-explanation-part-1/)