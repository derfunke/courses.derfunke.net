---
title: Building a device prototype
---

## Part 1: Exploring your kit

Let's go over the parts included in the kit and what they are used for.

<iframe width="560" height="315" src="https://www.youtube.com/embed/HeYuCvJPpfA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Part 2: Harmless electronics

A short disclaimer.

<iframe width="560" height="315" src="https://www.youtube.com/embed/_ouLrRoytng" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Part 3: Inspecting your microcontroller board

Let's look intently at the HUZZAH board and try to decipher the meaning of its labels.

<iframe width="560" height="315" src="https://www.youtube.com/embed/HsNeBGYLRoY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Part 4: Adding support for our board in Arduino

To program our board we will be using the Arduino environment, but our board is not really from the Arduino brand, so we need to add support for it before we can continue. The video will show you this step in greater detail. Oh, and you will need this copy and paste this URL to follow the video. `https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json` this is the URL that you will need to paste on the boards manager setting.

<iframe width="560" height="315" src="https://www.youtube.com/embed/G3JAHVcdbdk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Part 5: Checking that everything works

Now that we have the Arduino programming environment setup, we need to verify that we can actually upload code to the board. Once we clear this step we will have successfully set ourselves up for the work ahead.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ivSSZXhmVYc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Part 6: Why are we using the HUZZAH?

Why was this board chosen instead of the more conventional choice of an Arduino? What are its advantages and disadvantages?

<iframe width="560" height="315" src="https://www.youtube.com/embed/QHB5mp0cc5I" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

