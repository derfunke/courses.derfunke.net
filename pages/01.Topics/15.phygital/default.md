---
title: Phygital Interactions
---

In this module we will explore the connection between the physical and the digital and the kinds of interactions that are possible in these hybrid systems.

[Here you can find the slide deck for the entire module](https://docs.google.com/presentation/d/1i5RJWJrTAboViHEDnsiyyiTetNJFm6KoPbaZGKcwRc4/edit?usp=sharing).

- [Introduction](./intro)
- [For those who will be working with the circuit](./circuit)
- [For those who will be working without a circuit](./no-circuit)
- [Getting things to talk to one another using MQTT](./mqtt-intro)
- [Third party tools](./third-party-tools)
- [Documenting interactions](./documenting-interactions)
- [Assignment](./assignment)


https://www.interaction-design.org/literature/article/what-is-interaction-design
https://uxdesign.cc/how-to-write-for-interactions-3ff03080fb18
https://www.uxmatters.com/mt/archives/2012/01/defining-an-interaction-model-the-cornerstone-of-application-design.php

https://www.toptal.com/designers/interactive/interaction-design-principles

