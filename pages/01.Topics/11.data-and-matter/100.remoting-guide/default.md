---
title: MDD goes remote
---

In the spirit of continuation and adapting to a changing situation as response to the need to stay away from physical education, the whole of MDD will be working remotely in the near future. This document is meant to offer some tips on how to continue with your prototyping activities.

At the moment, the official line of the HvA is that facilities will remain open and everyone will still be available remotely on their usual working hours. But the situation changes quickly, so keep yourselves informed from official sources.

Please stay tuned to the [latest official communications from the HvA in this site](https://coronainfo.mijnhva.nl/en) and to your HvA email.

The department will be adopting Zoom as teleconferencing solution for all official affairs. **We recommend that you [download and install Zoom today](https://zoom.us/) and sign up with your HvA email account.**

## Decentralized Lab

We have decided that the Interaction Lab will become the **Decentralized LAB** for the coming weeks. This means that all of us have a little piece of it in our own homes. This will probably be the first Decentralized Interaction Lab in the world, so your cooperation will determine its success.

From the perspective of the lab and for the sake of facilitating that you keep working we have decided that you can prepare a box with materials and equipment to take with you.

This box might include:
- Portable VR kits (Oculus GO), cameras, sensors, arduinos or other microcontrollers, cables, etc.

You are **NOT ALLOWED** to take any tools:
- DO NOT TAKE: 3D printer, room-scale VR headsets, power supplies, soldering irons, drills, computers, screwdrivers, multimeters, etc.
(edited)

**WARNING! NOTHING LEAVES THE LAB, WITHOUT A PICTURE AND A FULL LIST OF EVERY ITEM YOU TAKE BEING ADDED TO THIS AIRTABLE BELOW  (please no DMs, we are all a bit overwhelmed at the moment)**

An [online inventory of who has what can be found here](https://airtable.com/shr5ID9aprl7fl7Lj/tblm5JNBjLDqpNWLa?blocks=hide). If you need access to update this table, ask Will.

<iframe class="airtable-embed" src="https://airtable.com/embed/shrrwiXuHEVAYaDTk?backgroundColor=pink&layout=card&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>

Some rules for the common good:
- at the end of the Decentralized Lab period, all materials will have to come back to the lab (except for consumables)
- the person that took the item is responsible for returning it
- so if you borrow it to somebody else, please make a note of that in the table as you will be the one held accountable for it

### Access to tools

If you need access to certain tools, like a soldering iron or a multimeter, they can be found cheaply in the Gamma or Praxis or in any of the webshops listed below, if you cannot buy one you can contact Dolinde or Luis. We each have a soldering iron ready to use for small jobs. Please get an appointment with us on Slack.

The official Slack channel for all things Decentralized Lab will be **#stuff_i_took_from_the_lab**.

## Makerspaces

**Continue using the facilities at the HvA, such as the Makers Lab as long as they stay open**. 

**Makers Lab**  
Benno Premselahuis  
room 00B05  
mon to thu from 8h30 to 18h00 uur  
fri from 8h30 to 16h30

If you find that they are closed, you can try [Contact Amsterdam](https://contactamsterdam.nl/). For any lasercutting, 3D printing or CNCing that you might need. We have made arrangements so that you can be well received at the Contact co-working and makerspace.

web: [Contact Amsterdam](https://contactamsterdam.nl/)  
map: [Contactweg 47](https://goo.gl/maps/U4fMpgRGX4XccpKbA)  
+31 202157136  
[info@contactamsterdam.nl](mailto:info@contactamsterdam.nl)

Let them know that you come from the Master of Digital Design.

If you need to work with wood or metal, most places will require that you take a workshop before you can use their equipment. Contact Luis to know about these places.

These places are in the city, so they are not subject to coronavirus response guidelines for educational institutions, but rather are subject to city guidelines. Always follow city guidelines or the makerspace's own when making use of these spaces.

## Electronics stores

#### Physical shops in Amsterdam

For small things and same-day purchases of small parts you can go to these shops.

**Radio Rotor**  
Kinkerstraat 55, phone: 020 612 5759

**Hecke Electronica**  
Ceintuurbaan 7, phone: 020 679 2459

#### Webshops
The following webshops have next-day delivery and carry many things that are needed for physical prototyping.

These are physical stores in Amsterdam where you can go for specific pieces that you might miss.

Always buy supplies as a group whenever you can, you can save on shipping costs. It’s silly to order two LED strips separately when they could all be sent in a single shipment.

**[www.floris.cc](www.floris.cc)**  
Good prices, next day delivery and Pieter Floris is very responsive and knowledgeable.

**[www.kiwi-electronics.nl](www.kiwi-electronics.nl)**  
Good for break-out boards and Raspberry Pi accessories and decent stepper motors. Good service, ships fast.

**[www.antratek.nl](www.antratek.nl)**  
They have some of the more obscure Sparkfun items that you cannot really find so easily in other Dutch suppliers.

**[www.tinytronics.nl](www.tinytronics.nl)**  
Interesting mix between brand-name components and cheap chinese knock-offs. 

**[www.conrad.nl](www.conrad.nl)**  
Reliable, very large supply of all kinds of things to do with electricity. Not cheap for small components or microcontroller items.

**[www.distrelec.nl](www.distrelec.nl)**  
Reliable, diverse, very large supply, you can find everything from a tiny electronic component to a fully equipped workbench. Great for tools.

## Access to academic papers

You can continue to access all HvA digital infrastructure if you use the official VPN. [Find instructions here](https://www.amsterdamuas.com/practical-matters/students/auas/its-si/vpn-accessing-the-auas-network/download-vpn-software/download-vpn-software-kopie.html).

This way you can also access academic databases you might need for papers, etc.

If you can find a specific paper, ask Nicolai in Slack on the **#designresearch channel** and he can help you get it.

And remember stay safe, wash your hands, sanitize your keyboards and trackpads!

<iframe width="560" height="315" src="https://www.youtube.com/embed/3PmVJQUCm4E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
