---
marp: true
title: Powering the digital medium
description: Designing digital interfaces for low energy impact
theme: gaia
size: 16:9
paginate: true
---

### Session plan

- 10.00 discussion
- 10.30 lecture
- 12.15 exercise
- 12.30 (( break ))
- 15.30 short presentations
- 16.15 wrap-up

---

### discussion
climate change?

---

### discussion
"Please before you print this email consider the impact that it will have in the environment"

---

![stripes](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1580757414/power_digital_medium/stripes_GLOBE---1850-2018-MO.png)
global temperatures between 1850-2018

---

what happens when we go to google.com?

---

<!-- _backgroundColor: white -->

![diagram](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1580760197/power_digital_medium/tim_frick_sustainable.jpg)
from *Designing for Sustainability* by Tim Frick

---

**Demo:** Let's trace the route between my laptop to instagram.com

---

[timo arnall's Internet Machine Trailer](https://vimeo.com/189545588)
(unfortunately, this slide framework doesn't allow HTML embeds)

---

## The trouble with measurements

Producing accurate measurements of how much power is consumed in a specific data transfer is very very hard as there are many factors and boundary conditions to consider.

There is not actual consensus on how many units of power (kWh) are used for each unit of data (Gb), estimates range from as high as 5kWh/Gb to as low as 0.027kWh/Gb.

For the sake of our class today, we are going to take a conservative estimate of **0.6kWh per Gigabyte (Gb)**.

---

## The trouble with measurements (pt. 2)
- static (stored) vs. dynamic (requested)
- your iCloud photo archive vs. your Instagram feed
- downloading a movie vs. streaming a movie
- torrent vs. netflix?
- what's the difference?
- it's complicated

---

<!-- _backgroundColor: white -->

![subsystems](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_580/v1580757408/power_digital_medium/internet_subsystems.png)
from Aslan et al., “Electricity Intensity of Internet Data Transmission.”

---

## The economy of digital content delivery
- Content Delivery Networks (CDNs) and data centers try to consume pay the electricity bills
- Content creators have no incentives to optimize content
- PUE (Power Usage Effectiveness), a ratio that describes out of each Watt consumed by datacenter, how much goes into computing (as opposed to airco, heating, light). Google has a PUE of 1.21.
- Scale matters

---

## Data companies want their own electricity
Google, Facebook, Amazon and Apple all invest heavily in building their own power infrastructure. It's cheaper for them to build their own power centrals than to pay electricity bills. 

Google and Apple are 100% renewable energy, but Amazon isn't (yet).

---

## Scale matters

Think for example that shaving off about 10Kb on Youtube's logo, given that youtube is visited about 60M times a day could save aproximately 572,2Gb of data traffic which is aproximately **2861kWh in one day**, which is the amount of combined power you use in about 6 months of your life.

---

## Let's look at one example of a digital asset

- [America's largest bitcoin mining operation](https://www.youtube.com/watch?v=ELA91d_mx80)
- [Inside a Secret Chinese Bitcoin Mine](https://www.youtube.com/watch?v=K8kua5B5K3I)
- [Inside a giant bitcoin mine](https://www.youtube.com/watch?v=yGeim9E24Wk)
- Ok, it's a particularly bad one...

---

![btcranking](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1580757403/power_digital_medium/bitcoin_country_ranking.png)source: Cambridge Bitcoin Electricity Consumption Index

---

## Even 3M knows about it
- [Bitcoin Energy Consumption: An Inside Look](https://www.youtube.com/watch?v=0fLvCnAiGPw)

---

# A comparative framework

I will need one for the rest of this lesson.

---

![flygskam](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1580757408/power_digital_medium/flygskam-001.jpg)

---

## A little experiment with the Dutch wikipedia

---

## Figures used in this write-up
- Energy used per gigabyte: **0.6 kWh/Gb** 
- CO2 emissions per kWh: **0.28307 kgCo2e/kWh** 
- 1 passenger flight from Amsterdam Schiphol to London Gatwick, one way in economy class **0.130 t of CO2**

These are conservative figures based on various reasonable estimates found in research sources (see class notes for references).

---

![wplogo](media/nlwiki-2x_untreated.png)

This is the Wikipedia logo, it's 45700 bytes on disk. (aprox. 45Kb)

---

According to the wikimedia stats for the Dutch wikipedia, the website gets **205M pageviews a month**.

---

Let's calculate how much it takes to download that logo every time: **8934,49 Gb that's 8.8 TERABYTES**, that's about **1517 kgCO2e**, **you could fly London to Amsterdam, aproximately 35 times on the same carbon budget**.

---

![wpnonlossy](media/nlwiki-2x_optimized_nonlossy.png)
43Kb, non-lossy compression

![wpnonlossy](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_250/v1580757404/power_digital_medium/wplogo_optimization-001.png)

---

![wpoptimized](media/nlwiki-2x_optimized.png)
19.7Kb - with lossy compression

![wpoptimized](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_250/v1580757404/power_digital_medium/wplogo_optimization-002.png)

---

### Same logo, different files

![wplogoall](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1580757403/power_digital_medium/wplogo_optimization-003.png)

Let's look at the impact of this simple one-click optimization.

---

## This is how many bytes we saved
### 45.7 - 19.7 = 26 Kb saved

---
## What we save per month
### 205000000 (impressions per month) * 26 = 5083Gb

---
## Let's be optimistic about browser caching
### 5083 Gb * 0.7 (cache factor) = 3558,1 Gb

---
## Thats how this translates to CO2
### (3558,1 Gb * 0.6 kWh/Gb) * 0.28307 kgCo2e/kWh = 604,31 kgCO2e

By using lossy compression on the logo with [ImageOptim](https://imageoptim.com), **we have saved the planet 20 one-way flights London to Amsterdam** per month.

---

### Carbon testing NL wikipedia
- [Website carbon test](https://www.websitecarbon.com/website/nl-wikipedia-org/)

Not bad, it's way above average. Still plenty of room for improvement.

---

![theinternet](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1150/v1580757409/power_digital_medium/clickclean-002.png)

---

### how does digital content on the internet get distributed?
- **Let's take a breath**, geeky stuff coming up...
- [Juan Benet @ Stanford Seminar - IPFS and the Permanent Web](https://www.youtube.com/watch?v=HUVmypx9HGI) from **13m15s** to **15m27s** aprox
- Remember what I said about scale?

---

## Streaming video

About **88% of all web traffic is streaming video**, your Herculean efforts to shave off 25Kb of the Wikipedia logo are puny in comparison to the expenditure of watching a Youtube video or **binge on Stranger Things on Netflix.** 

Unlike bitmaps in website, video streams are never cached by your browser, they are downloaded every time.

see: [This Video Has Consumed 1839247.5 AA Batteries!](https://www.youtube.com/watch?v=EQzyo3q-C2Y)

---

![](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1150/v1580775397/power_digital_medium/fahrenheit-451-truffaut.png)

---

# Netflix

"You would have to burn 62 copies of “Pride and Prejudice” to watch the film based on the book on Netflix — once. "

*calculation by Daniel Gross from Catalogtree*

---

## The Case of *Gangnam style*

The original *Gangnam Style video by Psy* has racked up **3,498,270,983 view as per Jan 2020**. The video is 4:12 minutes or **252 seconds**. The British Computer Society (BCS) estimate is about **312 GWh** and the video is still online and continues to accumulate views. That figure is what it takes to **power about 68000 homes in the UK for a whole year**.

"The starting point for the calculation is the revelation by Google in 2011 (who own YouTube) that streaming 1 minute of video consumes 0.0002 kWh of energy." [3]

---

### A design approach to reducing carbon emissions
"YouTube's annual carbon footprint is about 10Mt CO2e (Million metric tons of carbon dioxide equivalent), about the output of a city the size of Glasgow" [1]

- simple UX change in Youtube
- looking at the way that people streams video
- eliminating the "digital waste" of showing video images to users who are only listening to audio 
- could slash YouTube's carbon footprint by up to 500kt CO2e each year (power needs of 30000 homes in UK)

---

## Immediate vs. long-term effects
"With current network technologies, if you send less data along it, in most cases it doesn’t reduce the energy use. **It's like an airplane: if you don’t fly, the plane flies anyway, and so ‘not flying’ only reduces emissions if it leads to less airplanes flying in the long term**. However, newer technologies, particularly in the mobile network, will mean that reduced data leads to reduced energy consumption more directly." [1]

---

### Emissions per capita
- This is the maximum amount of CO 2 a person should generate per year to stop climate change: 0.600 t CO2
- This is the average amount of CO 2 a person generates in one year in the EU: 8.4 t CO2

---

## What can you do as a designer?

---

![whattodo](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_600/v1580757402/power_digital_medium/uxdesign.cc_design_for_sustainability.png)

---

## What can you do as a designer?

- The field of Sustainable Interaction Design is still in it's infancy, there's still much where curious and motivated talent can bring new light.

- Take on projects personal or associated with bold clients that give you room to experiment and push the envelope on low-carbon footprint digital services, the field is evolving and there are many economic incentives to move in this direction too.

---
## What can you do as a designer? (pt. 2)

- Include sustainability in digital design as part of your pitches (even when the client doesn't call for it explicitly)

- Include an energy budget in all your proposals

- Look at collectives that are looking for designers that want to engage with this issue, for example: [Tech Impact Makers](https://techimpactmakers.com/)

---
## Consider third-party resources
- third-party cookies
- advertisements (adsense)
- syndicated content

all can have a severe impact in your CO2 footprint

---

![nunl](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_500/v1580757411/power_digital_medium/requestmap_http___www.nu.nl.png)

understand your third-party dependencies with: [requestmap](https://requestmap.herokuapp.com/)

---

# The Cost of Ads

---

![](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1580757410/power_digital_medium/nyt_cost_of_ads-004.png)

---

![](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1580757410/power_digital_medium/nyt_cost_of_ads-002.png)

---

![](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1580757409/power_digital_medium/nyt_cost_of_ads-001.png)

---

# Where your site is hosted matters

---

![](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_600/v1580757404/power_digital_medium/aws_regions_sustainable.png)

---

#### Some useful tools

In your process and budgetting of resources, use measuring tools to assess the impact of your design decisions:

- [websitecarbon.com](https://www.websitecarbon.com/)
- [https://speedcurve.com](https://speedcurve.com/)
- [https://www.performancebudget.io/](https://www.performancebudget.io/)

---

![electricity](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_600/v1580757404/power_digital_medium/Annual-CO2-emissions-Treemap-1.png)

---

## Not all electricity is generated equally

[Understand where your product's electricity comes from](https://www.electricitymap.org)

---

#### About graphic assets (pt. 2)

- This is the best guide on image compression for the web, learn what's there: [https://images.guide/](https://images.guide)
- Use [SVG graphics](https://svgontheweb.com) whenever possible, they compress well and use relatively little resources and are easy to include in responsive designs.

---
#### About graphic assets (pt. 3)

- If you must absolutely include bitmap images, use image optimizers, here are a few: 
  - [https://tiny.pictures/](https://tiny.pictures)
  - [https://imagekit.io/](https://imagekit.io)
  - [https://kraken.io/](https://kraken.io)
  - or have one installed in your own computer: [https://imageoptim.com](https://imageoptim.com)

---

### Chose 100%-renewable hosting companies

https://www.thegreenwebfoundation.org/directory/

---

## Critically assessing a design
- How *media heavy* is the resulting design? (e.g. Page Weight Budget)
- What is the impact of my design if 1000 people use it? how about 1M people? and 10M people?
- How much javascript must be executed before the page is fully rendered?
- Are there any auto-play videos? or full-screen video backgrounds?
- How many advertisers and trackers are embedded? How can they be minimized?

---

## Critically assessing a design (pt. 2)
- How many requests are necessary to complete a full page load? Can any of these resources be inlined?
- Is it using a CDN (Content Delivery Network)?
- Is it hosted in a facility that uses renewable sources of energy?
- Are the media files optimized for streaming transfers?
- What are the peak hours of usage? Can the content adapt to demand?
- What devices are supported and how resource-heavy is the design when accessed through each device?

---
## Critically assessing a design (pt. 3)

- Is there a text-only version of this work?
- What are the choices offered to the user? How sustainable are they? (e.g. shipping a product, can I chose group shipping for specific delivery points?)

[Questionnaire to help develop roadmaps for sustainable design](https://www.aiga.org/roadmap).

---

# Other ways of responding from design

(go to class notes "Case Studies")

---

# It's worth it

![clean](https://res.cloudinary.com/zilogtastic/image/upload/v1580757405/power_digital_medium/climate-summit-what-if-it-s-a-big-hoax-and-we-create-a-better-world-for-nothing.jpg)

---

# Exercise

Work with your group (same as client project) and pick one of these tasks to work on:

1. choose a digital product that you use regularly and investigate what is its energy budget, what could you do to lower it?
2. work on an energy budget for the client project that you are working on
3. what can you do as an individual to lower your digital footprint?

How can we make sense of the *energy budget* in each of these contexts?

---

# Let's meet again at 15h30