---
title: Designing digital interfaces for low energy impact
---

We tend to think of digital systems as being inherently more sustainable than analog or material-based approaches. The digital system that we encounter in our daily interaction are all powered by electricity and our data is hosted in datacenters that have a physical and environmental footprint. So far designers have approached the digital medium with the mentality that power consumption was not a factor to take into consideration and that bandwidth was unlimitted, as we approach a reality in which we have to factor in a climate crisis we can not continue to neglct these aspects.

---

Did you know that a Wordpress site consumes 42% more energy than a plain HTML website? Would you save any energy if your website wasn't online 24h a day?

Pretty much every device that we could consider digital in its inner workings relies in one way or another on electrical power. Whether it is the cable that goes from our desktop computer to the wall outlet or the battery in our mobile phone, electric scooter, car, etc. They all connext to the electrical grid. As our gadgets "evolve" their power needs also change. For example, before smartphone it was not uncommon for a Nokia mobile phone to run for a whole week on one charge. It was the advent of the iPhone that "trained" people to charge their phones once a day. Smartphones also created a new need for charging outlets everywhere, for smatphones users to be able to charge their phones on the go. From airport waiting halls in Conpenhagen, to underground stations in Taipei there are now USB charging sockets in many transit areas where people wait. Our entire digital culture runs on electrical power, from thedata centers to bitcoin mining operations, our smartphones, etc. All draw power from the electrical grid, which ultimately draws resources from our planet, some of these sources are finite, others are renewable.

The advent of the smartphone also created a cultural shift in software developers and companies that develop mobile software as well as "front-enders". Smartphones created the incentive to make software that used as little software as possible. App reviews in app stores commented on apps that drained phone battery and thus a new consideration was born in the discipline of having to take energy consumption as a consideration in the software design process.

Websites, advertisements, javascript routines running in the background make some websites more battery intensive than others. A new rationale for adblockers in smartphones emerged after research proved that about 20% of the battery power of your smartphone is drained by advertisements shown in te sites you visit, creating an incentive for low power consumption advertising.

How can we design for a power-hungry world? What guidelines can we come up with, that would enable designers to create more energy-efficient digital content? What are the societal repercusions that arise when everything needs to be charged, from your phone to your car? What technologies are behind this shift and how are they sourced?

The goal of this this theme is to ask these questions and come up with design strategies that address those questions. You might decide that some aspects of this topic require public information campaigns, others require design guidelines, others might require visualizations, others can take more concrete forms such as an interactive installation. The job of this unit will be to produce knowledge on the topic through research and to think up of ways of communicating your findings to the different communities that you will encounter in your process.

---

Why data & matter?

### e-Waste & Planned Obsolescence
- [The Lightbulb Conspiracy](https://thoughtmaybe.com/the-light-bulb-conspiracy/)
- [Repair Manifesto](http://www.platform21.nl/download/4453)
- [We're all losers to a gadget industry built on planned obsolescence](http://www.theguardian.com/sustainable-business/2015/mar/23/were-are-all-losers-to-gadget-industry-built-on-planned-obsolescence)
- [E-Waste Republic](http://interactive.aljazeera.com/aje/2015/ewaste/index.html)
- [Blood in the Mobile](https://thoughtmaybe.com/blood-in-the-mobile/)
- ["The Man in the White Suit"](https://www.youtube.com/watch?v=RQfmI_gmDo4)

- [The Cobalt Pipeline](https://www.washingtonpost.com/graphics/business/batteries/congo-cobalt-mining-for-lithium-ion-battery/?)
- [Death metal: tin mining in Indonesia](https://www.theguardian.com/environment/2012/nov/23/tin-mining-indonesia-bangka)
- [The Tin Mines of Bangka Island](https://www.bloomberg.com/news/photo-essays/2015-08-26/the-tin-mines-of-bangka-island)