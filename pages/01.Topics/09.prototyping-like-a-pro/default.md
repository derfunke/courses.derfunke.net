---
title: Prototyping like a pro
---

##### Session plan
  - 10h00 Artur will show us how he uses p5js in his prototypes
  - 10h40 A basic process of screen-work: The Sketch, The Wireframe, The Mockup and The Prototype
  - 11h15 Revise together the Philips Trends bubble interface in this light
  - ((( Break )))
  - 13h00 Hacking together a mockup of Philips
  - 14h30 Exercise
  - 15h30 (if we have time) The DROID FARM (tm) *a method to make almost anything*

You can find the [slides I used in class right here](media/slides/slides-prototyping-like-a-pro.html), slides are extracted from these notes though, so these notes are more complete.

---

## Prototyping for research with Artur Cordeiro
Artur is an architect working in the field of digital urbanism and he is a resident researcher in our extertise center Play & Civic Media. I invited him to show you how he uses p5js as a low-stakes way to create prototypes that aid him in the design process.

---
## Sketching, Wireframing, Mocking-up and Prototyping

![process](media/procesimage.jpg)

These are the four most common stages of development that you can see in a digital product lifecycle, at least if that digital product is screen based. If you have studied digital design before you will probably be very familiar with this, this doesn't mean that you can skip this session though as I am expecting you to help out.  

There are a few differences when you are working on digital devices, or immersive experiences but as a general framework for conceiving the development of a product as a process, it is useful to study this more rigid and traditional approach. We will eventually see how this process can be inadecuate in certain situations.

It is important to understand that **this process ISN'T LINEAR**, it isn't like one stage follows the other immediately after. It is normal to make several iterations at each step. This is a process of gradual refinement of work towards a prototype. **It is about adding detail progressively**.

> Each section contains a few suggestions for tools but these are just the ones offered here, you can find a [comprehensive list of tools here, put together by the Flawless team](https://flawlessapp.io/designtools). Look at the resources in [Design Notes](https://www.designnotes.co/) for inspiration.
 
## The Sketch

A sketch is low-fidelity and it is often a first attempt at giving your ideas some kind of visual form. In screen-based products a sketch will often involve a simple pen-drawing of screen layouts perhaps with some text explainations of what goes there. It lays out the basic ideas behind your proposed work. At this stage there are many things in the work that are still open, such as visual identity, specifics about interactions, no animations, etc. It's a totally barebones representation of what we are thinking about.

At this stage you can use free-hand drawings, post-it notes, sketch pads, etc. as your tools. Work loosely and do not get married to any ideas just yet.

**Sketches are also a communication tool**, when you present them to your client they can help in making sure that the requirements were understood and that you are on the same page about what the design needs to accomplish.

![sketch](media/sketch-05.jpg)

#### Tools to sketch 🐰
Regular graphing paper and pen will get you there, but if you need fancy tools you can use these:
- A tablet and a stylus can be a pretty effective way of sketching using an all-digital toolchain
- [sneakpeekit](https://sneakpeekit.com/) grid paper for sketching your ideas
- Or the fantastic [graphing paper generator tool](https://incompetech.com/graphpaper/) that makes ready to print templates for your graphing paper needs

## The Wireframe

A wireframe is a more concrete representation of your screen-based work but still low-fidelity in terms of graphic detail. A wireframe could provide insights about:
  - how you go from one screen to another
  - how information is presented to your user
  - what information is required from the user
  - how the user enters that information
  - how is the information laid out on your screen
  - what forms do the individual elements in a screen take
  - if the work is to support multiple screens, the wireframe should be able to reflect how the work will look like in each of the proposed resolutions, aspect ratios, screen orientations, etc.
  - some designers also include things such as animation and transitions in the wireframes to get a rough understanding of how the screen-based work will **feel like**.

![wireframe](media/wireframe.gif)  

If there are any advanced workflows (e.g. personalization, user generated content) you can also specify them as diagrams at this stage of your design process.

#### Tools to wireframe 🐰
There are literally hundreds of tools to wireframe, but not all of them will allow you to evolve your wireframe to later stages:
- [Wireframe](https://wireframe.cc/)
- [Wireframes to go](https://wireframestogo.com/) is a collection of Balsamiq templates
- [Balsamiq](https://balsamiq.com/) (*just to be clear I do not encourage you to use this, but you might encounter it in the wild. This is a tool often used by non-designers.*)

These more professional tools will allow you to seamlessly evolve to more mature phases of your product's design:

- [protopie](https://www.protopie.io/)
- [proto.io](https://proto.io/)
- [InVision](https://www.invisionapp.com/)
- [Axure](https://www.axure.com/)

Some diagraming tools:
- [FlowMapp](https://flowmapp.com/)
- [Google Docs Drawings](https://docs.google.com/drawings/)
- [LucidChart](https://www.lucidchart.com)
- [Creately](https://creately.com/)
- [Gliffy](https://www.gliffy.com/)
- [Pencil](https://pencil.evolus.vn/) is open source
- [OmniGraffle](https://www.omnigroup.com/omnigraffle/)

## The Mock-up
The mockup is a higher-fidelity design stage where the visual identity of the screen-based work finally comes together. It is a close representation of how the final product is going to look like. It is in the mockup that you first see the application of the house-style or design system, the typography and color palettes. The mock-up looks and feels like the real thing but does not yet *behave* like the real thing or contain real data yet. Although not advised, it is ok to have a bit of placeholder text here and there in your mockups as at this stage perhaps the data might not yet be available to you. But of course the closer your mockup is to your final representation with real data included, the better.

Do better than *lorem ipsum*, **there are thousands of public domain works of literature** from which you can get realistic texts in multiple languages and the same goes for photography and video, there are millions of public domain pieces of content. The [New York Public Library has an amazing digital archive](https://digitalcollections.nypl.org/), where you can get tons of stuff. The [Prelinger Archives](https://archive.org/details/prelinger) are also available for free online and also contain tons of video material. And the [Gutenberg Project archive](https://archive.org/details/gutenberg) have thousands of out-of-copyright works of literature available for free online. So you can have more close-to-reality placeholder content than ancient latin text. You can also find public domain texts that read right-to-left (RTL) is you want to test how your design would look like if it ever was to be translated to Hebrew for example. So *lorem ipsum* *might* be ok, but you have other options too.

![mockup](media/mockup.gif)

A mock-up will provide you with reasonably detailed insights about:
- UI elements, how buttons, input areas, visualizations are going to look like
- Layout of content and/or data, where things will be placed and how will they look
- House styles, illustrations and all graphic elements such as punch-outs, masks, chat-heads, etc.
- The entire range of typographic work must be visible in the mockup too, different fonts and different sizes
- Blank spaces, spacing and margins are crucial for successful design, make sure they are correctly represented in your mockup.
- Navigation, status bars
- Errors, warnings, pop-ups, notifications and any other visual elements that might not pertain to the regular flow should also exist in mock-up form with the same level of detail as any other elements in the mockup.

Because a mock-up already contains much of the graphic detail, if your design is responsive, it is important that you present it for different screen-sizes, pixel densities and aspects ratios of all the devices that the product must support as some screen formats will demand from you different layouts for the information you want to present, etc.

Like in the wireframe is there are any unusual flows, conditions or usage states that are are not represented in the mockup, you can present them as diagrams together with the mockup. Perhaps the user must complete a challenge in the flesh in the real world, before they can use the app, this happens sometimes with customer verification processes, where an ID or address-check is needed before an account is confirmed, etc.

#### Tools to create mock-ups 🐰
- [Facebook Devices](https://facebook.design/devices)
- [FRRAMES](https://frrames.com/) device frames
- [Sketch.app](http://wwww.sketchapp.com/)
- [Figma](https://www.figma.com/)
- [Adobe XD](https://www.adobe.com/products/xd.html)
- [Abstract](https://www.abstract.com/) works together with sketch, all your design assets in one place (suggested by Grabriel)

## The Prototype

A prototype is the highest-fidelity representation of this process. Prototypes themsleves, depending of how far in your process you are might have different levels of fidelity among them. Your first prototype for example might be closer to your mockup than to the final product, but as you iterate on your prototype every iteration brings you closer to the finalized product. Prototyping is by far the most crucial stage of the whole process presented here, specially when working on devices, experiences and other non-screen based work. 

Every behavior, every function that needs to exist in the final work must be clearly defined and simulated through the prototype. A prototype should be as close in function as possible to the real product. In screen-based work it is fairly common to do more than one prototype and then *A/B test* them. (see research methods). Each evolving from the last test and each progressively refining and adding detail to the final design.

![proto](media/prototype.gif)

A prototype allows us to conduct user research without having to rely on assumptions. The closer our prototype is to the product we desire the more accurate the results of our user tests will be. Prototypes can also function as *cultural probes* (see research methods).

Prototyping is absolutely crucial as it will give you insights into:
- The general feel of the product (all animations, interactions and inputs must be represented accurately) in the case of a device, looks, materials, behaviours.
- A sense of how **real data** is presented to the user
- A precise representation of each interaction: how it takes place, what triggers it, what feedback the user receives, how does the interaction develop over time, etc.
- Allows you and other stakeholders to test the design in real-life conditions, through user tests, or by providing real-life data for the visualizations, UX research methods, etc.
- It serves as a petri dish for new ideas. Inevitably once everybody can see the product in a more finalized form it will allow new ideas to emerge by responding to the things in the prototype that could be improved, or done differently. 
- Prototyping will also give you a more accurate idea how much a product will cost to develop. In the case of devices and digital products with a physical presence, the prototype can dictate material costs, costs of assembly, costs of manufacturing and costs of development. For screen based work it will be similarly helpful although it will probably involve fewer cost break-downs.
- Viability of your product, only through prototyping will you really know if what you designed is possible to develop, manufacture or assemble.

#### Becoming a prototyping ninja

Prototyping is a skill in itself (like all other steps of the process) and there are designers and companies that are better at one stage of these process than others, there is such thing as a good and a bad prototype. A good prototyping expert is worth their weight in gold as they can take responsibility and demonstrate with evidence that the design decisions accurately follow the requirements of the brief, the wishes of the designer, the needs of our stakeholders, and that the product is viable to make.

**Early selective prototyping** if your product has a feature that is somewhat out-of-the-box, perhaps a radical innovation, an unusual interface, or something that has never been attempted before, or quite simply a feature that you have never yourself done before (so it will be hard for you to know how much it will cost to develop it). That's what you should prototype first. You may think of prototyping as a kind of feasibility study.

Another thing that will make you into a prototyping ninja is to **parametrize your designs**. Design your work so that a simple tweak of a slider, a simple change in a set of parameters, allows you to make certain aspects of your design adaptable. Things that you can parametrize easily are button sizes, whitespaces, dimensions of UI elements, colors, font sizes, etc. as well as certain behaviours, for exmple if you have a physical-based animation based on springs you can make the *springiness* of your springs change at the tweak of a slider and that way be able to try various things out in your design and see how the user reacts to slight tweaks in your design. Facebook is famous for doing design alterations that are highly parametrizable and then they can test multiple versions of one design on millions of people to see which specific tweak of which design is most effective.

#### Tools for prototyping 🐰
We will look at screen-based work only, as prototyping physical digital products is way too broad a topic to cover in a simple list.

- [p5js](http://www.p5js.org) is a fairly reasonable framework to selectively prototype some behaviours, specially for non-standard widgets or visualizations 
- [protopie](https://www.protopie.io/) easy to use but limited to simple animation and simple built-in behaviours
- [UXpin](https://www.uxpin.com/) 
- [Framer](https://www.framer.com/) great tool, you can customize all behaviours by dropping to code but has a vast array of very rich components
- [Origami](https://origami.design/) can work together with Sketch, very powerful. Allows you to prototype innovative interactions using patches. You are less constrained by the defaults and standard widgets that you will find in other prototyping tools and allows you to be inventive. The learning curve is a little steeper than other tools, but it is worth it.
- [Sketch.systems](https://sketch.systems/) a designer-friendly state machine design tool to prototype stateful designs.

---

## Study case: Philips Trend database

Project by Beatriz Bernardino, Josie Fengjiao Zuo, Rowan Verbraak and Kareem Elkady for Philips Design.


#### Assumptions
The mockup you see in these screens has a few assumptions built-in, can you identify any?
- This screen functions as a visualization of the categories in our dataset as well as an interface for filtering through the data. From the perspective of a person using this interface, how effective is it at these tasks?
- Is the data leading the visualization or the other way around?
- What happens if our dataset contains 50 categories for which there's 1 to 5 hits, and 10 categories for which there are 800 to 2000 hits? How does this representation fare when ranges are so broad?
- If there are categories that are too small to represent in this screen, how do we make them visible?
- When we click on one of these bubbles, we are effectively *filtering* through our data. Are we aware of what information we are excluding?
- Typography in the bubbles autoscales to bubble size, what happens when this logic breaks and the bubble can no longer contain the text or the text becomes unreadable?
- how accessible is this interface? (is this even a consideration that we must take into account in our definition?)
- does this visualization animate? if so... how, when, what?
- the selection of filters on top of the screen suggests a hierarchy, how effective is our filtering strategy as we drill down the hierarchy?
- the small bubbles seem to have conveniently shorter texts, reality is never as neat as a hand-drawn mockup!

---

## Study case: Philips Trend database

Making our own **early selective prototype**.

#### Creating a fake dataset

You will hear the word JSON a lot in the coming year, so [read up a little on it](https://en.wikipedia.org/wiki/JSON) as it will be handy to share a common language to know what people are talking about.

JSON is a way of representing datasets containing `key-value pairs` in a Javascript-friendly way. You can think of a `key` as being the name of a column in excel and a `value` as the value in the cell. So this data entry:

|name|date of birth|passport number|affiliate id|number of pets|
|---|---|---|---|---|
|Lucy in the Sky|16-11-1992|8765123AD|12345|2|

Could be represented in JSON as:

```js
{
  name: 'Lucy in the Sky',
  dob: '16-11-1922',
  passport_nr: '8765123AD',
  affiliate_id: 12345,
  pets: 2
}
```

Essentially these two pieces of data are semantically identical, they are just represented differently, the first is in *tabular* format (or as a table) and the second is in JSON format (friendly to Javascript).

Just like you would use placeholder text or images in your design, you can also use placeholder data for your prototypes. For this we can use a [json-generator.com](http://json-generator.com). This website allows us to create a template, to generate mockup datasets, think of it as the *lorem ipsum* of datasets. For the specific purpose of prototyping this design, I will use this template:

```
[
  '{{repeat(50, 70)}}',
  {
    _id: '{{objectId()}}',
    index: '{{index()}}',
    archived: '{{bool()}}',
    hits: '{{integer(1, 800)}}',
    tag: '{{lorem(1, "words")}}',
    last_updated: '{{date(new Date(2014, 0, 1), new Date(), "YYYY-MM-ddThh:mm:ss Z")}}',
    label_color: [
      '{{integer(0, 255)}}',
      '{{integer(0, 255)}}',
      '{{integer(0, 255)}}'
      ],
    children: [
      '{{repeat(0, 10)}}',
      '{{lorem(1, "words")}}'
    ]
  }
]
```

#### Circle-packing 🐰

Watch Daniel Shiffman build a simple circle-packing algorithm step by step.

<iframe width="560" height="315" src="https://www.youtube.com/embed/XATr_jdh-44" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<iframe width="560" height="315" src="https://www.youtube.com/embed/QHEQuoIKgNE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

#### Other viz techniques 🐰

These are other visualization techniques that apply to the kind of information that the Philips Trends challenge proposes:
- [Cascaded Treemap](https://observablehq.com/@d3/cascaded-treemap)
- [Circle packing](https://observablehq.com/@d3/circle-packing-monochrome)
- [Continuous scales](https://observablehq.com/@d3/continuous-scales)
- [Enclosing diagram](https://observablehq.com/@eesur/d3-enclosing-diagram-circle-pack)
- [D3 scales](https://observablehq.com/@d3/introduction-to-d3s-scales)
 
---

# The DROID FARM method

You will not find the DROID FARM method anywhere else, as it is a collection of tips that work well together and come from my own personal experience. My hope is that it will be helpful to you as you set yourself in a career as a designer. 

### D - define the challenge 

Imagine that you sit down to work on a specific part of your project, you can describe the challenge that you are about to work on as: "I want to make an interactive projection surface of some kind. I want it to be functional for the purposes of my interaction and I want it to be cheap to make." This is a fairly good start but it begs the question "what is then the purpose of your interaction?" and "how much means cheap to you!?". Should children, for example, be able to use your work? What new constraints does that bring to how you define your problem?

When we spend time defining our problem carefully, we will already identify needs or challenges that we might not have seen before.

**Be specific**, there are many ways of defining a problem, for example: 

1. I want to design a more efficient checkout process in this specific webshop.
    - while it has a clear goal, it is still quite vaguely defined what is meant here by *more efficient*? Is it more efficient for the shopper or more efficient for the seller?

What you would like to do ideally is to define the problem in as specific a way as you can **without constraining yourself to any specific solution**:

2. I want to design more efficient process where a customer can complete a purchase in two clicks and one single pageview and in no more than a week's development time.
   - this definition gives us more concrete constraints that we can take into consideration in our design, while at the same time leaves open the "how to" solve it part, perhaps we find a solution in the check out process, or perhaps the solution is somewhere else. but now we have given ourselves creative freedom to explore the various options.

**How you define a problem can also have an impact in your creativity**. The broader the definition the more *in the box* your *solution* is likely to be (as you are likely to approach the problem in the same way that others approached it before).

Definition #1 will constrain you to a specific kind of solution (existing checkout processes for webshops) while at the same time is too vague (it doesn't state specific goals). While the other offers a more open prospect (talks about checkout) and keeps the measurable goals clearly in view (two clicks, one week of development) and doesn't narrow you down too early in your process to a specific solution, it simply states more strict conditions.

**Don't try to think about solutions too early**, when you do that, you force yourself into thinking about your *problem* in a way that already fits the solution you have in mind and that will narrow your options too early in your process. By offering solutions too early in the process you will also tend to end up using formulaic solutions that have been used before. It will be harder to innovate.

Another thing that you need to keep in mind when defining your problem are **any hard-defined constraints that you might have**. For example: the resulting design will have to fit in a 60x60cm space, or it must be less expensive than 100,- EUR, or it will have to be transported often in a small car, or it should be doable with a team of two people with such and such skills. This is absolutely crucial to produce solutions that fit the design challenge.

~~In stating your problem be aware as well of the skills you have, what you can do yourself and what you will need the help of others to do. This will have an impact on the cost of your solution, the size of the team needed to build it. Adding people will also have an impact in time spent communicating, etc.~~


## R - research

Do your homework before you start hacking away. **Whatever challenge you face, there is a very high probability that somebody else has faced a similar challenge before**. Let go off the "already been done" mental blockade. 

If it's been done before you want to know why, who made it and how they made it and you want to know all that in as much detail as you can. "Already been done" is often an excuse to not do it yourself. Knowing the ancestry of your challenges *in detail*, the *detail* part is important, will allow you to find things that might otherwise be easily overlooked where you can innovate and do it in your own way.

In your research you might also come across a commercially available product that does exactly what you need and meets all the requirements of your problem statement. You might want to consider buying it instead of spending your time in doing it yourself.

**When researching keep your mind open to *lateral options* that is, you might come across something that, if repurposed, might fit your needs**. Perhaps a microphone stand can be the perfect foot for a light fixture, or a plastic that is normally used for moulding can be the perfect enclosure for your electronics.

## O - own your core

If your core business is finely crafted animations in apps, **you have to strive to be the best at that**. Outsource as much as you can in everything that is not part of your *core*. You need backend storage? get it or get someone else to build it, don't build it yourself. **The core of your business is something you have to craft yourself to the best of your ability and it must command your full attention**. Whether your core business is a visual style, a special expertise in a material, precission-crafted rocket parts, 3D printed fashion, persuasive story-telling, a prototyping technique or whatever it is, your aim here is to totally own the core aspects of your process. By *owning something* I mean to be in control of all aspects that allow you to make that something.

~~If your core business is making waffles you have to get your hands involved in the making of the waffles. If however your core business is to sell waffles in the most beautiful gift boxes ever seen, your core business is making great gift boxes, but perhaps you do not need to bake the waffles yourself.~~

Make sure that your core business is something you love and enjoy doing, because you will be doing it a lot.

## I - incentives

If you want people to interact with something you made, you have to provide incentives for that interaction to be satisfactory. Interactive work is often rather demanding on the audience, if often implies engaging with a new interface, learning something new, etc. You might feel naturaly inclined to approach an interactive thing and explore it, that's perhaps why you are here, but this is not true of everybody. Some people are *germophobes* that shudder at the thought of using the VR set that somebody else has used. Some people prefer to make bookings book through a telephone call or talking to a human than using the seemigly convenient app for the same task. It is not htat you have to please everybody (you don't), but just understand that using your work involves a *going out of my way* investment on the side of the humans that you want to use your work.

You must create the conditions for that interaction to be inviting and you should give something back to the person that engages with your design.

## D - delete

Kill your darlings. Archive your design files. Wipe your whiteboards. Do it often.

If you are stuck it might be because you are holding onto something that is holding you back. 

Annotating is forgetting. Storing is forgetting. Whatever is not commanding your full attention at the moment should be out of your sight. Otherwise all that debris will add to your level of stress.

There are files in your computer that you haven't opened in more than two years, yet you know they are there, you might not be able to remember where exactly they are but you know they are there. That's a cognitive load you carry with you every hour of the day, every day. Same applies to notes, whiteboards, etc. You do these things to remember but in fact, you also do it to move on and forget. The cognitive load this bears on you is real and it gets heavier every day you do nothing about it. It feels busy, your desktop is busy, your notebook is full, your whiteboards are full, the post-its pile up, you think you are busy but all you are doing is come up with methods to forget, defer, archive and forget again. You can stop this any time and you should.

Lighten the load. Wipe your whiteboards, discard your post-its. Archive your files.

Have a realistic look at your workspace and your computer. If it is from the past, archive it. If it's from the future, schedule it. Stay in the present.

## F - function

How does your design actually address the challenge you defined in the first step. Does it provide a solution? How does it provide that solution?

Evaluate this questions often, keep your eyes on the ball. Every step in your design process will (and probably should) change the answers to these questions, re-evaluate them often. It will help you stay focused.

## A - ask the right questions

Most clients will approach you with a really vague idea of what they need. They have identified a problem, but they do not yet have a clear understanding of what could address their problem. That's why they contact a designer. If they already knew what pipe needs fixing, they would call a plumber.

Part of your skill as a designer will be determined by how good your questions are to obtain the right information to understand the design challenge you and other stakeholders are facing.

A client might come and ask something along the lines *how can we increase engagement in the neighbouring communities?*, while a good start for a client to be able to pose that question to a design team. This question is not good enough for a design team to start designing right away, it's just not specific enough. You must understand why they have this need in the first place, what would be a successful form of engagement from their perspective and what is the audience they are trying to *engage*. Always be mindful that whenever you hear the word *engagement* your client might be implying some kind of *behavioural change* so ask ethical questions as well!

## R - risk

As you are building your design, rip it apart often. Try to find every single failure point that you can. Identify your risks and assess them seriously. If you find a risk that is unnaceptable you have to *design defensively* around it.

For example: does my installation need an internet connection? What are the chances that one might not be available? How will my work behave if the internet connection fails? What if it fails once? What if it fails twice a day?

Prepare for failure, things do fail when they are out in the wild. Problems will arise that you did not foresee, perhaps a person did something that you did not expect. Perhaps it rains very heavily and parts of your work get damaged. Run a few scenarios through your head and think about how you would diagnose and fix those problems. If you find a weakness in your design that would make a problem in the field insurmountable, try to *design defensively*. If you think you will not be able to recover from water damage and the risk of water damage exists, consider waterproofing your work and that will become a requirement in your definition of the design challenge.

Prepare yourself for possible scenarios: "we launch the product and a small glitch in the order process means 90% of our customers cannot complete an order in the first day", "our installation opened yesterday at a festival and the festival production assistant called this morning to tell us that it doesn't work anymore".

Assessing your risks realistically can help you in budgeting your work, perhaps you need to get certain pieces in duplicates so that you have spares. Perhaps you or your team are confronted with a new technique that none of you is familiar with and you have to allocate more time to get the hang of it before you are confident that your work will withstand a production environment.

Nobody is perfect! Know your weaknesses and the weaknesses of your designs.

## M - model

Before you build anything, specially if it's a physical device, model it. Make detailed drawings. Create a detailed 3D model. Build a maquette. If you are working on something too large to fit in your workspace, work at a reduced scale. Architects have been doing this forever. Fine-tune your design by iterating quickly on the model, you will be able to find all kinds of problems in your model before you build the real thing. A model will help you in determining dimensions, relations of scale, stability of your build, it will allow you to understand how light will affect your work, where things have to be in relation to each other.

Same thing applies if you are building non-material things such as digital products or stories. In those cases we might not use the word model though, but call them wireframes or storyboards. 

The purpose of a model is to previsualize your design and serve as a canonical reference as you build. A model also helps you to communicate with others, perhaps a contractor that you need to build a specific part of the work, or other stakeholders. We can refer to the model in our stand-ups and make sure that everybody is working towards building the same thing. 

If in the building process you realize something is amiss in your model, update your model. Try to keep your product and your model in sync at all times.

## S - Serendipity

Let the thing that you are doing speak back to you and tell you *what it wants to become* and be open to be taken into new directions by your own work. Not only by your ideas, but by the thing that you make itself. At some point the things that you make will begin "talking back" to you.




