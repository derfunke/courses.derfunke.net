---
marp: true
title: Prototyping like a pro
description: Prototyping digital products
theme: uncover
paginate: true
---

### Session plan

- 10h00 Artur: prototyping with p5js
- 10h40 Brief lecture on formal screen-based processes
- 11h15 Study case: Philips Trends
- ((( Break )))
- 13h00 Mockup of the bubble interface
- 14h30 Exercise
- 15h30 (if we have time) The DROID FARMS (tm) method to make almost anything

---

![process](media/procesimage.jpg)

---

##### Sketches, Wireframes, Mock-ups and Prototypes

- four most common stages of development that you can see in a digital product lifecycle
- some differences when you are working on digital devices, or immersive experiences
- **this process ISN'T LINEAR**
- a process of gradual refinement, it is about adding detail progressively

---

### The Sketch

![sketch](media/sketch-05.jpg)

---

### The Sketch

- low-fidelity
- first attempt at giving your ideas some kind of visual form
- totally barebones representation of what we are thinking about

*At this stage you can use free-hand drawings, post-it notes, sketch pads, etc. as your tools. Work loosely and do not get married to any ideas just yet.*

**Sketches are also a communication tool**

---

### The Sketch: tools 🐰
Regular graphing paper and pen will get you there, but if you need fancy tools you can use these:
- A tablet and a stylus can be a pretty effective way of sketching using an all-digital toolchain
- [sneakpeekit](https://sneakpeekit.com/) grid paper for sketching your ideas
- Or the fantastic [graphing paper generator tool](https://incompetech.com/graphpaper/) that makes ready to print templates for your graphing paper needs

---

### The Wireframe

![wireframe](media/wireframe.gif)  

---

### The Wireframe

A wireframe may provide insights about:
  - how you go from one screen to another
  - how information is presented to your user
  - what information is required from the user
  - how the user enters that information
  - how is the information layed out on your screen
  - what forms do the individual elements in a screen take

---

### The Wireframe

  - if the work is to support multiple screen, the wireframe should be able to reflect how the work will look like in each of the proposed resolutions, aspect ratios, etc.
  - some designers also include things such as animation and transitions in the wireframes to get a rough understanding of how the screen-based work will **feel like**.

---

### The Wireframe

If there are any advanced workflows (e.g. personalization, user generated content, off-screen account verification) you can also specify them as flow diagrams at this stage of your design process.

---

### The Wireframe: tools

Basics (not really recommended for serious work):
- [Wireframe.cc](https://wireframe.cc/)
- [Balsamiq](https://balsamiq.com/)
- [Wireframes to go](https://wireframestogo.com/) is a collection of Balsamiq templates

---

### The Wireframe: tools

More sophisticated, will keep you company at later stages of your process:
- [protopie](https://www.protopie.io/)
- [proto.io](https://proto.io/)
- [InVision](https://www.invisionapp.com/)
- [Axure](https://www.axure.com/)

---

### The Wireframe: tools

Some diagramming tools that you might find useful as support:
- [FlowMapp](https://flowmapp.com/)
- [Google Docs Drawings](https://docs.google.com/drawings/)
- [LucidChart](https://www.lucidchart.com)
- [Creately](https://creately.com/)
- [Gliffy](https://www.gliffy.com/)
- [Pencil](https://pencil.evolus.vn/) is open source
- [OmniGraffle](https://www.omnigroup.com/omnigraffle/)

---

### The Mockup

![mockup](media/mockup.gif)

---

### The Mockup

- higher-fidelity design stage
- close representation of how the final product is going to look like
- application of the house-style or design system, typography and color palettes

> *The mock-up looks and feels like the real thing but doesn't yet *behave* like the real thing or may not contain real data yet.*

---

### The Mockup

A good mock-up will provide you with reasonably detailed insights about:
- UI elements, how buttons, input areas, visualizations are going to look like
- Layout of content and/or data, where things will be placed and how will they look
- House styles, illustrations and all graphic elements such as punch-outs, masks, chat-heads, etc.
- The entire range of typographic work must be visible in the mockup too, different fonts and different sizes

---

### The Mockup

- Blank spaces, spacing and margins are crucial for successful design, make sure they are correctly represented in your mockup
- Navigation and status bars and any other indicators of *statefulness* (e.g. recording, editing, uploading)
- Errors, warnings, pop-ups, notifications and any other visual elements that might not pertain to the regular flow, should also exist with the same level of detail as any other elements in the mockup

---

### The Mockup: responsive

![responsive](media/Apple-Responsive-Screen-Mockups-full.jpg)

---

### The Mockup: tools
- [Facebook Devices](https://facebook.design/devices)
- [FRRAMES](https://frrames.com/) device frames
- [Sketch.app](http://wwww.sketchapp.com/)
- [Adobe XD](https://www.adobe.com/products/xd.html)

---

### The Prototype
- highest-fidelity representation of your product in this process
- as you iterate on your prototype every iteration brings you closer to the finalized product
- Prototyping is by far the most crucial stage of the whole process

---
### The Prototype

> Every behavior, every function that needs to exist in the real app must be clearly defined and simulated through the prototype. A prototype should be as close in function as possible to the real product.

> In screen-based work it is fairly common to do more than one prototype and then *A/B test* them. (see research methods). Each evolving from the last test and each progressively refining and adding detail to the final design.

---
### The Prototype

> A prototype allows us to conduct user research without having to rely on assumptions. The closer our prototype is to the product we desire, the more accurate the results of our user tests will be.

> Prototypes can also function as *cultural probes* (see research methods).

---

### The Prototype

It will give you insights into:
- The general feel of the product
- How **real data** is presented to the user
- A precise representation of each interaction (what are the triggers, what feedback the user receives, how does the interaction develop over time, etc.)

---

### The Prototype
- Allows you and other team members to test the design in real-life conditions
- It serves as a *petri dish* for new ideas
- Viability of your product (e.g. how much it will cost to build, what expertise is needed to build it, etc.)

---

### The Prototype: a vital professional skill

> A good prototyping expert is worth their weight in gold as they can take responsibility and demonstrate with evidence that the design decisions accurately follow the requirements of the brief, the wishes of the designer, and that the product is viable to make.

---

### The Prototype: ninja-level skill

- **Early selective prototyping** 
- **Parametrize your designs**

---

### The Prototype: tools
**for screen-based work only**
- [p5js](http://www.p5js.org)
- [protopie](https://www.protopie.io/)
- [UXpin](https://www.uxpin.com/) 
- [Framer](https://www.framer.com/)
- [Origami](https://origami.design/)
- [Sketch.systems](https://sketch.systems/)

---

## Study case: Philips Trends

Project by Beatriz Bernardino, Josie Fengjiao Zuo, Rowan Verbraak and Kareem Elkady for Philips Design.

---

## Study case: Philips Trends

> *Summarized problem statement*: client had a SharePoint share of PDF documents, categorized by tags, it was hard to find content for visually-oriented people

---

## Study case: Philips Trends

Work by students

---

## Study case: Philips Trends
##### The Bubble Interface

---

## Study case: Philips Trends
##### The Bubble Interface

The mockup you see in these screens has a few assumptions built-in, can you identify any?

---

# ((( BREAK )))

---

## Study case: Philips Trends
##### The Bubble Interface
Let's hack together on a p5js mockup of this interface, see what we find out

---

# ((( BREAK )))

---

# The DROID FARMS method (tm)
a method to make *almost* anything

---

![androids](media/android_army.jpg)

---

## D - define the challenge 

**Be specific, without constraining yourself to any specific solution**

>How can I provide more working space in this classroom without any reduction in the amount of floorspace?

**vs.**

> Design a shelving solution that will keep clutter away in this classroom without a significant impact in the floorspace available

---

## part 2: D - define the challenge 

> How you define a problem can also have an impact in your creative process.

> Don't try to think about solutions too early, you will narrow your options

> Include any hard-defined constraints that you might have in your definition (e.g. has to fit through a door, should be buildable by a maximum of two people)

---

## R - research

> Whatever challenge you face, there is a very high probability that somebody else has faced a similar challenge before

> When researching keep your mind open to *lateral options* (you might come across something that, if repurposed, might fit your needs)

---

## O - own your core

> If your core business is making waffles you have to get your hands involved in the making of the waffles and strive to become the best at it. If however your core business is to sell waffles in the most beautiful gift boxes ever seen, your core business is making great gift boxes, but perhaps you do not need to bake the waffles yourself.

**By *owning something* I mean to be in control of all aspects that allow you to make that something.**

---

## I - incentives

> All interactions require a significant investment from your audience (e.g. time or learning a new interface)

> When speaking of interactive work: you must create the conditions for that interaction to be inviting and you must give something back to the person that engages with your design.

---

## D - delete

> Kill your darlings. Archive away your design files. Wipe your whiteboards. Do it often.

> Annotating is forgetting. Storing is forgetting. Whatever is not commanding your full attention at the moment should be out of your sight. Otherwise all that debris will add to your level of stress.

>Have a realistic look at your workspace and your computer. If it is from the past, archive it. If it's from the future, schedule it. Stay in the present.

---

## F - function

> How does your design actually address the challenge you defined in the first step. Does it provide a solution? How exactly does it do that?

> Evaluate this questions often, keep your eyes on the ball

---

## A - ask the right questions

> Part of your skill as a designer will be determined by how good your questions are in obtaining the right information to understand the design challenge you are facing.

> Clients will almost never be asking the right questions (to design with) when they first approach you, they have other concerns

---

## R - risk

> Prepare for failure, things do fail when they are out in the wild. Problems will arise that you did not foresee, perhaps a person did something that you did not expect. Perhaps a dirty dataset breaks your visualization.

> Rip apart your designs often. See how they fail and where they fail. *Design deffensively* around failure.

---

## M - model

> Before you build anything, specially if it's a physical device, model it. Make detailed drawings. If you are working on something too large to fit in your workspace, build a maquette. Architects have been doing this since forever for a reason.

---

## part 2: M - model

> The purpose of a model is to previsualize your design and serve as a canonical reference as you build. A model also helps you to communicate with other team members.

---

## part 3 M - model

> If in the building process you realize something is amiss in your model, update your model. Try to keep your product and your model in sync at all times.

---

## alternative: M - measure

> to measure is to know, if you don't know you can't design

e.g. if it has to fit grip of the hand of a 7 year old, how big is that exactly? (see *M for model*) 

---

## S - serendipity

> At some point the things that you make will being "talking back" at you. Learn to listen to them and let your design process be guided by the things that you are making rather than your preconceived ideas of what they should become.

