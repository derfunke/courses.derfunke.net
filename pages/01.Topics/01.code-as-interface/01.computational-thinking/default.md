---
title: Computational thinking
---

The art of playing computer in your head!

> "Pick any field X, from archeology to zoology. There either is now a ‘computational X’ or there soon will be. And it’s widely viewed as the future of the field." 
> — **Stephen Wolfram** [(source)](http://blog.stephenwolfram.com/2016/09/how-to-teach-computational-thinking/)

#### Who are you? and what do you expect from this course?
- who here can write code or do some electronics?
- what is digital design for you?

#### Presentation
- who I am? where I am from? what do I do?
- where else do I teach?
- what else do I do?
- what will this course be about?
- how is the course organized?

#### Discussion
- what is special about digital media?
- why learn about technical systems?
    (Paula's arguments)
    - when you write about computing in your BA (computing history, media cultures, etc.), having a deeper understanding of the technical processes of computing will strengthen your theoretical argument! 
    - you might want to go ahead and persue work in one of the many professions in the software field:
        - software developer
        - product owner (see [So You Don't Want to be a Programmer](https://blog.codinghorror.com/so-you-dont-want-to-be-a-programmer-after-all/) )
    - you might want to pursue work as a product owner in a software company or tech company which needs you to translate the work of programmers to non programmers 
    - you might want to deepen your expertise in a data related field, from data science to algorithmic regulation, programming will allow you to better understand the subtle distinctions between data and code in these fields
    - finally, from the designers of the Major Digital Media, we feel that understanding digital media today calls for understanding at least some of the technical basics of computing! 

# Read 

["Why software is eating the world"](https://a16z.com/2016/08/20/why-software-is-eating-the-world/), Marc Andreessen , Aug 20, 2011

Notice that "Software will eat the world" is actually "a thing" on the Internet, it is a very influential essay in industry, perhaps a little less known in academia.

complementary readings:
- ["Software is eating the world 5 years later"](https://techcrunch.com/2016/06/07/software-is-eating-the-world-5-years-later/)
- [Software might be eating the world, but AI is eating software](https://www.technologyreview.com/s/607831/nvidia-ceo-software-is-eating-the-world-but-ai-is-going-to-eat-software/)

> *Amazon (1994), Facebook (2004), Twitter (2006), iPhone (2007), Airbnb (2008), Uber (2009)*

Discuss:
>    "Six decades into the computer revolution, four decades since the invention of the microprocessor, and two decades into the rise of the modern Internet, all of the technology required to transform industries through software finally works and can be widely delivered at global scale."

>    "In the next 10 years, I expect at least five billion people worldwide to own smartphones, giving every individual with such a phone instant access to the full power of the Internet, every moment of every day."

>    "Now even the books themselves are software."

>    "Today’s largest video service by number of subscribers is a software company: Netflix."

>    "Today’s dominant music companies are software companies, too: Apple’s iTunes, Spotify and Pandora." 

>    "Today’s fastest growing entertainment companies are videogame makers"

>    "Pixar, was a software company. Disney — Disney! — had to buy Pixar, a software company, to remain relevant in animated movies."

>    "Today’s largest direct marketing platform is a software company — Google"

----

# Talk

Although different forms of media have been with us for centuries the very specific digital media landscape in which we live today is relatively recent and in many ways it is one of the largest experiments ever run on human civilization, we still haven't fully realized the consequences of this running experiment.

Just like it is not necessary to read sheet music in order to be able to play the guitar, it is not strictly necessary to write code to understand digital media. But just like in music, if you do not learn its notation a lot of past knowledge and nuance will not be available to you. All that knowledge that is locked up in technical systems is only available to those that know how to unveil their secrets by speaking its language.

Perhaps the least interesting aspect of learning how to program is the code itself, the syntax and formal grammars, the Backus-Naur forms, the notations and keywords that we use to communicate our intentions to the computer. It is what that can unlock, *computational thinking*, that is rather more profound and will affect the way you look at things and will all in all make you more competetent at understanding todays world.

### Discussion

Think from the moment you woke up this morning... what digital systems (or software) have you used?

(and it isn't lunchtime yet...)

Most of this systems were mechanical, others even had analog electronics until not so long ago, but today most of them are digital. An airplane is a computer you fly inside of, (increasingly) a building, specially an office building are computers that you sit in for hours every day. Your car, if it was built in the last decade is also a computer you can drive... Why is that? Why is it that so many systems around us, in the last decades of the 1900s and beginning of the 2000s have become digital?


# Exercises

### Quarter circle
Open up any program in your computer that is capable of letting you draw shapes. Now try and draw a quater-circle, think of a clock and draw the part of the circle that is between 12 and 3. **Write down every step you made to accomplish this task.**

![qcircle](media/quarter-circle.svg)

(discussion)

### The Sorting Algorithm exercise
(discussion)

### The clustering algorithm

#### Follow the rabbit 🐰

- [Taeyoon Choi "Poetic Computation"](http://poeticcomputation.info/contents/)

![video](Man_and_Computer-A_Perspective.mp4)

The film Man & Computer, made in 1967 by IBM's UK branch, provides a basic understanding of computer operations. A large portion of the film shows the ways in which a computer can be simulated by five people using the standard office equipment of the day. The film employs a number of different techniques, including animations, and features a few brief scenes of an IBM System/360 in use—just months after the first machines were delivered.

Starting in the 1940s, IBM became a major producer of films used for sales, training, documenting business processes, entertaining at company functions, and educating the public. Several IBM films were made by respected filmmakers and sometimes featured well-known actors.

(source: [Computer History Museum Youtube channel](https://www.youtube.com/watch?v=BUCZJWo9MZo) )


