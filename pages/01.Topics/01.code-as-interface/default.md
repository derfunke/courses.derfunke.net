---
title: Code as Interface
---

All our interactions with machine interfaces, including non-digital machines like our washing machine. Generate a set of codified actions that can be understood as grammars these grammars can form programming languages in themselves. In this module we look at Computational Thinking, statements, actions, performance as a strategy to understand algorithms and a brief and cursory introduction to the most basic statements of Javascript.

