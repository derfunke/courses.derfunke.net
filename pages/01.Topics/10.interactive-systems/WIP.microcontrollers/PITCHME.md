## Physical Computing (PhysComp)
making computers understand the physical world around us
+++
![Klima Kontrol by Roel Wouters](https://www.youtube.com/embed/s2fTLKIs9Z4)
+++
### how the computer sees you

![Tom Igoe Human](http://res.cloudinary.com/zilogtastic/image/upload/v1505408559/igoefinger_ie2ihx.jpg)
Note:
Tom Igoe, 2003
+++
Touchscreens, touchpads and the Kinect have changed this perception a little but it remains largely the same.
+++
### Kinect
![Skeletal tracking](http://res.cloudinary.com/zilogtastic/image/upload/v1505408570/kinect4_wnlfpb.png)

The Kinect introduced skeletal tracking for full-body interactions but as you can see the image that the computer has of the human is still **reductionist** at best.

---
## electronics
 let us bridge the gap
**between the digital world**
**and the physical world**

---
### Microcontrollers (MCUs)

![PIC microcontroller](http://res.cloudinary.com/zilogtastic/image/upload/v1505408580/33_bdso4q.jpg)

your washing machine has one

+++
##### there are more MCUs in the world than there are CPUs
They are by far the most ubiquitous form of computing. There are many different kinds but in this course we will be dealing primarily with two kinds AVR and ESP82xx.

+++
# AVR chipset
Made by a company named Atmel, is the kind used in many of the Arduino family of MCUs. There are many AVR chips and the area of embedded electronics has been exploding the last two years.
+++
### Arduino UNO

![uno](https://cdn.sparkfun.com//assets/parts/6/8/1/6/11224-04.jpg)

Trusty old friend that never disappoints, big, bulky, reliable and slow. Great for the classroom, we all need one of these to sketch on, but it's too big and too **expensive** for project work.

+++
### Arduino Nano

![Arduino Nano](http://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_410/v1505408591/Electronics-DIY-Arduino-Nano-v3.0-Mini-ATmega328-Board-1_10-366-550x650_dmldfg.jpg)
+++
### Attiny85

![Attiny85](http://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_410/v1505408635/09378-1_okao4b.jpg)

If you just need a few pins for your project, why use a whole Arduino?
(note: needs a USB programmer, see next slide)
+++
### The ATtiny85 programmer

![Attiny programmer](http://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_410/v1505408774/tiny-avr-programmer_dsjkc2.jpg)

+++
### Attiny85 pinout

![Attiny85 pinout](http://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_430/v1505408652/52713d5b757b7fc0658b4567_h7idgb.png)

+++
### Meet the Digispark

The supertiny Attiny85 protoboard

![Digispark](http://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_420/v1505408867/SKU172392b_mh5dcp.jpg)

+++
### Other ATtinies

Atmel makes about 45 different varieties of the ATtiny series of MCUs. Enough to make your head spin. So if the Attiny85 doesn't suit your needs there's plenty to choose from. [See full product list here](http://www.atmel.com/products/microcontrollers/avr/tinyavr.aspx).

+++
### ESP82xx
Much more powerful MCU, supports in-chip WiFi, a lot more memory (4Mb), can be programmed with the same Arduino toolchain that you have grown used to. Popular for IoT applications.

+++
### Adafruit Featherboard HUZZAH

![HUZZAH](http://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_410/v1505408916/2821-01_w3lriy.jpg)

18.50,- EUR in floris.cc

+++
### Wemos D1 mini Pro

![Wemos D1](http://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_410/v1505408930/Wemos_D1_mini_Pro_jf1tqp.jpg)

8,- EUR in tinytronics.nl, cheaper from Chinese supplier

+++

Then there's this little oddball

+++

![microbit](https://microbit.org/images/redirect_scrolling_bit.gif)

---

### Introduction to Microbit

![microbit](https://user-content.gitlab-static.net/cf1e3777dea33eb313245a09fc290e174f46bf54/68747470733a2f2f63646e2e73686f706966792e636f6d2f732f66696c65732f312f303231352f363435382f70726f64756374732f6d6963726f6269742d6e6f7465735f35643739343936632d333734622d343033622d386634312d3533643339663230313636395f353830784032782e706e673f763d31343939393533393039)

[notes](https://gitlab.com/dropmeaword/intro-to-microbit/blob/master/NOTES.md)

---

# Arduino: a primer
+++
I'm sure you have heard about this Arduino thing.
+++
## Arduino
#### is a platform to program microcontrollers
(think of it as a wee little computer if you will)
+++
## A microcontroller
(your washing machine has one)

Note:
microcontrollers far exceed the number of computers in the world and they have been around for a very long time
+++
![sensors and actuators](http://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_450/v1506374484/sensors_and_actuators_xhuves.png)
---
## Working with Arduino
+++
## Inspecting your kit
what are all these weird objects?
+++
#### breadboard
![breadboard](http://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_420/v1506374485/breadboard_ywjkvo.png)
+++
#### the arduino platform: the UNO
![Arduino Uno](http://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_450/v1506370084/arduino_uno_wwnmzi.jpg)
+++
#### the arduino platform: the NANO
![Arduino NANO](http://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_520/v1506370085/ARDUINO_NANO_03_ltdqb1.png)
+++
### If you have a cheap Arduino clone, get the driver
[CH340G serial driver install](https://kig.re/2014/12/31/how-to-use-arduino-nano-mini-pro-with-CH340G-on-mac-osx-yosemite.html)
+++
### Download and install the software (IDE)
[get a recent version, greater than 1.8](https://www.arduino.cc/en/Main/Software)
+++
### What's in the IDE?
+++
### Mac only
###### If you have problems: disable System Integrity Protection (SIP)
[Disable SIP](https://tzapu.com/making-ch340-ch341-serial-adapters-work-under-el-capitan-os-x/)
+++
### Verify
###### Can I see my Arduino in the list of ports in the IDE?
if so let's move on
+++
### The canonical blinking LDE
![blinky](http://res.cloudinary.com/zilogtastic/image/upload/v1506374484/LED_blink_cdxeci.png)
+++
#### Blink code
load it from the **examples > basics > Blink** menu
+++
## Sensing the world around you
sensors and what they do
+++
## What is a sensor?
A sensor is an electronic device that can transform a physical property of our world into an electrical signal that a computer can understand.

The only thing that you need to know about a sensor for now is that it is "a value that moves through time". People in finance call that a "ticker" in EE it's called a "signal" in electronic music they call it a "control signal" but in all of them they mean "a value that moves through time".
+++
## There are (for now) two types of sensors
We can classify them according to the kind of signal that they produce:
  - digital (two states)
    - shit happened (HIGH = 1)
    - shit didn't happen (LOW = 0)
  - analog
    - how much of the actual shit happened
    - it is often a number between 0 and 1024
+++
### Light-level sensor
setup your breadboard
![LDR sensor](http://res.cloudinary.com/zilogtastic/image/upload/c_scale,h_450/v1506369513/ldr_arduino_ppac8t.jpg)
---
### code
###### reading the LDR sensor with Arduino
---
## CHOICE
#### of sensors
+++
### Distance
- [Ultrasonic rangefinder](https://www.google.com/search?q=Ultrasonic+sensor)
- [IR sensor](https://www.google.com/search?q=IR+sensor)
### Presence & motion
- [PIR sensor](https://www.google.com/search?q=PIR+sensor) (passive infrared)
+++
### Smoke, gas and alcohol
- [MQ-2 smoke sensor](https://www.google.com/search?q=MQ-2+smoke+sensor)
- [MQ-3 Alcohol-ethanol (breathalyzer)](https://www.google.com/search?q=MQ-3+smoke+sensor)
+++
### Touch
- Button
- Switch
- [Potentiometer](https://www.google.com/search?q=potentiometer)
- Capacitive sensing
  - simple resistor circuit (see cap. sensing slides)
  - [QT113](https://www.google.com/search?q=QT113+sensor)
+++
### Movement
- Tilt sensor
- Vibration sensor
- Joystick
+++
### Acceleration
- Accelerometer
- Gyroscope
- IMU (Inertial motion unit, 9-DoF)
+++
![YCAM Awareness in Motion](https://player.vimeo.com/video/61942488)
+++
### Light
- LDR
- Flame sensor
- UV index sensor
- Color sensor (RGB)
- Camera (3CCD)
- Thermal camera
+++
### Weather
- Barometric pressure
- Wind (anemometer)
- Temperature
- Soil humidity
- Atmospheric humidity
+++
[James Bridle's A Ship Adrift](https://jamesbridle.com/works/a-ship-adrift)
where he installed a weather station on top of the Southbank Centre in London and used the data generated, including wind speed and air pressure, to determine the path of an "imaginary mad airship". The program logs its theoretical position on Google Maps and gathers streams of information from the internet that are tagged with that location, using them to generate tweets and a log that combine a selection of words it picks up. [from Dezeen](https://www.dezeen.com/2012/10/02/the-internet-has-escaped-out-into-the-street-says-james-bridle/)
+++
Weather thingy by Adrian Kaeser
![Weather thingy](https://player.vimeo.com/video/292088058)
+++
### Identity
- [Fingerprint scanner](https://www.google.com/search?q=fingerprint+sensor)
- RFID/NFC
- Keypad
+++
### Electricity and magnetism
- Magnetometer
- Hall effect sensor (magnetic fields)
- Compass
- Solar cell
+++
### Sound
- Microphone
- Clap sensor
+++
### Biosignals
Are kinds of signals that can be (continually) measured and monitored from biological beings. The term biosignal is often used to mean bio-electrical signal but in fact, biosignal refers to both electrical and non-electrical signals.
+++
### Biosignals (electrical)
- [EEG (electroencephalogram)](https://www.google.com/search?&q=EEG)
- EKG/ECG (electrocardiogram) 
- EMG (electromyographer)
- Respiration
- Goniometer (position / angle)
- Pulse oxymeter (pulse / light) aka [PPG photoplethysmography](https://www.google.com/search?q=ppg+sensor&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjg2vSDo8TeAhWtPOwKHZF2AtoQ_AUIDigB&biw=1099&bih=612)
- GSR (galvanic skin response, aka EDA electro dermal activity)
- Esthetoscope (sound)
- Ultrasound (baby's heart monitor)
+++
### Biosignals (acustic)
- Breathing rhythm (lungs)
- Muscles
- Heart
- Gut
+++
Most biosignals are either electrically weak or acoustically weak and they all are fairly complex, so they require heavy amplification are prone to noise and require extensive digital signal processing (DSP) before they can be used.
+++
### Example QRS complex
![QRS complex](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/QRS_complex.png/220px-QRS_complex.png)
+++
### Brain activity
- [MUSE headband](https://choosemuse.com/)
- [Emotiv headset](https://www.emotiv.com/)
- [HOLST Centre](https://www.google.com/search?q=holst+centre+EEG+headset)
+++
- [EEG kiss sketch](https://player.vimeo.com/video/113102248)
- [EEG kiss theatrical setup](https://player.vimeo.com/video/158300331)
+++
### Biosensing platforms
- [BITalino](http://bitalino.com/en/)
- [BITalino sensor kit](http://biosignalsplux.com/en/products/sensors)
