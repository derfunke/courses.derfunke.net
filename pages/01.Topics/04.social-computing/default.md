---
title: Social computing
---

- Project One
- [Project One Warehouse](http://www.foundsf.org/index.php?title=Project_One_Warehouse)

---
### References
1. Many of the details about The WELL and ECHO bouletin boards comes from the book "Broad Band", 2018, Claire L. Evans
2. "WOMEN AND TECHNOLOGY: HISTORY IS A CAUTIONARY TALE", Orit Gat, April 2018, [link accessed 24.08.2019](http://www.thewhitereview.org/reviews/women-technology-history-cautionary-tale/)
3. "Not everybody is a 30-year-old white guy, and not everybody loves the Grateful Dead. Stacy Horn least of all.", [link accessed 24.08.2019](http://www.youmagazine.co/echo/)
4. "Social Media’s Dial-Up Ancestor: The Bulletin Board System", [link accessed 24.08.2019](https://spectrum.ieee.org/tech-history/cyberspace/social-medias-dialup-ancestor-the-bulletin-board-system)
5. "She pointed the way to a better Facebook... in 1988", KEITH WAGSTAFF, Apr 2018, [link accessed 24.08.2019](https://mashable.com/2018/04/28/claire-evans-broad-band-the-untold-story-of-the-women-who-made-the-internet/)
6. "Growing Old in New York's Snarkiest Early-Internet Community", SANDRA NEWMAN, May 2018, [link accessed 24.08.2019](https://www.theatlantic.com/technology/archive/2017/05/echo-growing-old-online/524577/)

