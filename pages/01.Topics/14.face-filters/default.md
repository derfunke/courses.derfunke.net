---
title: The Face Filter Project
---

I made a shared folder for all of us to share stuff while we work remote. This is the link: [https://zilog.stackstorage.com/s/pvFDVYlhKNeNOHCB](https://zilog.stackstorage.com/s/pvFDVYlhKNeNOHCB) in the "share" you can find a folder with your name on it, use it to exchange materials.

- [Introduction](./intro)
- [Preparations before you begin](./preparing)
- [Session 1 - Intro to Lens Studio](./session1)
- [Session 2 - Blender for filter-making](./session2)
- [Session 3 - Downsampling sculpts and simple modelling project from scratch](./session3)
- [Session 4 - Matcaps and building a shader for them](./session4)
