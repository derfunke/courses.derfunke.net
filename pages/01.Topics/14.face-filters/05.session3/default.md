---
title: Session 3 - more modelling
---

These two tutorials cover some questions that I got from you, hope you find them helpful.

## Part 1 - downsampling meshes (decimate)

<iframe width="560" height="315" src="https://www.youtube.com/embed/uYplms9P-kw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Part 2 - small modelling project from scratch

<iframe width="560" height="315" src="https://www.youtube.com/embed/gyRt9EkZPCE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
