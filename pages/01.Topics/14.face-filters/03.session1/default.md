---
title: Session 1 - Lens Studio primer
---

In this session we will get familiar with the basics of Lens Studio and will do our first couple of filters using a combination of face mesh and post-processing filters.

## Part 1: Lens Studio Walkthrough 
in this video I explain the interface of Lens Studio, what the different screens are, how to navigate around the tool, how to preview your work, how to make screenshots and capture videos, etc. This will be useful to get you started. Go ahead, watch the video.

<iframe width="560" height="315" src="https://www.youtube.com/embed/y7vaSrGmdhU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Take some time to navigate the tool yourself, try to add objects and move them around, get familiar with the interface. And move to *part 2* after you feel more confident where everything is.

Take a break now.

## Part 2: From tool to production
This video is meant for you to replicate step by step. So please open Lens Studio in your computer and follow along.
<iframe width="560" height="315" src="https://www.youtube.com/embed/OPnqds4HTLY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Part 2: Making your first filter
This video is meant for you to replicate step by step. So please open Lens Studio in your computer and follow along.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZRJQPyjH1O0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Part 3: making a psychedelic filter and learning some tricks about post-processing
To follow this tutorial you are going to need some assets, [go to this link](https://zilog.stackstorage.com/s/PNjnuwMuuX5vO725) and select "download map" to get the whole set.

This video is meant for you to replicate step by step. So please follow along.

<iframe width="560" height="315" src="https://www.youtube.com/embed/kkh_XJFWgP8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Exercise for today: appropriating the tool

See the list of videos that you can previsualize your filter with? Well, we are going to be using these videos a lot when we work on filters, so I think maybe we should remake them in our own way. Who are the people in those videos? Let's make the tool our own by looking at the performative aspect of these demo videos. First observe how these videos are made to give good input to the computer vision algorithms used by the app. They are well lit, no heavy shadows, they have clear white backgrounds, faces and bodies fill the frame, they are shot in optimal conditions to be legible to the machine.

I would like to make this tool into a more personal working space, so let's break the tool open and change it to fit ourselves. Please look at each of these videos, [pick two and reinterpret them in your own way](https://docs.google.com/spreadsheets/d/1BGn-cfdPRzkyOW8nyx9F-wgF3HspO3wtodXdgzZugoY/edit), film them using your phone in vertical position. Save your files with their correct filenames and share them by uploading them to the shared folder below.

#### What to deliver?
- Two video files one for each of your chosen actions, in `.webm` format and upload them to the shared folder.

[Upload into `LensStudio hack` folder](https://zilog.stackstorage.com/s/pvFDVYlhKNeNOHCB)

You can use [this spreadsheet to organize who is going to do what](https://docs.google.com/spreadsheets/d/1BGn-cfdPRzkyOW8nyx9F-wgF3HspO3wtodXdgzZugoY/edit?usp=sharing).

