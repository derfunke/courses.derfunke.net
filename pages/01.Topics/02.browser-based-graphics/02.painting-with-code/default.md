---
title: Painting with code
---

#### Why p5js?
- Zero barriers to start with the p5js editor
- Processing heritage
  - Great for graphics fundamentals
  - Easy to learn
- Fairly thin canvas wrapper
- Easy coding means fast prototyping
- Integrates well with other web development techniques
- Once you get the hang of it, it is trivial to change to other libraries
- Great and growing community
- The *p5js widget* means I can write tutorials that have live illustrations which is great for teaching
 
#### Projects made in p5js
- [Pixel Weaver](https://www.sosolimited.com/work/pixelweaver/)
- [Creatability tools](https://experiments.withgoogle.com/collection/creatability) were prototyped in p5js
- [Melody mixer](https://melodymixer.withgoogle.com/ai/melody-mixer/view/)
- [Quick draw!](https://quickdraw.withgoogle.com/data) original R&D work was done in p5js before Google took it over
- [Synth app](https://ericrosenbaum.github.io/MK-1/)  for MakeyMakey
- Yuri Suzuki's [Sonic Pendulum](http://yurisuzuki.com/design-studio/sonic-pendulum)
- [Kinetic typography](https://timrodenbroeker.de/processing-tutorial-kinetic-typography-1/) by Tim Rodenbroeker

#### Alternatives to p5js

There are other **canvas plus** libraries out there that you might prefer over p5js. The fundamentals you will see are more or less the same, but different libraries have different features. p5js focuses on ease of use and leverages the large Processing community.

- From the same people that gave the world [Scriptographer](https://scriptographer.org/) comes [paper.js](http://paperjs.org/) by Jürg Lehni and Jonathan Puckey.
- [konva](https://konvajs.org/) seems ok, but never used it
- [two.js](https://two.js.org/)
- [anime.js](https://animejs.com/) is specially designed for staggered animations

Unlike Processing, which can do 2D and 3D both fairly well, p5js is still very much a 2D library. There is work underway to improve WebGL support and make it 3D as well but there's no public release of this work yet at the time of this writing.

- Round, flat, designer-friendly pseudo-3D engine for canvas & SVG [Zdog](https://zzz.dog/)
- [Threejs](https://threejs.org/) is perhaps the best graphics library for the browser, hands down. It is also one of the most complex ones.
- [pex-gl](https://github.com/pex-gl/pex-gl)

#### p5js editor

I recommend that you work in Firefox, it has the best developer tools and it is the most standards-compliant browser. You can access the [editor here](https://editor.p5js.org/), create an account for yourself so that you can save your experiments and come back to them later. *(in class we have experienced some issues with the p5js editor in Firefox, copy and paste not working for example, perhaps switch back to Chrome if you encounter this problem?)*

![editor](media/editor.p5js.org.png)

These cheatsheets will help you follow:
- [Cheatsheet](media/p5cheatsheet.pdf) for total beginners
- [Cheatsheet](media/p5js-cheatsheet_by_RyoSakai.pdf) by Ryo Sakai (a bit more complete)

#### p5js: anatomy of a p5js sketch

p5js provides you with two major hooks to initialize and to paint in your sketch. When your sketch is loaded p5js will call the function called `setup()` if it exists in your sketch and it will execute it only once. This is a good place to create your canvas, initialize any objects or arrays that your sketch might need, basically any bootstrapping required by your sketch. If you draw anything here it will also be drawn, but it will only be drawn one, so this is not a good place to do animation.

p5js will then try to execute a function called `draw()` in your code, if it finds it, p5js will go into an infinite drawing loop executing this function at every frame. This is the right place to put your drawing and animation code.

![bracketed](media/bracketed.png)

For example this little program hooks onto both of p5js' default functions but doesn't do anything in either, nothing is shown as a result.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  // what you put in here will only be executed once at the beginning of your sketch
}

function draw() {
  // what you put in here will be executed once per frame
}
</script>

#### p5js: dealing with errors

Writing code is fairly tricky when you are not used to it. Many things can go wrong and throw off your browser. Do not fear, things get better with practice and generally you can't break anything when you code in Javascript, so you have the license to experiment. 

Every program must abide by strict syntax and grammar rules, a simple misplaced comma can cause your sketch to display an error message. Error messages are there to tell you what is wrong with your code. Read them carefully and try to decipher their meanings.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  cruateCanvas();
}
</script>

The important bit in this error message is the bit that says `cruateCanvas is not defined`, that means that we are trying to use something in our program called `cruateCanvas` that apparently doesn't exist.

Panic doesn't solve programming errors. Deal with errors with serenity. Time an practice will make interpreting these seemingly arcane messages second nature.

#### p5js: drawing in the coordinate system

Before we draw anything in p5js, we need to create a canvas with a specific dimension. In this example we will create a canvas that is 300 pixels wide by 100 pixels high, and then give it a background color of pink so that we can see it clearly.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(300, 100);
    // paint it pink
    background(255, 0, 200);
}
</script>

Let's now draw a single point in space, at coordinates (0,0).

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
    // paint it pink
    background(255, 0, 200);
    // change painting color to black
    stroke(0);
    // draw a point at position (0,0)
    point(0, 0);
}
</script>

**Do it yourself:** try placing the dot in the center of the screen, then one third to the left and lastly one forth from the right edge. To calculate the positions you can use the `width` and `height` variables in p5js, which contain the dimensions of the canvas.

#### p5js: basic primitives

That dot is kind of hard to see isn't it? Let's draw something else, something that is a little less challenging to actually visualize. Let's draw a circle.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
    // paint it pink
    background(255, 0, 200);
    // do not draw a stroke around our shape
    noStroke();
    // change painting color to black
    fill(0);
    // draw a point at position (0,0)
    ellipse(width/2, height/2, 20, 20);
}
</script>

The `ellipse` function takes 4 parameters, the first two determine the position of its center and the other two determine the `width` and `height` of the ellipse respectively, when these two values are the same the resulting shape is a perfect circle. So the circle is basically a special case of the `ellipse` primitive. Try and change the values and see how this code behaves.

Other primitives in p5js include the `triangle()`, `rect()`, `quad()`, `ellipse()`, and `arc()`. The circle and the square are special cases of `ellipse()` and `rect()` respectively. Let's play around with some of these.


<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
    // paint it pink
    background(255, 0, 200);
    // do not draw a stroke around our shapes
    noStroke();
    // change painting color to black
    fill(0);

    let xpos = 60;

    // draw a point at position (0,0)
    ellipse(xpos, height/2, 20, 20);

    // move 25 pixels to the right
    xpos += 15;

    // draw a rectangle
    rect(xpos, (height/2)-10, 20, 20);

    xpos += 35;

    // draw a triangle
    triangle(xpos, (height/2)-10, xpos-10, (height/2)+10, xpos+10, (height/2)+10);

    xpos += 25;

    // draw an arc center x, center y, width, height, start angle, end angle
    arc(xpos, (height/2)+10, 25, 25, PI, TWO_PI);

    xpos += 16;

    // draw an arc center x, center y, width, height, start angle, end angle
    quad(xpos, (height/2)+10, xpos+5, (height/2)-10, xpos+20, (height/2)-10, xpos+8, (height/2)+10);
}
</script>

As you can see when we try to draw things relative to each other, the calculations to place things on the canvas can become quite overwhelming and difficult to track, in this case we are using a variable named `xpos` to keep track of the `x` position of our shapes, but then we have to calculate each coordinate of each shape. This will quickly become quite a hassle to maintain and our code will become littered with *magical numbers* that we will not be able to read a few weeks from now. There must be a better way!

#### p5js: Adding bitmap images to your sketch

[source for this sketch](https://editor.p5js.org/zilog/sketches/Fb0f2anw9)
<iframe width="400" height="400" src="https://editor.p5js.org/zilog/embed/Fb0f2anw9"></iframe>

#### p5js: Lines

A point is a one dimensional primitive, so we only need to pass the `X` and `Y` position of its location to draw it. But a line can be two dimensional, to define a line we need to have two points. Therefore we need two coordinates.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
    // paint it pink
    background(255, 0, 200);
    // change painting color to black
    stroke(0);
    // draw a line across the canvas
    line(0, 0, width, height);
}
</script>

**Do it yourself:** try the same thing with the line that you tried with the dots, in the `X` axis and then try again for the `Y` axis.

Using simple lines and a coloring trick we can draw a button, or let's better call it *a shape that our eye will read as a button*.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
    // paint it pink
    background(255, 0, 200);

    // store the dimensions of our button in some variables to be used later
    let bh = 40;
    let bw = 120;

    // calculate coordinates for our button
    let tlx = (width/2) - (bw/2);
    let tly = (height/2) - (bh/2);
    let brx = (width/2) + (bw/2);
    let bry = (height/2) + (bh/2);

    // draw light part of our button
    stroke(255);
    line(tlx, tly, tlx+bw, tly);
    line(tlx, tly, tlx, tly+bh);
    // draw shaded part of our button
    stroke(0);
    line(brx, bry, brx-bw, bry);
    line(brx, bry, brx, bry-bh);
}
</script>

This simple coloring technique is fundamental to how we perceive UI widgets, most early browser buttons were draw using variations of this technique. It is so fundamental that artist [Jan Robert Leegte made an artwork about it](http://www.leegte.org/work/pastel-drawings/) in the first example that I know of UI-inspired minimalism.

Or if you prefer an old [material design](https://material.io/components/buttons/) approach to drawing a button.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
}

function draw() {
    // paint it white
    background(255);

    // store the dimensions of our button in some variables to be used later
    let bh = 40;
    let bw = 120;
    let raise = 1.5; 

    // calculate coordinates for our button
    let tlx = (width/2) - (bw/2);
    let tly = (height/2) - (bh/2);
    let brx = (width/2) + (bw/2);
    let bry = (height/2) + (bh/2);

  	noStroke();
  	fill(color(200, 200, 200));
  	rect(tlx+raise, tly+raise, bw, bh);
  	fill(color(255, 100, 100));
  	rect(tlx-raise, tly-raise, bw, bh);
}
</script>

#### Mouse position

p5js give's us an easy way to get the current position of our mouse. The *X* position of our mouse is stored in the `mouseX` variable, and `mouseY` constains the *Y*. With this knowledge, let's implement a simple mouse over for our super-primitive button.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
    // paint it pink
    background(255, 0, 200);
}

function draw() {
    // store the dimensions of our button in some variables to be used later
    let bh = 40;
    let bw = 120;

    // calculate coordinates for our button
    let tlx = (width/2) - (bw/2);
    let tly = (height/2) - (bh/2);
    let brx = (width/2) + (bw/2);
    let bry = (height/2) + (bh/2);

    let mouseIsInside = false;

    // find out if our mouse is inside of the button region defined by it's coordinates
    if( (mouseX > tlx) && (mouseX < brx) && (mouseY > tly) && (mouseY < bry) ) {
      mouseIsInside = true;
    }

  	let l = color(255, 200, 220);
  	let s = color(155, 60, 80);
  
    // draw light part of our button
    if(mouseIsInside) {
      stroke(s);
    } else {
      stroke(l);
    }
    line(tlx, tly, tlx+bw, tly);
    line(tlx, tly, tlx, tly+bh);
    // draw shaded part of our button
    if(mouseIsInside) {
      stroke(l);
    } else {
      stroke(s);
    }
    line(brx, bry, brx-bw, bry);
    line(brx, bry, brx, bry-bh);
}
</script>

Observe how the visual effect in this interaction is entirely determined by a change of color. In this second example I decided not to use black and white to draw the edges of the button and instead used different shades in the pink tint, from a bright pink to a dark, almost brownish tone. What effect does this have in how you perceive the button? How would these two buttons feel to the touch if they were objects and you could run your finger through them?

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
}

function draw() {
    // paint it white
    background(255);

    // store the dimensions of our button in some variables to be used later
    let bh = 40;
    let bw = 120;
    let raisemax = 1.5; 

    // calculate coordinates for our button
    let tlx = (width/2) - (bw/2);
    let tly = (height/2) - (bh/2);
    let brx = (width/2) + (bw/2);
    let bry = (height/2) + (bh/2);

    var mouseIsInside = false;
    var raise = raisemax;
  
    colorMode(HSB);
    var dim = color(255, 200, 80);
    var bright = color(255, 200, 60);
    var shade = color(0, 0, 80);

    var butcol = bright;
  
  // find out if our mouse is inside of the button region defined by it's coordinates
    if( (mouseX > tlx) && (mouseX < brx) && (mouseY > tly) && (mouseY < bry) ) {
      mouseIsInside = true;
      raise = raisemax * 1.3;
      butcol = dim;
    }


  	noStroke();
  	fill(shade);
  	rect(tlx+raise, tly+raise, bw, bh);
  	fill(butcol);
  	rect(tlx-raise, tly-raise, bw, bh);
}
</script>

#### Mouse wheel

<script type="text/p5" data-autoplay data-preview-width="250">
let pos = 25;

function setup() {
  // create a working canvas
  createCanvas(200, 200);
}

function draw() {
  // create a working canvas
  createCanvas(200, 200);
  background(237, 34, 93);
  fill(0);
  rect(25, pos, 50, 50);
}

function mouseWheel(event) {
  print(event.delta);
  //move the square according to the vertical scroll amount
  pos += event.delta;
  //uncomment to block page scrolling
  //return false;
}
</script>

#### p5js: Repeating shapes using loops

Let's use a `for loop` to draw circles in different arrangements.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
}

function draw() {
    // paint it pink
    background(255, 0, 200);
    // do not draw a stroke around our shape
    noStroke();
    // change painting color to black
    fill(0);

    var xpos = 40;
    var ypos = height/2;
  
    for(let count = 0; count < 7; count++) {
      ellipse(xpos, ypos, 20, 20);
      xpos += 20;
    }
}
</script>

Let's at the moment we use the for loop to draw multiple circles and change their `x` coordinate. Let's try and change more than one parameter, let's change colors too.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
}

function draw() {
    // paint it pink
    background(255, 0, 200);
    // do not draw a stroke around our shape
    noStroke();
    // change painting color to black
    fill(0);

    var xpos = 40;
    var ypos = height/2;
    var grey = 0;

    // repeat the block of code 7 times
    for(let count = 0; count < 7; count++) {
      fill(grey);
      ellipse(xpos, ypos, 20, 20);
      xpos += 20; // change the x position
      grey += 35; // change the tone of grey
    }
}
</script>

#### p5js: Using a loop to draw a clock

The formula to arrange items in a circle uses trigonometric functions `sin()` and `cos()`. To arrange things in a circle we need to know the `radius` of that circle too. The `sinus` gives us the `x` position and the `cosinus` gives us the `y` position.

```
x = center.x + radius * sin(angle);
y = center.y + radius * cos(angle);
```

To better understand the relationship between trigonometric functions and the shape of a circle this Khan academy video does a pretty good job at showing it.

<iframe width="560" height="315" src="https://www.youtube.com/embed/a_zReGTxdlQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![angles](media/degrees.png)

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 200);
}

function draw() {
    // paint it pink
    background(255, 0, 200);
    // do not draw a stroke around our shape
    noStroke();
    // change painting color to black
    fill(0);

    // we want to draw 12 markers
    var markers = 12;
    var radius = 50;
    var msize = 15;

    var angle = 0;
    var angleinc = TWO_PI/markers;
  
    for(let count = 0; count < markers; count++) {
      let xpos = width/2 + radius * sin(angle);
      let ypos = height/2 + radius * cos(angle);
      fill(0);
      ellipse(xpos, ypos, msize, msize);
      angle += angleinc;
    }
}
</script>

#### Reuben Margolin: waves to circles and back

Reuben Margolin creates kinetic installations that represent waves all based on this principle.

<iframe width="560" height="315" src="https://www.youtube.com/embed/RIYRdQZPANA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

On minute 6:30 you can see a prototype of the principle at work.

<iframe width="560" height="315" src="https://www.youtube.com/embed/D2HF-1xjpP8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/dehXioMIKg0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Relative coordinates

The `push()` and `pop()` functions of p5js can help us in handling our shapes and position them on screen without having to calculate the individual position of every coordinate for every shape. These functions are used to create local drawing states, for now you can thing of them as *local coordinate systems* that apply only to the shapes in between these two functions. This notion is quite important and it will come back later when we move into 3D graphics and animation.

When you see a `push()` and `pop()` operation you can normally read it as "ok, this person is changing the frame of reference so that the (0,0) positions is somewhere else in the screen now".

Let's look at some practical examples of how this looks like:

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
}

function draw() {
    // paint it pink
    background(255, 0, 200);
    // do not draw a stroke around our shape
    noStroke();
    // change painting color to black
    fill(0);
 
    ellipse(0, 0, 20, 20);
  
    push();
      translate(width/2, height/2);
      ellipse(0, 0, 20, 20);
    pop();
}
</script>

Observe how the instruction that draws the ellipse hasn't changed at all, same parameters, yet these two circles are drawn at different positions. The trick is that the `translate` statement changes the origin of the coordinate system, what we put in `translate(x, y)` becomes our new `(0,0)`.

The `rotate()` statement when given only one parameter will rotate our coordinate system by whatever angle we give it.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  createCanvas(200, 200);
  background(255);
  noStroke();
}

function draw() {
  if (frameCount % 10 == 0) {
    // change to another color every frame
    fill(frameCount * 3 % 255, frameCount * 5 % 255, 
      frameCount * 7 % 255);
    push();
      translate(100, 100);
      rotate(radians(frameCount * 2  % 360));
      rect(0, 0, 80, 20);
    pop();
  }
}
</script>

Let's draw the clock again using `push()`, `pop()`, `translate()`, and `rotate()`.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
  createCanvas(200, 200);
  background(255);
  noStroke();
}

function draw() {
  var angleinc = 360 / 12;
  fill(0);
  translate(width/2, height/2);
  for (let tick = 0; tick < 12; tick++) {
    push();
      rotate( radians(tick * angleinc) );
      push();
        ellipse(50, 0, 12, 12);
      pop();
    pop();
  }
}
</script>


At first it might seem boringly simple but `push` and `pop` have a hidden superpower in that they can be compounded, to create recursive visuals that can be visually quite complex.

<script type="text/p5" data-autoplay data-preview-width="250">
let theta = 0;
let t = 40;

function setup() {
  createCanvas(200, 200);
  smooth();
}

function draw() {
  background(255);
  translate(width/2, height/2);
  for (i=0; i<TWO_PI; i+=0.92) {
    push();
    rotate(theta+i);
    line(0, 0, t, 0);
    for (j=0; j<TWO_PI; j+=0.88) {
      push();
      translate(t,0);
      rotate(-theta-j);
      line(0, 0, t, 0);
      for (g=0; g<TWO_PI; g+=0.6){
        push();
        translate(t,0);
        rotate(theta+g);
        line(0,t,0,0);
        pop();
      }
      pop();
    }
    pop();
  }
  theta+=0.005;
}
</script>

### Follow the rabbit into the hole 🐰

- [Read more about transformations in the Processing manual](https://processing.org/tutorials/transform2d/)
- To better understand matrix transformations have a look at this one on [matrix transformations](http://www.proxyarch.com/search/lab2/lab2.html)

##### More about p5js
- [Cheatsheet](media/p5cheatsheet.pdf)
- [Cheatsheet](media/p5js-cheatsheet_by_RyoSakai.pdf) by Ryo Sakai
- To continue learning more about p5js go to the p5js.org community [learning resources](https://github.com/processing/p5.js/wiki/Educational-Resources) there's plenty there.

##### Basic HTML/CSS grids
- [Gridlover](https://www.gridlover.net/try)
- [The Grid System](http://www.thegridsystem.org/category/tools/)

##### Grid systems
- If you come from graphic design you will likely know this, but grids are vital in creating well balanced compositions. When working with HTML/CSS specially if you are working on responsive interfaces, you will need a grid. There are many options out there that will get you started, these are some of my favorites:
  - [Raster Grid System](https://rsms.me/raster/) this one is a little unusual but I find it quite intuitive, ymmv
  - [Bulma](https://bulma.io/) flexbox based
  - [Flexboxgrid](http://flexboxgrid.com)
  - [Skeleton dead simple and responsive](http://getskeleton.com/)
  - [Simplegrid](https://simplegrid.io/) small and easy to learn but quite limited

##### stand-alone p5js apps

If you are confident with Javascript the command line and working with your own editor you can try Pantera. For this course we have created a stand-alone [p5js application bootstrap called Pantera, you can find it here](/topics/browser-based-graphics/p5js-pantera).

### Acknowledgements

`push()/pop()` example was adapted from [code in OpenProcessing by Jose Concha](https://www.openprocessing.org/sketch/81999). Thanks!

Cheatsheets by Ryo Sakai [@ryodejaneiro](https://twitter.com/ryodejaneiro) and [Ben Moren](https://github.com/bmoren). Thank you very much!

The dynamic poster compositions are largely inspired by published work from [Studio Naam](https://studionaam.com)

### Appendix: Josef Albers color cyling experiment 

What you see in this experiment is a dynamic version of the Josef Albers illustration, where we simply adjust the saturation of the colours in the background without the smaller squares. Observe how your eye interprets the relationship between the colors as the colors fade.

<script type="text/p5" data-autoplay data-preview-width="250">
let sat = 0;
let lastUpdate = 0;
let direction = 1

function setup() {
  createCanvas(200, 200);
}

function draw() {
  let w = width / 6;
  noStroke();
  colorMode(HSL);

  fill(hue(color('#FF8600')), sat, lightness(color('#FF8600')));
  rect(0, 0, w*1.8, height);

  fill(color('#AC5B0E'));
  rect((w*1.8)-40, (height/2)-20, 40, 40);

  let xpos = w*1.8;
  fill(hue(color('#FFF725')), sat, lightness(color('#FFF725')));
  rect(xpos, 0, w*1.2, height);

  fill(hue(color('#2B2C67')), sat, lightness(color('#2B2C67')));
  xpos += w*1.2;
  rect(xpos, 0, w*1.2, height);

  fill(hue(color('#428BB5')), sat, saturation(color('#428BB5')));
  xpos += w*1.2;
  rect(xpos, 0, w*1.8, height);

  fill(color('#AC5B0E'));
  rect(xpos, (height/2)-20, 40, 40);

  // every 100ms update the saturation of our colors
  if( 100 < (millis() - lastUpdate) ) { sat+=direction; lastUpdate = millis(); }

  // go from no-saturation at all to full saturation and then back
  if( (sat > 128) || (sat < 0) ) { direction = -1 * direction; };
}
</script>

### Appendix: poster compositions

Here you can find a couple of poster designs for a digital release, you can think of these as quick loops running in public screens. This design has quite a graphic look, with a grid layout composed of 6 rows and 3 columns. The middle row with the dynamic graphic elements is the only thing that changes, both designs are made using p5js primitives, namely `arc()` in the case of th first design and and `rect()` in the case of the second. What appears to the eye as a fancy animation is a `sin()` wave that modulates the sizes of our drawn elements.

[source for this poster](https://editor.p5js.org/zilog/sketches/lzi8r8Bwu)
<iframe  width="440" height="610" src="https://editor.p5js.org/zilog/embed/lzi8r8Bwu"></iframe>

[source for this poster](https://editor.p5js.org/zilog/sketches/HbDutD150)
<iframe  width="440" height="610" src="https://editor.p5js.org/zilog/embed/HbDutD150"></iframe>

[source for this book cover](https://editor.p5js.org/zilog/sketches/Fk8VIoboJ)
<iframe width="440" height="610" src="https://editor.p5js.org/zilog/embed/Fk8VIoboJ"></iframe>
