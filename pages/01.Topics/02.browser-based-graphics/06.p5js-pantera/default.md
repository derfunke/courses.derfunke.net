---
title: p5js Pantera
---

The official bootstrap kit for the Data+Matter Lab is called p5js.Pantera. [You can find it in our gitlab](https://gitlab.com/dropmeaword/p5js.pantera) repo.
