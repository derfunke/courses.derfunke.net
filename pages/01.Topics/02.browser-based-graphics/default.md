---
title: Browser based graphics
---

This module focuses on the basics of drawing and animating in the browser using a canvas API, it will also touch upon simple CSS animation. It is meant as a starting point to explore color, shapes and two-dimensional computational drawing. 

The principles laid out in this module are useful for mobile development and the general notions are still valid even if you are using sophisticated UI libraries. Ultimately when you need a custom widget or animation most of these libraries will let you draw directly into a widget's canvas or framebuffer and it is there that you can apply these techniques to achieve the effect that you seek.

### Acknowledgements

This instruction materials owe a great deal to [Lauren McCarthy and the p5js community](http://www.p5js.org) as well as Processing at large and the Processing Foundation for supporting the development of all this wonderful work.

Thanks also to Atul Varma aka [@tollness](https://github.com/toolness) for the [p5js widget](https://github.com/toolness/p5.js-widget), which made it possibl for this course to have interactive inline code examples.

Most of the examples in these tutorials are my own, as couldn't find something with the focus and the fast pace that I needed in my course. All this material is released in the hope that it can be helpful to my students first and foremost and perhaps other souls out there in the internet that want an in-depth yet directly applicable knowledge of animation and graphics in the browser.

In this materials I refer to the work of artists, designers and coders. I try to give credit to everybody for their credit. If you see your work here and the credit is missing, please accept my apologies and feel free to contact me to notify me of the omission.

