---
title: Topics
---

I use the materials in this topics section during my classes as references that try to offer a starting point for a student to get started in a topic. Here you can find historical materials, how-tos, guides, surveys, pictures, videos, etc. These materials have been curated by me and they are therefore a personal vision, I do not claim that this should be understood as a cannon.

