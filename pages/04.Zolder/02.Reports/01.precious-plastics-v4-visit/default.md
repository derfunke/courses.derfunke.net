---
title: Visita a Precious Plastic durante la Semana del Diseño en Eindhoven
---

Estimados compañeros Copinchantes, aquí les envio un breve sumario pictórico de mi visita a la última exposición de Precious Plastic que tuvo lugar durante la Dutch Design Week en Eindhoven hace unos días. Durante este evento los chicos y chicas de Precious Plastic desvelaron la última versión de las máquinas de reciclaje en las que han estado trabajando durante el último año.

![recipientes](./media/2019-preciousplastics-v4__2.jpg)

Ese día fuimos gratamente recibidos por Joseph y Adrián, que han estado viviendo aquí durante el último año. Joseph se encarga de desarrollar modelos de negocio para los talleres de Precious Plastic y Adrián es un diseñador de producto gallego que encontró en Precious Plastic una manera mejor de canalizar su profesión, que la mque estimulaba la educación que recibió en la escuela de diseño donde estudió. Ambos han estado totalmente sumergidos en esta comunidad durante el último año y han vivido, comido y trabajado juntos en un grupo de de unos cincuenta voluntarios que día a día configuran la actividad de Precious Plastic, una comunidad autónoma y temporal unida por el objetivo común de empoderar a comunidades a reciclar su propio plástico.

Durante la Dutch Design Week y con la visita del diseñador y artista Cubano Ernesto Oroza, hicimos todos juntos una visita al taller de Precious Plastic que para la ocasión de la Semana del Diseño de Eindhoven se había convertido en un espacio de expositivo donde se podía ver la rebosante actividad de esta comunidad.

![recipientes](./media/2019-preciousplastics-v4__164.jpg)
![recipientes](./media/2019-preciousplastics-v4__165.jpg)

Si se quieren bajar todas las fotos en un solo paquete y con la calidad original, [las podeis obtener aquí](./media/2019-preciousplastics-v4___all-pictures.zip).

Espero que esta documentación les sirva de inspiración y motivación a nuestros compañeros de Copincha en La Habana. 

## El último año de Precious Plastic

Adrián y Joseph nos cuentan que durante el último año gran parte del esfuerzo del grupo se concentró en tareas de investigación y desarrollo de maquinaria. Entre sus objetivos está atender también la necesidad explícita de muchos de los talleres que querían crecer y crear productos de manera más fiable. Las versiones anteriores de la maquinaria de Precious Plastic estaba pensada para talleres de pequeña escala y comunidades que reciclan plástico para el uso propio. 

La versión 4, que es el esfuerzo que les ha tenido concentrados durante el último año, está pensada para talleres medianos que quieren crear productos de plástico reciclado por su cuenta a una mayor escala y con mayor diversidad de materiales. El trabajo de investigación que han llevado a cabo gira alrededor de crear nueva maquinaria con mayor capacidad y mejor calidad en el producto resultante, así como investigación sobre las posibilidades del material a través de experimentos.

Uno de los retos en el reciclaje de plástico es el de clasificar y separar correctamente el plástico que se obtiene de las colectas. Este proceso se hace típicamente a mano. Como parte de la investigación del equipo este año se desarrolló también un prototipo de brazo robótico que utiliza algoritmos de *machine learning* para diferenciar el plástico por tipo y color.

![cestita](./media/2019-preciousplastics-v4__1.jpg)

### La exposición

La expo estaba dividida en tres grandes secciones, por un lado las máquinas que han diseñado para procesar el plástico reciclado, mostradas dentro del contexto de un taller mínimo, donde vemos a la máquina acompañada por las herramientas necesarias para operarlas y trabajar con los materiales que la máquina produce.

![talleres](./media/2019-preciousplastics-v4__161.jpg)

Otra de las areas de la expo muestra productos creados por miembros de la comunidad global de Precious Plastic. Algunos de los productos están muy elaborados, se ven ejemplos de talleres que se han especializado en un proceso, como por ejemplo crear perfiles en forma de T, que sirven como materia prima para diseños más robustos. Talleres que crean juguetes, muebles e incluso ropa a partir de plástico reciclado usando los procesos de Precious Plastic.

![productos](./media/2019-preciousplastics-v4__160.jpg)

Otra sección está dedicada a tres proyectos paralelos, uno de clasificación, con el brazo robótico, aun en fase experimental. Otro de prevención, que hace uso de otro tipo de desechos materiales orgánicos para crear envoltorios o recipientes que no sean de plástico, que sean viables en su contexto de uso además de biodegradables.

![recipientes](./media/2019-preciousplastics-v4__163.jpg)


### Las máquinas y su talleres


#### Taller de recogida

Aquí se efectua la recogida, limpieza y separación de plásticos que es el comienzo del proceso de reciclaje. Nuestros guías Joseph y Adrian nos cuentan que este es un proceso clave en el que es fundamental implicar a la gente que contribuye el plástico. Ya que supone muchísimo trabajo limpiar el plástico si lo tiene que hacer el propio taller. Nos contaron historias de cómo han implicado a negocios locales, como por ejemplo heladerías poniendo carteles y pequeños puntos de colecta donde pedían a los consumidores que chupasen la cucharilla del helado hasta que estuviese limpia, antes de donarla al proyecto. Adrían nos cuenta que es clave en el proceso de concienciación de la gente que el plástico se entregue limpio, pues es a través de ese gesto que la gente se conciencia realmente de lo preciado que es este material como recurso.

Estación de clasificación de plástico entrante:
![recipientes](./media/2019-preciousplastics-v4__3.jpg)

Punto de limpieza:
![recipientes](./media/2019-preciousplastics-v4__4.jpg)

Adrián nos muestra un puesto de trabajo donde se le quitan las etiquetas a los envases.
![recipientes](./media/2019-preciousplastics-v4__5.jpg)
![recipientes](./media/2019-preciousplastics-v4__6.jpg)

Cartelito de la campaña "Lick it Clean" de cucharillas de helado, llevada a cabo en puntos de colaboración en la ciudad de Eindhoven.

![recipientes](./media/2019-preciousplastics-v4__10.jpg)

Educando a la comunidad a como tratar el plástico antes de donarlo.
![recipientes](./media/2019-preciousplastics-v4__8.jpg)

Antes del proceso de reciclaje es crucial que los plásticos de diferentes tipos estén bien separados, diferentes tipos de plásticos tienen propiedades diferentes y procesos de reciclaje diferentes, además de tener efectos secuntarios diferentes. Existen por ejemplo diferentes tipos de toxicidad en según que plásticos. Por ello es importante separarlos adecuadamente:

![recipientes](./media/2019-preciousplastics-v4__9.jpg)

![recipientes](./media/2019-preciousplastics-v4__11.jpg)

#### Taller de trituración

El siguiente paso es triturar los envases, tapones y diferentes productos de plástico para obtener virutas, que son el producto básico del cual se derivan todos los demás productos en el proceso.

![recipientes](./media/2019-preciousplastics-v4__12.jpg)
![recipientes](./media/2019-preciousplastics-v4__13.jpg)

La version 4 de la trituradora es más grande, tiene mayor capacidad y potencia.

![recipientes](./media/2019-preciousplastics-v4__14.jpg)
![recipientes](./media/2019-preciousplastics-v4__16.jpg)
![recipientes](./media/2019-preciousplastics-v4__17.jpg)

En el taller de trituración vemos ejemplos del equipamiento que sería util para complementar este proceso:

![recipientes](./media/2019-preciousplastics-v4__18.jpg)

#### Taller de extrusión de vigas y palos

![recipientes](./media/2019-preciousplastics-v4__22.jpg)
![recipientes](./media/2019-preciousplastics-v4__23.jpg)
![recipientes](./media/2019-preciousplastics-v4__25.jpg)
![recipientes](./media/2019-preciousplastics-v4__27.jpg)
![recipientes](./media/2019-preciousplastics-v4__28.jpg)
![recipientes](./media/2019-preciousplastics-v4__29.jpg)

Barra creada a partir de botellas PET:
![recipientes](./media/2019-preciousplastics-v4__30.jpg)

Detalles en el deposito de las capas de colores en el proceso de extrusión:
![recipientes](./media/2019-preciousplastics-v4__31.jpg)
![recipientes](./media/2019-preciousplastics-v4__32.jpg)
![recipientes](./media/2019-preciousplastics-v4__33.jpg)

Viga de perfil T:
![recipientes](./media/2019-preciousplastics-v4__35.jpg)

Moldes de inyección de vigas:
![recipientes](./media/2019-preciousplastics-v4__36.jpg)
![recipientes](./media/2019-preciousplastics-v4__37.jpg)
![recipientes](./media/2019-preciousplastics-v4__38.jpg)

Ejemplo de taller de extrusión al completo, con herramientas, máquina, extractor de humos y zona de almacenado:
![recipientes](./media/2019-preciousplastics-v4__19.jpg)
![recipientes](./media/2019-preciousplastics-v4__21.jpg)

#### Taller de planchas

Esta es la muestra del taller de prensado de plancha, con una superficie de trabajo que permite preparar el molde con las virutas, la prensa hidraulica, las herramientas necesarias y la zona de almacenamiento para las planchas resultantes del proceso.

![recipientes](./media/2019-preciousplastics-v4__120.jpg)
![recipientes](./media/2019-preciousplastics-v4__121.jpg)
![recipientes](./media/2019-preciousplastics-v4__122.jpg)
![recipientes](./media/2019-preciousplastics-v4__123.jpg)
![recipientes](./media/2019-preciousplastics-v4__124.jpg)
![recipientes](./media/2019-preciousplastics-v4__125.jpg)
![recipientes](./media/2019-preciousplastics-v4__126.jpg)
![recipientes](./media/2019-preciousplastics-v4__128.jpg)
![recipientes](./media/2019-preciousplastics-v4__129.jpg)
![recipientes](./media/2019-preciousplastics-v4__130.jpg)
![recipientes](./media/2019-preciousplastics-v4__131.jpg)
![recipientes](./media/2019-preciousplastics-v4__132.jpg)
![recipientes](./media/2019-preciousplastics-v4__133.jpg)
![recipientes](./media/2019-preciousplastics-v4__134.jpg)
![recipientes](./media/2019-preciousplastics-v4__127.jpg)

#### Taller de inyección

Quizás el taller más popular de Precious Plastic, aquí se podía ver la versión anterior de las máquinas en pleno funcionamiento:

![recipientes](./media/2019-preciousplastics-v4__155.jpg)
![recipientes](./media/2019-preciousplastics-v4__156.jpg)
![recipientes](./media/2019-preciousplastics-v4__157.jpg)

#### Los productos desarrollados por la comunidad

![recipientes](./media/2019-preciousplastics-v4__85.jpg)
![recipientes](./media/2019-preciousplastics-v4__87.jpg)
![recipientes](./media/2019-preciousplastics-v4__88.jpg)
![recipientes](./media/2019-preciousplastics-v4__89.jpg)
![recipientes](./media/2019-preciousplastics-v4__90.jpg)
![recipientes](./media/2019-preciousplastics-v4__92.jpg)
![recipientes](./media/2019-preciousplastics-v4__93.jpg)
![recipientes](./media/2019-preciousplastics-v4__94.jpg)
![recipientes](./media/2019-preciousplastics-v4__95.jpg)

Taller en Tailandia que ha experimentado mezclando materiales como la madera y el plástico reciclado:
![recipientes](./media/2019-preciousplastics-v4__96.jpg)
![recipientes](./media/2019-preciousplastics-v4__97.jpg)
![recipientes](./media/2019-preciousplastics-v4__98.jpg)

![recipientes](./media/2019-preciousplastics-v4__99.jpg)
![recipientes](./media/2019-preciousplastics-v4__100.jpg)
![recipientes](./media/2019-preciousplastics-v4__101.jpg)

![recipientes](./media/2019-preciousplastics-v4__102.jpg)
![recipientes](./media/2019-preciousplastics-v4__103.jpg)
![recipientes](./media/2019-preciousplastics-v4__104.jpg)
![recipientes](./media/2019-preciousplastics-v4__105.jpg)
![recipientes](./media/2019-preciousplastics-v4__106.jpg)
![recipientes](./media/2019-preciousplastics-v4__107.jpg)

![recipientes](./media/2019-preciousplastics-v4__108.jpg)
![recipientes](./media/2019-preciousplastics-v4__109.jpg)
![recipientes](./media/2019-preciousplastics-v4__110.jpg)
![recipientes](./media/2019-preciousplastics-v4__114.jpg)

![recipientes](./media/2019-preciousplastics-v4__115.jpg)
![recipientes](./media/2019-preciousplastics-v4__116.jpg)
![recipientes](./media/2019-preciousplastics-v4__117.jpg)
![recipientes](./media/2019-preciousplastics-v4__118.jpg)

![recipientes](./media/2019-preciousplastics-v4__135.jpg)
![recipientes](./media/2019-preciousplastics-v4__136.jpg)

Nodos de junta para construir cúpulas geodésicas hechos a partir de un molde de inyección:

![recipientes](./media/2019-preciousplastics-v4__119.jpg)
![recipientes](./media/2019-preciousplastics-v4__137.jpg)
![recipientes](./media/2019-preciousplastics-v4__40.jpg)
![recipientes](./media/2019-preciousplastics-v4__41.jpg)
![recipientes](./media/2019-preciousplastics-v4__42.jpg)

Viga en forma de I, icono de la arquitectura moderna:
![recipientes](./media/2019-preciousplastics-v4__138.jpg)
![recipientes](./media/2019-preciousplastics-v4__139.jpg)
![recipientes](./media/2019-preciousplastics-v4__140.jpg)

Diseño para una fachada paramétrica:
![recipientes](./media/2019-preciousplastics-v4__141.jpg)
![recipientes](./media/2019-preciousplastics-v4__142.jpg)

#### Ladrillo LEGO

Uno de los proyectos que se podían ver en el espacio de exposición es el de este sistema modular de ladrillos de plástico para poder levantar viviendas temporales para responder a crisis humanitarias. El sistema de ensamblaje permite a un equipo no especializado apilar ladrillos como si de un LEGO se tratase, sin necesidad de cemento.

![recipientes](./media/2019-preciousplastics-v4__43.jpg)
![recipientes](./media/2019-preciousplastics-v4__44.jpg)
![recipientes](./media/2019-preciousplastics-v4__45.jpg)

Instantanea del video que muestra el proceso de moldeo del ladrillo:
![recipientes](./media/2019-preciousplastics-v4__47.jpg)

Molde para el ladrillo modular:
![recipientes](./media/2019-preciousplastics-v4__48.jpg)
![recipientes](./media/2019-preciousplastics-v4__49.jpg)

Pruebas y diferentes fases del proceso de inyección del ladrillo:
![recipientes](./media/2019-preciousplastics-v4__50.jpg)
![recipientes](./media/2019-preciousplastics-v4__51.jpg)

#### Experimentación con el material

Durante el último año los voluntarios de Precious Plastic han hecho cientos de pruebas materiales para familiarizarse con los secretos del proceso, cómo crear formas, patrones, colores, moldes, distintos acabados, imitaciones de otros materiales, etc. Una parde de la exposición estaba dedicada a mostrar algunos de estos experimentos materiales.

![recipientes](./media/2019-preciousplastics-v4__52.jpg)
![recipientes](./media/2019-preciousplastics-v4__53.jpg)
![recipientes](./media/2019-preciousplastics-v4__54.jpg)
![recipientes](./media/2019-preciousplastics-v4__55.jpg)
![recipientes](./media/2019-preciousplastics-v4__56.jpg)
![recipientes](./media/2019-preciousplastics-v4__57.jpg)
![recipientes](./media/2019-preciousplastics-v4__58.jpg)
![recipientes](./media/2019-preciousplastics-v4__59.jpg)
![recipientes](./media/2019-preciousplastics-v4__60.jpg)
![recipientes](./media/2019-preciousplastics-v4__61.jpg)
![recipientes](./media/2019-preciousplastics-v4__62.jpg)
![recipientes](./media/2019-preciousplastics-v4__63.jpg)
![recipientes](./media/2019-preciousplastics-v4__64.jpg)

#### Un robot para separar los plásticos

Aun en fase experimental, este brazo robótico de segunda mano ha sido re-adaptado por un grupo de voluntarios que han creado un software a medida que especializa a este robot en la tarea de separación de plásticos automatizada. Durante la exposición el robot funcionaba con un circuito cerrado de plásticos que permitia a miembros del publico interactuar con él.

![robot3](./media/2019-preciousplastics-v4__65.jpg)
![robot2](./media/2019-preciousplastics-v4__66.jpg)
![robot1](./media/2019-preciousplastics-v4__67.jpg)

<video id="myVideo" preload="auto" src="https://edu.derfunke.net/zolder/reports/precious-plastics-v4-visit/media/2019-preciousplastics-v4__167.m4v" type="video/mp4" controls></video>

#### Sustitutos al plástico de un solo uso

El proyecto de Precious Plastic no solo se enfoca de cara al reciclaje del plástico ya desechado. Evitar que el plástico llegue a los vertederos también implica encontrar sustitutos para plásticos de un solo uso. Un grupo de voluntarios llevó a cabo varios experimentos de moldeado de materiales residuals biodegradables que se pueden emplear para crear envases, empaquetado y envoltorios partiendo de materia orgánica como mondas de naranja, café molido usado, hojas plantas, etc.

![recipientes](./media/2019-preciousplastics-v4__70.jpg)
![recipientes](./media/2019-preciousplastics-v4__71.jpg)
![recipientes](./media/2019-preciousplastics-v4__72.jpg)
![recipientes](./media/2019-preciousplastics-v4__73.jpg)
![recipientes](./media/2019-preciousplastics-v4__74.jpg)

Adrián muestra la prensa que se ha empleado en estos experimientos:
![recipientes](./media/2019-preciousplastics-v4__76.jpg)
![recipientes](./media/2019-preciousplastics-v4__77.jpg)
![recipientes](./media/2019-preciousplastics-v4__78.jpg)

La prensa está pensada para poder ocupar un espacio pequeño, la esquina de un almacén o tienda donde se puedan crear los envases in situ, sin necesidad de transportar materiales o productos.

## Publicación

Los miembros de la comunidad de Precious Plastic han estado trabajando duro desarrollando estas investigaciones, pero este proceso no se termina en la exposición, en la siguiente fase esperan completar el material de instrucción, así como los planos para todas las máquinas. La publicación de estos materiales está planeado para Enero del 2020, [puedes seguir los planes en este website](https://next.preciousplastic.com/).

![recipientes](./media/2019-preciousplastics-v4__159.jpg)


## La comunidad de Precious Plastic

El último año la comunidad de Precious Plastic se ha sostenido por el trabajo de sus voluntarios, pero también con el apoyo del ayuntamiento de Eindhoven y una beca de desarrollo que les ha permitido hacer las inversiones necesarias para llevar a cabo sus pesquisas. Pero a finales de Noviembre estos acuerdos de apoyo cerrarán ciclo y los voluntarios tendrán que mudarse. El futuro es incierto y no hay un plan ya establecido para los siguientes pasos, pero nuestros guías no parecían preocupados pues son conscientes de que hay trabajo más que de sobra cuando se trata de reciclar plástico.

![recipientes](./media/2019-preciousplastics-v4__166.jpg)


