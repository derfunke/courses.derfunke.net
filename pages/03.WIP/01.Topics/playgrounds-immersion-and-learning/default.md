---
title: Learning from Playgrounds
---


## References

- [The Forgotten Artistic Playgrounds of the 20th Century](https://hyperallergic.com/295172/the-forgotten-artistic-playgrounds-of-the-20th-century/)
- [The Guardian on Eric McMilan](https://www.theguardian.com/news/2019/aug/09/ball-pits-water-slides-playground-design-eric-mcmillan-childrens-village-ontario-place-toronto)