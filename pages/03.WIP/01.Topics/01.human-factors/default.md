---
title: Human Factors in Interaction Design
---

## Shneiderman’s Eight Golden Rules of Interface Design

- **Strive for consistency** by utilizing familiar icons, colors, menu hierarchy, call-to-actions, and user flows when designing similar situations and sequence of actions. Standardizing the way information is conveyed ensures users are able to apply knowledge from one click to another; without the need to learn new representations for the same actions. Consistency plays an important role by helping users become familiar with the digital landscape of your product so they can achieve their goals more easily.
- **Enable frequent users to use shortcuts**. With increased use comes the demand for quicker methods of completing tasks. For example, both Windows and Mac provide users with keyboard shortcuts for copying and pasting, so as the user becomes more experienced, they can navigate and operate the user interface more quickly and effortlessly.
- **Offer informative feedback**. The user should know where they are at and what is going on at all times. For every action there should be appropriate, human-readable feedback within a reasonable amount of time. A good example of applying this would be to indicate to the user where they are at in the process when working through a multi-page questionnaire. A bad example we often see is when an error message shows an error-code instead of a human-readable and meaningful message.
- **Design dialogue to yield closure**. Don’t keep your users guessing. Tell them what their action has led them to. For example, users would appreciate a “Thank You” message and a proof of purchase receipt when they’ve completed an online purchase.
- **Offer simple error handling**. No one likes to be told they’re wrong, especially your users. Systems should be designed to be as fool-proof as possible, but when unavoidable errors occur, ensure users are provided with simple, intuitive step-by-step instructions to solve the problem as quickly and painlessly as possible. For example, flag the text fields where the users forgot to provide input in an online form.
- **Permit easy reversal of actions**. Designers should aim to offer users obvious ways to reverse their actions. These reversals should be permitted at various points whether it occurs after a single action, a data entry or a whole sequence of actions. As Shneiderman states in his book:

>This feature relieves anxiety, since the user knows that errors can be undone; it thus encourages exploration of unfamiliar options.

- **Support internal locus of control**. Allow your users to be the initiators of actions. Give users the sense that they are in full control of events occurring in the digital space. Earn their trust as you design the system to behave as they expect.
- **Reduce short-term memory load**. Human attention is limited and we are only capable of maintaining around five items in our short-term memory at one time. Therefore, interfaces should be as simple as possible with proper information hierarchy, and choosing recognition over recall. Recognizing something is always easier than recall because recognition involves perceiving cues that help us reach into our vast memory and allowing relevant information to surface. For example, we often find the format of multiple choice questions easier than short answer questions on a test because it only requires us to recognize the answer rather than recall it from our memory. Jakob Nielsen, a user advocate who’s been called one of the “world’s most influential designers” by Bloomberg Businessweek has invented several usability methods including heuristic evaluation. Recognition over recall is one of Nielsen’s ten usability heuristics for interface design.

## ISO 9241-110 (formerly ISO9241-10)
Ergonomic requirements for office work with visual display terminals (VDTs) -- Part 10: Dialogue principles.

This part deals with general ergonomic principles which apply to the design of dialogues between humans and information systems. The use of the word *dialogues* here means the sequence of actions and reactions that are required for the successful completion of an interaction. This is not to be confused with pop-up dialogs, or UI elements.

| principle | description |
|:---|:---|
|suitability for the task|a dialogue supports the user in the effective and efficient completion of a task|
|suitability for learning|a dialogue supports and guides the user during the learning phases|
|suitability for individualization|the dialogue system is constructed to allow for modification by the user to match the individual's needs and skills for a given task|
|conformity with user expectations|the dialogue correspondond's to the user's knowledge of the task, education, experience and to commonly accepted conventions|
|self-descriptiveness|each step in a dialog system is comprehensible through feedback from the current state of the system, or is explained to the user on request|
|controllability|we say that the interactive system is controllable when the user is able to maintain direction over the whole course of the interaction until the point that the goal has been met|
|error tolerance|we say that an interaction is error tolerant, if despite obvious errors in the input the intended result may be achieved with minimal or not corrective ction from the user|

### The rabbit hole 🐰
- [Shneiderman’s Eight Golden Rules Will Help You Design Better Interfaces](https://www.interaction-design.org/literature/article/shneiderman-s-eight-golden-rules-will-help-you-design-better-interfaces) at the Interactive Design Foundation.
- [ISO 9241-210 ergonomics of human–system
interaction. Part 210: Human-centred design for interactive
systems](https://richardcornish.s3.amazonaws.com/static/pdfs/iso-9241-210.pdf)

## Acknowledgements
Shneiderman’s Eight Golden Rules material was taken from the introductory section of the Interaction Design Fountation page on the same title.

The seven ergonomic principles of ISO 9241-110, [were obtained in part from wikipedia](https://en.wikipedia.org/wiki/ISO_9241#cite_note-ISO9241-210_2010-4) the phrasing of the definitions was writen by the author based on .

