### Articles
- [Dan Saffer The End of Design as We Know It](https://medium.com/@odannyboy/the-end-of-design-as-we-know-it-bc2dfe0cb4)
- The journey of learning how to code [Why Learning to Code is So Damn Hard](https://www.thinkful.com/blog/why-learning-to-code-is-so-damn-hard/)

### Js libraries
- [Tonejs](https://tonejs.github.io/) Tone.js is a framework for creating interactive music in the browser. It provides advanced scheduling capabilities, synths and effects, and intuitive musical abstractions built on top of the Web Audio API.
- [Cannonjs](https://schteppe.github.io/cannon.js/) physics engine
- [Ammo.js](http://kripken.github.io/ammo.js)
- [dat.GUI](http://workshop.chromeexperiments.com/examples/gui/#1--Basic-Usage)
- [p5.serial](https://github.com/p5-serial/p5.serialport)
- [p5canvas for VS code](https://github.com/pixelkind/p5canvas)

### 3D printing
- [3D printing joint classroom](https://coloringchaos.github.io/form-fall-16/joints)

### History of computers
- [UCLA’s 1948 Mechanical Computer](https://vimeo.com/70589461)
- [The Never-Before-Told Story of the World's First Computer Art (It's a Sexy Dame)](https://www.theatlantic.com/technology/archive/2013/01/the-never-before-told-story-of-the-worlds-first-computer-art-its-a-sexy-dame/267439/)

### Sketching tools
- [remarkable](https://remarkable.com/)

### Books
- [The Architecture Machine](https://www.amazon.com/Architecture-Machine-Toward-Human-Environment/dp/0262640104/ref=sr_1_1?keywords=The+Architecture+Machine+nicholas+negroponte&qid=1566678887&s=gateway&sr=8-1)

### Post-capitalism
- [Belt and Road rap](https://www.youtube.com/watch?v=98RNh7rwyf8)

### Body modifications
- [tragus magnet implant](https://www.cnet.com/news/surgically-implanted-headphones-are-literally-in-ear/) ([video](https://www.youtube.com/watch?v=yTqHZrIcCdE))

- [Future Interviews](https://vimeo.com/79277863) Mike Mills

### Light

- [Anthony James Studio - Portals](https://vimeo.com/321319194) infinite mirror effect with platonic solids
- [bitPALACE - Wu Juehui](https://vimeo.com/207929349) great projection mapping work
- [e-blood bag - Wu Juehui](https://vimeo.com/208089840) phone chargers as blood bags

### P5js
- Blockly + P5js [Rigglin](https://rigglin.appspot.com) (released 5 Sept 2019)

### DIY
- [David Mellis dissertation defense at MIT media lab](https://www.media.mit.edu/videos/mellis-2015-07-09/)

### Tools: HTML/CSS
- [Gridlover](https://www.gridlover.net/try)
- [The Grid System](http://www.thegridsystem.org/category/tools/)

### Tools: UI/UX

Wireframing tools:
- [protopie](https://www.protopie.io/)
- [proto.io](https://proto.io/)
- [InVision](https://www.invisionapp.com/)
- [Axure](https://www.axure.com/)

Prototyping:
- [UXpin](https://www.uxpin.com/)
- [Framer](https://www.framer.com/)
- [Origami](https://origami.design/)

Custom animation examples:
- [Custom animation in Flutter](https://medium.com/flutter-community/building-a-custom-page-load-animation-in-flutter-89f9aaa51e93)
- [Understanding linear interpolation in UI animations](https://www.freecodecamp.org/news/understanding-linear-interpolation-in-ui-animations-74701eb9957c/)
 
Hardware:
- [A closer look at Apple's breathing light](https://avital.ca/notes/a-closer-look-at-apples-breathing-light)

## Open Source Intelligence OSINT
- [SpiderFoot](https://www.spiderfoot.net/index.html)

## Machine Learning
- [Machine Learning for Designers](https://www.oreilly.com/library/view/machine-learning-for/9781491971444/)
- [A visual introduction to machine learning language](http://www.r2d3.us/visual-intro-to-machine-learning-part-1/)
- [A VISUAL INTRODUCTION TO MACHINE LEARNING—PART II](http://www.r2d3.us/visual-intro-to-machine-learning-part-2/)
- [Machine Learning — A magic ingredient for prototyping](https://medium.com/a-view-from-above/machine-learning-a-magic-ingredient-for-prototyping-f7851f7474c7)

## Data science
- [Data Carpentry](https://datacarpentry.org/)
- [SURFsara deep learning](https://github.com/sara-nl/elixir-dl)
- [Geohackweek: Machine Learning](https://geohackweek.github.io/machine-learning/)

## Deploying your work on the public web
- [Awesome self-hosting](https://github.com/awesome-selfhosted/awesome-selfhosted)
- [Glitch.com](https://www.glitch.com/)
- [Netlify](https://www.netlify.com/)
  
## Open Tabs on Android

- [This is your phone on feminism](https://conversationalist.org/2019/09/13/feminism-explains-our-toxic-relationships-with-our-smartphones/)
- [Our Networks Recorded Talks](https://ournetworks.ca/recorded-talks/)
- [The Old School Fire Effect and Bare-metal programming](https://www.hanshq.net/fire.html)
- [Pleroma.social](https://pleroma.social/)
- [Coding is not the new literacy](https://www.chris-granger.com/2015/01/26/coding-is-not-the-new-literacy/)
- [Ballot Bin](https://ballotbin.co.uk/)
- [Designing Ethically](https://medium.com/@odannyboy/designing-ethically-edc29352679a) Dan Saffer on Medium
- [RunwayML](https://runwayml.com/) machine learning for creators
- [The MoFE](https://www.themofe.com/) Museum of Future Experiences
- [When Body Draws Abstract Space](http://socks-studio.com/2017/07/19/when-body-draws-the-abstract-space-slat-dance-by-oskar-schlemmer/) “Slat Dance” by Oskar Schlemmer
- [The Original E.P.C.O.T](https://sites.google.com/site/theoriginalepcot/model-overview)
- [Bifocal display](https://www.interaction-design.org/literature/book/the-encyclopedia-of-human-computer-interaction-2nd-ed/bifocal-display)
- [History of Virtual Reality](https://www.vrs.org.uk/virtual-reality/history.html)
- [Lee Felsestein](http://www.leefelsenstein.com/?page_id=100)
- [Myndr internet 'thermostaat'](https://www.sprout.nl/artikel/startups/deze-thermostaat-voor-internet-zorgt-voor-minder-strijd-gezinnen)
- [Daniel Wurztel](http://www.danielwurtzel.com/) artist that works with fans and fabrics
- [The Choosatron](https://choosatron.com/)
- [Twine](https://twinery.org) interactive storytelling tool
- [Office Plants, Verena Friedrich](http://heavythinking.org/office-plants/)
- Jane Jacobs (important author on urbanism)
- [Host Yo Self](https://hostyoself.com/) browser-based self-hosting
- [Maslow CNC](https://www.maslowcnc.com/)
- [Run Your Own Social](https://runyourown.social/) Darius Kazemi
- [Unsplash](https://unsplash.com/) freely usable images
- [Worldwide FM](https://worldwidefm.net/)
- [The Accidental Cosmopolitanism of Facebook](https://conditiohumana.io/facebook-global-community/)
- [The Disconnect](https://thedisconnect.co/) off-line only digital magazine
- [Jami.net](https://jami.net/) french GNU messaging software available on all platforms
- [Rendering the desert of the real](https://unthinking.photography/articles/rendering-the-desert-of-the-real) essay on digital images
- [MIDI sprout](https://www.midisprout.com/) listen to the music of plants
- [Luna](https://luna-lang.org/) is a node-based programming language for data processing
- [Stupid Shit No One Needs and Terrible Ideas Hackathon](http://www.stupidhackathon.com/)
- [Cloud platform for Virtual Worlds](https://coherence.io/)
- [Holepunch.io](https://holepunch.io/) gets you past any NAT or firewall
- [The Charismatic Prototype](https://limn.it/articles/the-charismatic-prototype/)
- [veryinteractive.net](http://veryinteractive.net/classes) this website is a hub for courses taught by Laurel Schwulst
- [The Gentrification of the Internet](http://culturedigitally.org/2019/03/the-gentrification-of-the-internet/) essay
- [Airtable](https://airtable.com/)
- [DropzoneJS](https://www.dropzonejs.com/) drag'n'drop uploads with image previews
- [Don't Delay() Use an Arduino Task Scheduler Today!](https://www.hackster.io/GadgetsToGrow/don-t-delay-use-an-arduino-task-scheduler-today-215cfe)
- Work by Lalalab [Las Calles Habladas](http://www.lalalab.org/las-calles-habladas/)
